KEY_FILE=$1;
KEY_ALIAS=$2;
STORE_PASSWORD=$3;
LEAD_NAME=$4;
APP_NAME=$5;
ORGANISATION=$6;
CITY=$7;
REGION=$8;
COUNTRY=$9;

if ! [ -f "${KEY_FILE}" ]; then
  keytool -genkey -v -keystore "${KEY_FILE}" -keyalg RSA -deststoretype pkcs12 -keysize 4096 -validity 10000 -alias "${KEY_ALIAS}" -keypass ${KEY_PASSWORD} -storepass ${KEY_PASSWORD} -dname "CN=${LEAD_NAME}, OU=${APP_NAME}, O=${ORGANISATION}, L=${CITY}, S=${REGION}, C=${COUNTRY}";
else
  echo "Key already generated";
fi