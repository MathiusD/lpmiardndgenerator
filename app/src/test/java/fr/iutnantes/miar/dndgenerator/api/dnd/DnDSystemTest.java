package fr.iutnantes.miar.dndgenerator.api.dnd;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class DnDSystemTest {
    @Test
    public void getRandomSystem() {
        assertNotNull(DnDSystem.getRandomSystem());
        assertTrue(DnDSystem.systems.contains(DnDSystem.getRandomSystem()));
    }

    @Test
    public void testCompareTo() {
        List<DnDSystem> systemsUnSorted = new ArrayList<>();
        systemsUnSorted.add(DnDSystem.lbb);
        systemsUnSorted.add(DnDSystem.lotfp);
        List<DnDSystem> systemsSorted = new ArrayList<>();
        systemsSorted.add(DnDSystem.lotfp);
        systemsSorted.add(DnDSystem.lbb);
        assertNotEquals(systemsSorted, systemsUnSorted);
        Collections.sort(systemsUnSorted, DnDSystem::compareTo);
        assertEquals(systemsSorted, systemsUnSorted);

    }
}
