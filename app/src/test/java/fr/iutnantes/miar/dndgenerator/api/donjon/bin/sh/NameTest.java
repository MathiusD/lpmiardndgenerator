package fr.iutnantes.miar.dndgenerator.api.donjon.bin.sh;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static fr.iutnantes.miar.dndgenerator.api.donjon.bin.sh.Name.createAllCombination;
import static fr.iutnantes.miar.dndgenerator.api.utils.ListUtils.joinList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class NameTest {
    Name example = new Name("example", null);
    Name secondExample = new Name("2ndExample", null);
    List<Name> twoExamples = Arrays.asList(example, secondExample);
    Name exampleTruc = new Name("example", "truc");
    Name secondExampleTruc = new Name("2ndExample", "truc");
    List<Name> twoExamplesTruc = Arrays.asList(exampleTruc, secondExampleTruc);
    Name exampleBidule = new Name("example", "bidule");
    Name secondExampleBidule = new Name("2ndExample", "bidule");
    List<Name> fourExamples = Arrays.asList(
        exampleTruc, exampleBidule, secondExampleTruc, secondExampleBidule);
    List<Name> allExamples = joinList(Arrays.asList(twoExamples, twoExamplesTruc,
        Arrays.asList(exampleBidule, secondExampleBidule)));
    List<Name> allExampleArg = Arrays.asList(example, exampleTruc, exampleBidule);
    List<Name> allSecondExampleArg = Arrays.asList(secondExample, secondExampleTruc,
        secondExampleBidule);

    @Test
    public void createAllCombinationTest() {
        assertEquals(
            Collections.singletonList(example),
            createAllCombination("example")
        );
        assertEquals(
            twoExamples, createAllCombination("example", "2ndExample")
        );
        assertEquals(
            twoExamples, createAllCombination(Arrays.asList("example", "2ndExample"))
        );
        assertEquals(
            twoExamplesTruc, createAllCombination(Arrays.asList("example", "2ndExample"),
            "truc")
        );
        assertEquals(
            fourExamples, createAllCombination(Arrays.asList("example", "2ndExample"),
            "truc", "bidule")
        );
        assertEquals(
            fourExamples, createAllCombination(Arrays.asList("example", "2ndExample"),
                Arrays.asList("truc", "bidule"))
        );
    }
    @Test
    public void matchWithTest() {
        assertTrue(example.matchWith("ExAmple", "Truc", "Bidule"));
        assertTrue(example.matchWith("  Example", "Truc", "Bidule"));
        assertFalse(example.matchWith("Truc", "Bidule"));
        assertTrue(exampleTruc.matchWith("ExAmple", "Truc", "Bidule"));
        assertTrue(exampleTruc.matchWith("  Example", "truc   ", "Bidule"));
        assertTrue(exampleTruc.matchWith("ExAmple, Truc", "Bidule"));
        assertTrue(exampleTruc.matchWith("ExAmple", "Truc, Bidule"));
        assertTrue(exampleTruc.matchWith("ExAmple, Truc, Bidule"));
        assertFalse(exampleTruc.matchWith("Truc", "Bidule"));
        assertFalse(exampleTruc.matchWith("Example", "Bidule"));
        assertFalse(exampleTruc.matchWith("Bidule"));
    }
    @Test
    public void getNamesMatchesTest() {
        assertEquals(allExampleArg,
            Name.getNamesMatches(allExamples, "example", "truc", "bidule"));
        assertEquals(allSecondExampleArg,
            Name.getNamesMatches(allExamples, "2nDexample", "truc", "bidule"));
    }
    @Test
    public void matchAlmostWithTest() {
        assertTrue(example.matchAlmostWith("ExAmple", "Truc", "Bidule"));
        assertTrue(example.matchAlmostWith("  Example", "Truc", "Bidule"));
        assertFalse(example.matchAlmostWith("Truc", "Bidule"));
        assertTrue(exampleTruc.matchAlmostWith("ExAmple", "Truc", "Bidule"));
        assertTrue(exampleTruc.matchAlmostWith("  Example", "truc   ", "Bidule"));
        assertTrue(exampleTruc.matchAlmostWith("ExAmple, Truc", "Bidule"));
        assertTrue(exampleTruc.matchAlmostWith("ExAmple", "Truc, Bidule"));
        assertTrue(exampleTruc.matchAlmostWith("ExAmple, Truc, Bidule"));
        assertTrue(exampleTruc.matchAlmostWith("Truc", "Bidule"));
        assertTrue(exampleTruc.matchAlmostWith("Example", "Bidule"));
        assertFalse(exampleTruc.matchAlmostWith("Bidule"));
    }
    @Test
    public void getNamesAlmostMatchesTest() {
        assertNotEquals(allExampleArg,
                Name.getNamesAlmostMatches(allExamples, "example", "truc", "bidule"));
        assertNotEquals(allSecondExampleArg,
                Name.getNamesAlmostMatches(allExamples, "2nDexample", "truc", "bidule"));
        assertNotEquals(allExampleArg,
                Name.getNamesAlmostMatches(allExamples, "example", "bidule"));
        assertNotEquals(allSecondExampleArg,
                Name.getNamesAlmostMatches(allExamples, "2nDexample", "truc"));
        assertEquals(allExampleArg,
                Name.getNamesAlmostMatches(allExamples, "example"));
        assertEquals(allSecondExampleArg,
                Name.getNamesAlmostMatches(allExamples, "2nDexample"));
    }
}
