package fr.iutnantes.miar.dndgenerator.api.dnd;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DnDClassTest {

    @Test
    public void testCompareTo() {
        List<DnDClass> classUnSorted = new ArrayList<>();
        classUnSorted.add(DnDClass.FIGHTER);
        classUnSorted.add(DnDClass.DWARF);
        List<DnDClass> classSorted = new ArrayList<>();
        classSorted.add(DnDClass.DWARF);
        classSorted.add(DnDClass.FIGHTER);
        assertNotEquals(classSorted, classUnSorted);
        Collections.sort(classUnSorted, DnDClass::compareTo);
        assertEquals(classSorted, classUnSorted);
    }
}
