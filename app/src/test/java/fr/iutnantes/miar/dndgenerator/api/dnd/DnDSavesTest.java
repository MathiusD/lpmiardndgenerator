package fr.iutnantes.miar.dndgenerator.api.dnd;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DnDSavesTest {
    @Test
    public void toStringTest() {
        //With Empty Saves
        DnDSaves saves = new DnDSaves(null, null, null, null,
                null);
        assertEquals(saves.toString(), DnDSaves.NO_SAVES);
        //With 1 Saves
        saves = new DnDSaves(1, null, null, null, null);
        assertEquals(saves.toString(), "wands : 1");
        //With N Saves
        saves = new DnDSaves(1, 2, null, 3, null);
        assertEquals(saves.toString(), String.format("wands : 1%nbreath : 2%nmagic : 3"));
        //With all Saves
        saves = new DnDSaves(1, 2, 3, 4, 5);
        assertEquals(saves.toString(),
                String.format("wands : 1%nbreath : 2%nstone : 3%nmagic : 4%npoison : 5"));
    }
}
