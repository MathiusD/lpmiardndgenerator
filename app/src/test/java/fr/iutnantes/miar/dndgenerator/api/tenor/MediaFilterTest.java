package fr.iutnantes.miar.dndgenerator.api.tenor;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MediaFilterTest {
    @Test
    public void getMinimalMediaFilter() {
        //With MinimalOrLower
        assertEquals(MediaFilter.MINIMAL,
            MediaFilter.getMinimalMediaFilter(MediaFilter.MinimalAndLower));
        //With BasicOrLower
        assertEquals(MediaFilter.BASIC,
                MediaFilter.getMinimalMediaFilter(MediaFilter.BasicAndLower));
        //With Default
        List<MediaFilter> list = new ArrayList<>();
        list.add(MediaFilter.DEFAULT);
        assertEquals(MediaFilter.DEFAULT,
                MediaFilter.getMinimalMediaFilter(list));
        //With CustomList
        list = new ArrayList<>();
        list.add(MediaFilter.BASIC);
        list.add(MediaFilter.MINIMAL);
        assertEquals(MediaFilter.BASIC,
                MediaFilter.getMinimalMediaFilter(list));
    }
}
