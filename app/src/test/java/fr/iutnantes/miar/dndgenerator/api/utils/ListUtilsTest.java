package fr.iutnantes.miar.dndgenerator.api.utils;

import org.junit.Test;

import java.util.Arrays;

import static fr.iutnantes.miar.dndgenerator.api.utils.ListUtils.joinList;
import static org.junit.Assert.assertEquals;

public class ListUtilsTest {
    @Test
    public void joinListTest() {
        assertEquals(
            Arrays.asList(1, 2, 3), joinList(Arrays.asList(1, 2), 3)
        );
        assertEquals(
            Arrays.asList(1, 2, 3, 4),
            joinList(Arrays.asList(Arrays.asList(1, 2), Arrays.asList(3, 4)))
        );
    }

}
