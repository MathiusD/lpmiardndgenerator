package fr.iutnantes.miar.dndgenerator.api.dnd;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DnDSkillsTest {
    @Test
    public void toStringTest() {
        //With Empty Skills
        DnDSkills skills = new DnDSkills(new ArrayList<>());
        assertEquals(skills.toString(), DnDSkills.NO_SKILLS);
        //With 1 Skill
        List<DnDSkill> skillsData = new ArrayList<>();
        DnDSkill example = new DnDSkill("blap", 3);
        skillsData.add(example);
        skills = new DnDSkills(skillsData);
        assertEquals(skills.toString(), String.format("%s", example));
        //With N Skills
        skillsData = new ArrayList<>();
        skillsData.add(example);
        skillsData.add(example);
        skillsData.add(example);
        skills = new DnDSkills(skillsData);
        assertEquals(skills.toString(), String.format("%s%n%s%n%s", example, example, example));
    }
}
