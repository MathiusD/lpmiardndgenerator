package fr.iutnantes.miar.dndgenerator.api.tenor;

import androidx.annotation.NonNull;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * Class representing the data in a GIF for easy backup
 */
public class GIFArtifact implements Serializable {

    public final String query;
    public final MediaFilter mediaFilter;
    public final FilterContent filterContent;

    /**
     * Default Constructor
     */
    public GIFArtifact() {
        this(GIFObject.NOT_FOUND);
    }

    /**
     * Constructor with only query of research
     * @param query : String represent query of research
     */
    public GIFArtifact(@NonNull String query) {
        this(query, MediaFilter.DEFAULT);
    }

    /**
     * Constructor with query and mediaFilter of research
     * @param query : String represent query of research
     * @param mediaFilter : MediaFilter represent mediaFilter used in research
     */
    public GIFArtifact(@NonNull String query, @NonNull MediaFilter mediaFilter) {
        this(query, mediaFilter, FilterContent.DEFAULT);
    }

    /**
     * Constructor with query and filterContent of research
     * @param query : String represent query of research
     * @param filterContent : FilterContent represent filterContent used in research
     */
    public GIFArtifact(@NonNull String query, @NonNull FilterContent filterContent) {
        this(query, MediaFilter.DEFAULT, filterContent);
    }

    /**
     * Constructor with all arguments
     * @param query : String represent query of research
     * @param mediaFilter : MediaFilter represent mediaFilter used in research
     * @param filterContent : FilterContent represent filterContent used in research
     */
    public GIFArtifact(@NonNull String query, @NonNull MediaFilter mediaFilter,
                       @NonNull FilterContent filterContent) {
        this.query = query;
        this.mediaFilter = mediaFilter;
        this.filterContent = filterContent;
    }

    /**
     * For compare Any Object with this artifact.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }
    /**
     * For identify artifact.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(21, 23)
            .append(query.hashCode()).append(mediaFilter.hashCode())
            .append(filterContent.hashCode()).toHashCode();
    }
}
