package fr.iutnantes.miar.dndgenerator.api.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * ListUtils is a class with utils methods
 */
public class ListUtils {

    /**
     * Method for join data inside unique list
     * @param list Base list used for join data
     * @param data Data used for join inside list
     * @param <T> Class of Data
     * @return joinedList
     */
    public static <T> List<T> joinList(List<T> list, T... data) {
        return joinList(Arrays.asList(list, Arrays.asList(data)));
    }

    /**
     * Method for join data inside unique list
     * @param lists lists used for join data
     * @param <T> Class of Data
     * @return joinedList
     */
    public static <T> List<T> joinList(List<List<T>> lists) {
        List<T> out = new ArrayList<>();
        for (List<T> list: lists)
            out.addAll(list);
        return out;
    }
}
