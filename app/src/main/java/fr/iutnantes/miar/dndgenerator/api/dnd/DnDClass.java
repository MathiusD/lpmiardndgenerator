package fr.iutnantes.miar.dndgenerator.api.dnd;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * DnDClass is a class representing the character’s class with possibly the search parameter to
 * retrieve cards of that class only
 */
public class DnDClass implements Serializable, Comparable<DnDClass> {

    public final String queryArg;
    public final String name;

    /**
     * Basic Constructor for create class if queryArg is missing
     * @param name : String represent Name of this class
     */
    public DnDClass(@NonNull String name) {
        this(name, null);
    }

    /**
     * Basic Constructor for create attribute if all arguments are provided.
     * @param name : String represent Name of this class
     * @param queryArg : String represent the argument to pass to find a record with this class.
     */
    public DnDClass(@NonNull String name, @Nullable String queryArg) {
        this.name = name;
        this.queryArg = queryArg;
    }

    /**
     * For write class.
     * @return toString
     */
    @Override
    @NonNull
    public String toString() {
        return this.name;
    }

    /**
     * For identify attributes.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    /**
     * For compare Any Object with this class.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }

    public static final List<DnDClass> classes;
    public static DnDClass CLERIC = new DnDClass("Cleric", "cleric");
    public static DnDClass FIGHTER = new DnDClass("Fighter", "fighter");
    public static DnDClass MAGICUSER = new DnDClass("Magic-User", "magicuser");
    public static DnDClass THIEF = new DnDClass("Thief", "thief");
    public static DnDClass ELF = new DnDClass("Elf", "elf");
    public static DnDClass DWARF = new DnDClass("Dwarf", "dwarf");
    public static DnDClass HALFLING = new DnDClass("Halfling", "halfling");
    public static DnDClass ANYONE = new DnDClass("Any");
    static {
        List<DnDClass> clas = new ArrayList<>();
        clas.add(CLERIC);
        clas.add(FIGHTER);
        clas.add(MAGICUSER);
        clas.add(THIEF);
        clas.add(ELF);
        clas.add(DWARF);
        clas.add(HALFLING);
        Collections.sort(clas, DnDClass::compareTo);
        classes = clas;
    }

    /**
     * For get random class from the API.
     * @return class
     */
    @NonNull
    public static DnDClass getRandomClass() {
        return getRandomClass(classes);
    }

    /**
     * For get random class from list provided.
     * @param dnDclass : List where we’re going to shoot randomly
     * @return class
     */
    @NonNull
    public static DnDClass getRandomClass(@NonNull List<DnDClass> dnDclass) {
        return dnDclass.get(new Random().nextInt(dnDclass.size()));
    }

    /**
     * For compare 2 class
     * @param o : Object for comparison
     * @return compareToResult
     */
    @Override
    public int compareTo(DnDClass o) {
        return this.name.compareTo(o.name);
    }
}
