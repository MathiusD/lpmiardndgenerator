package fr.iutnantes.miar.dndgenerator.api.tenor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class representing the different types of media available within the Tenor API.
 */
public class GIFFormat implements Serializable {

    public final String name;
    public final List<MediaFilter> mediaFiltersSupported;

    /**
     * Default constructor with name of format
     * @param name : String represent Name of format
     */
    public GIFFormat(@NonNull String name) {
        this(name, new ArrayList<>());
    }

    /**
     * Constructor with all arguments
     * @param name : String represent Name of format
     * @param mediaFiltersSupported : Media in which the format is present
     */
    public GIFFormat(@NonNull String name, @NonNull MediaFilter... mediaFiltersSupported) {
        this.name = name;
        List<MediaFilter> temp = new ArrayList<>();
        for (MediaFilter mediaFilterSupported : mediaFiltersSupported)
            if (mediaFilterSupported != null)
                temp.add(mediaFilterSupported);
        this.mediaFiltersSupported = temp;
    }

    /**
     * Constructor with all arguments
     * @param name : String represent Name of format
     * @param mediaFiltersSupported : List of media in which the format is present
     */
    public GIFFormat(@NonNull String name, @NonNull List<MediaFilter> mediaFiltersSupported) {
        this.name = name;
        this.mediaFiltersSupported = mediaFiltersSupported;
    }

    /**
     * Indicate if mediaFilter contain this format
     * @param mediaFilter : MediaFilter for verification
     * @return media
     */
    public boolean isPresentInMediaFilter(MediaFilter mediaFilter) {
        return this.mediaFiltersSupported.contains(mediaFilter);
    }

    /**
     * For write format.
     * @return toString
     */
    @NonNull
    @Override
    public String toString() {
        return name;
    }

    /**
     * For compare Any Object with this format.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }

    /**
     * For identify format.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public final static GIFFormat GIF = new GIFFormat("gif", MediaFilter.MinimalAndLower);
    public final static GIFFormat MEDIUMGIF = new GIFFormat("mediumgif", MediaFilter.DEFAULT);
    public final static GIFFormat TINYGIF = new GIFFormat("tinygif",
        MediaFilter.MinimalAndLower);
    public final static GIFFormat NANOGIF = new GIFFormat("nanogif",
        MediaFilter.BasicAndLower);
    public final static GIFFormat MP4 = new GIFFormat("mp4", MediaFilter.MinimalAndLower);
    public final static GIFFormat LOOPEDMP4 = new GIFFormat("loopedmp4", MediaFilter.DEFAULT);
    public final static GIFFormat TINYMP4 = new GIFFormat("tinymp4",
        MediaFilter.BasicAndLower);
    public final static GIFFormat NANOMP4 = new GIFFormat("nanomp4",
        MediaFilter.BasicAndLower);
    public final static GIFFormat WEBM = new GIFFormat("webm", MediaFilter.DEFAULT);
    public final static GIFFormat TINYWEBM = new GIFFormat("tinywebm", MediaFilter.DEFAULT);
    public final static GIFFormat NANOWEBM = new GIFFormat("nanowebm", MediaFilter.DEFAULT);
    public final static List<GIFFormat> ALL_FORMAT;
    public final static List<GIFFormat> GIFS_FORMAT;
    public final static List<GIFFormat> MP4_FORMAT;
    public final static List<GIFFormat> WEBM_FORMAT;
    static {
        List<GIFFormat> gifFmt = new ArrayList<>();
        gifFmt.add(GIF);
        gifFmt.add(MEDIUMGIF);
        gifFmt.add(TINYGIF);
        gifFmt.add(NANOGIF);
        GIFS_FORMAT = gifFmt;
        List<GIFFormat> mp4Fmt = new ArrayList<>();
        mp4Fmt.add(MP4);
        mp4Fmt.add(LOOPEDMP4);
        mp4Fmt.add(TINYMP4);
        mp4Fmt.add(NANOMP4);
        MP4_FORMAT = mp4Fmt;
        List<GIFFormat> webmFmt = new ArrayList<>();
        webmFmt.add(WEBM);
        webmFmt.add(TINYWEBM);
        webmFmt.add(NANOWEBM);
        WEBM_FORMAT = webmFmt;
        List<GIFFormat> allFmt = new ArrayList<>();
        allFmt.addAll(GIFS_FORMAT);
        allFmt.addAll(MP4_FORMAT);
        allFmt.addAll(WEBM_FORMAT);
        ALL_FORMAT = allFmt;
    }

    /**
     * For get specific format if supported.
     * @param name : String to represent format to search
     * @return format
     */
    @Nullable
    public static GIFFormat get(String name) {
        for(GIFFormat fmt : ALL_FORMAT)
            if (fmt.name.toLowerCase().equals(name.toLowerCase()))
                return fmt;
        return null;
    }

    /**
     * For get random format from the API.
     * @return format
     */
    @NonNull
    public static GIFFormat getRandomGIFFormat() {
        return getRandomGIFFormat(ALL_FORMAT);
    }

    /**
     * For get random format from list provided.
     * @param fmts : List where we’re going to shoot randomly
     * @return formats
     */
    @NonNull
    public static GIFFormat getRandomGIFFormat(@NonNull List<GIFFormat> fmts) {
        return fmts.get(new Random().nextInt(fmts.size()));
    }

    /**
     * For get minimal media filter for get this format for reduce cost of request
     * @return mediaFilter
     */
    @Nullable
    public MediaFilter getMinimalMediaFilter() {
        return MediaFilter.getMinimalMediaFilter(this.mediaFiltersSupported);
    }
}
