package fr.iutnantes.miar.dndgenerator.api.dnd;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

/**
 * DnDSkills is a class that represents all the skills of a character record.
 */
public class DnDSkills implements Serializable {

    public static final String NO_SKILLS =
        "No skills provided (The system probably doesn’t manage the skills).";

    public final List<DnDSkill> skills;

    /**
     * Basic Constructor for create skills if all arguments are provided.
     * @param skills : List of DnDSkill represents all attributes of DnDSheet
     */
    public DnDSkills(@NonNull List<DnDSkill> skills) {
        this.skills = skills;
    }

    /**
     * For write skills.
     * @return toString
     */
    @NonNull
    @Override
    public String toString() {
        String out = "";
        for (DnDSkill skill : skills)
            out = String.format("%s%s%s", out, !out.equals("") ? String.format("%n") : "", skill);
        return !out.equals("") ? out : NO_SKILLS;
    }

    /**
     * For identify skills.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return skills.hashCode();
    }

    /**
     * For compare Any Object with this skills.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }
}
