package fr.iutnantes.miar.dndgenerator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import java.io.Serializable;
import java.util.List;

import fr.iutnantes.miar.dndgenerator.adapter.ListResultsAdapter;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDClass;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSheet;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSystem;

public class ResearchResultsActivity extends AppCompatActivity {
    private static final int ERROR_INTENT = -2;
    private static boolean m_hasListRetour = false;
    private static String TAG = "ResearchResultsActivity :: ";
    public static final String SHEET = "sheet";
    private static Bundle m_savedInstanceState;
    public static List<DnDSheet> m_listResults;
    ListResultsAdapter m_adater;
    private int m_systemKey, m_classKey, m_numberResults;
    RecyclerView rclr_researchResults;
    ProgressDialog m_progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_research_results);
        m_savedInstanceState = savedInstanceState;
        rclr_researchResults = findViewById(R.id.rclr_researchResults);

        // Get intent elements
        Intent v_intent = getIntent();
        m_classKey = v_intent.getIntExtra(HomeActivity.CLASS_KEY, ERROR_INTENT);
        m_systemKey = v_intent.getIntExtra(HomeActivity.SYSTEM_KEY, ERROR_INTENT);
        m_numberResults = v_intent.getIntExtra(HomeActivity.NUMBER_RESULT, 1);
        v_intent.removeExtra(HomeActivity.SYSTEM_KEY);
        Log.d(TAG, m_systemKey + " " + m_classKey);
    }

    @Override
    protected void onResume() {
        super.onResume();

        AsyncTaskResearch v_asyncTaskResearch = new AsyncTaskResearch();
        v_asyncTaskResearch.execute(m_savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable("listResults", (Serializable) m_listResults);
        super.onSaveInstanceState(outState);
    }

    private void displayResult(Bundle savedInstanceState) {
        DnDSystem v_system = DnDSystem.anyone;
        DnDClass v_class = DnDClass.ANYONE;

        if(m_systemKey > -1) {
            v_system = DnDSystem.systems.get(m_systemKey);
        }
        if(m_classKey > -1) {
            v_class = DnDClass.classes.get(m_classKey);
        }

        if(m_hasListRetour) {
            m_hasListRetour = false;
            return;
        }
        if (savedInstanceState != null && (m_systemKey == ERROR_INTENT || m_classKey == ERROR_INTENT)) {
            m_listResults = (List<DnDSheet>) savedInstanceState.getSerializable("listResults");
        }
        else {
            m_listResults = DnDSheet.getRandomsCaracterSheet(this, m_numberResults, v_system, v_class);
        }

    }

    private void setView() {
        m_adater = new ListResultsAdapter(m_listResults, ResearchResultsActivity.this);
        rclr_researchResults.setAdapter(m_adater);
        rclr_researchResults.setLayoutManager(new LinearLayoutManager(ResearchResultsActivity.this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == resultCode) {
            int v_index = data.getIntExtra(ListResultsAdapter.INDEX, -1);
            if (v_index > -1) {
                m_listResults.set(v_index, (DnDSheet) data.getSerializableExtra(DisplayResultActivity.SHEET));
                m_hasListRetour = true;
            }
        }
    }

    private class AsyncTaskResearch extends AsyncTask<Bundle, String, Nullable> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            m_progressDialog = new ProgressDialog(ResearchResultsActivity.this);
            m_progressDialog.setMessage("Please wait...It is downloading");
            m_progressDialog.setIndeterminate(false);
            m_progressDialog.setCancelable(false);
            m_progressDialog.show();
        }
        @Override
        protected Nullable doInBackground(Bundle... bundle) {
            displayResult(bundle[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Nullable nullable) {
            super.onPostExecute(nullable);
            setView();
            m_progressDialog.hide();
        }
    }
}