package fr.iutnantes.miar.dndgenerator.api;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * Class used as a basis for our databases
 */
public abstract class BaseDB extends SQLiteOpenHelper {
    protected Context ctx;
    protected int currentVersion;
    protected String dbName;

    /**
     * Method for generate sql query for creation table
     * @param allStringAttrs : List of text fields
     * @param intAttrs : List of int fields
     * @param tableName : Base table name
     * @param versionDatabase : Version of database
     * @param primaryKey : Primary key
     * @return sqlQuery
     */
    @NonNull
    public static String getSQL_CREATE_ENTRIES(@NonNull List<String> allStringAttrs,
                                               @NonNull List<String> intAttrs,
                                               @NonNull String tableName,
                                               @NonNull Integer versionDatabase,
                                               @NonNull String primaryKey) {
        String tempCreation = String.format(
            "CREATE TABLE IF NOT EXISTS %s (%s INTEGER PRIMARY KEY,", tableName(tableName,
                versionDatabase), primaryKey);
        for (String field : allStringAttrs)
            tempCreation = String.format("%s%s TEXT,", tempCreation, field);
        for (String field : intAttrs)
            tempCreation = String.format("%s%s INTEGER,", tempCreation, field);
        tempCreation = String.format("%s)", tempCreation.substring(0, tempCreation.length() - 1));
        return tempCreation;
    }

    /**
     * Method for generate sql query for drop table
     * @param tableName : Base table name
     * @param dbVersion : Version of database
     * @return sqlQuery
     */
    @NonNull
    public static String getSQL_DELETE_ENTRIES(@NonNull String tableName,
                                               @NonNull Integer dbVersion) {
        return String.format("DROP TABLE IF EXISTS %s", tableName(tableName, dbVersion));
    }

    /**
     * Default Constructor
     * @param context : Context for access database
     * @param dbName : Name of database
     * @param dbVersion : Version of database
     */
    public BaseDB(@NonNull Context context, @NonNull String dbName, int dbVersion) {
        super(context, dbName, null, dbVersion);
        this.ctx = context;
        this.currentVersion = dbVersion;
        this.dbName = dbName;
        this.onCreate(this.getWritableDatabase(), dbVersion);
    }

    /**
     * Method for upgrade table in database
     * @param db : sql connector linked to database
     * @param oldVersion : Integer represent old version
     * @param newVersion : Integer represent new version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion, tableName());
    }

    /**
     * Method for generate name of table based to base Table name and version of model
     * @param baseTableName : Base name for table
     * @param dbVersion : Version of database
     * @return name
     */
    @NonNull
    public static String tableName(@NonNull String baseTableName, int dbVersion) {
        return String.format("%s_%d", baseTableName, dbVersion);
    }

    /**
     * Method for generate name of table based to base Table name and version of model
     * @return name
     */
    @NonNull
    public String tableName() {
        return tableName(dbName, currentVersion);
    }

    public abstract void onCreate(@NonNull SQLiteDatabase db, @NonNull Integer dbVersion);

    /**
     * Method for create table in database
     * @param db : sql connector linked to database
     * @param allStringAttrs : List of text fields
     * @param intAttrs : List of int fields
     * @param tableName : Base table name
     * @param versionDatabase : Version of database
     * @param primaryKey : Primary key
     */
    public void onCreate(@NonNull SQLiteDatabase db, @NonNull List<String> allStringAttrs,
                         @NonNull List<String> intAttrs, @NonNull String tableName,
                         @NonNull Integer versionDatabase, @NonNull String primaryKey) {
        db.execSQL(getSQL_CREATE_ENTRIES(allStringAttrs, intAttrs, tableName, versionDatabase,
            primaryKey));
    }

    /**
     * Method for clear content of current Table
     */
    public void onClear() {
        onClear(this.currentVersion);
    }

    /**
     * Method for clear content of specific version
     * @param version : Integer represent version of database
     */
    public void onClear(int version) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(getSQL_DELETE_ENTRIES(dbName, version));
        onCreate(db, version);
    }

    /**
     * Method for upgrade table in database
     * @param db : sql connector linked to database
     * @param oldVersion : Integer represent old version
     * @param newVersion : Integer represent new version
     * @param tableName : Name of table
     */
    public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion,
                          @NonNull String tableName) {
        db.execSQL(getSQL_DELETE_ENTRIES(tableName, oldVersion));
        onCreate(db, newVersion);
    }

    /**
     * Method for create table in database
     * @param db : sql connector linked to database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        onCreate(db, this.currentVersion);
    }

    /**
     * Method for downgrade table in database
     * @param db : sql connector linked to database
     * @param oldVersion : Integer represent old version
     * @param newVersion : Integer represent new version
     */
    @Override
    public void onDowngrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
