package fr.iutnantes.miar.dndgenerator.api.tenor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class representing a filter per media within the tenor API.
 */
public class MediaFilter implements Serializable {

    public final String name;

    /**
     * Default constructor
     * @param name : String represent name of filter
     */
    public MediaFilter(@NonNull String name) {
        this.name = name;
    }

    /**
     * For write media filter.
     * @return toString
     */
    @NonNull
    @Override
    public String toString() {
        return name;
    }

    /**
     * For compare Any Object with this media filter.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }

    /**
     * For identify media filter.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public final static MediaFilter MINIMAL = new MediaFilter("minimal");
    public final static MediaFilter BASIC = new MediaFilter("basic");
    public final static MediaFilter DEFAULT = new MediaFilter("default");
    public final static List<MediaFilter> MinimalAndLower;
    public final static List<MediaFilter> BasicAndLower;
    static {
        List<MediaFilter> basicList = new ArrayList<>();
        basicList.add(BASIC);
        basicList.add(DEFAULT);
        BasicAndLower = basicList;
        List<MediaFilter> minimalList = new ArrayList<>();
        minimalList.add(MINIMAL);
        minimalList.addAll(BasicAndLower);
        MinimalAndLower = minimalList;
    }
    public final static List<MediaFilter> filters = MinimalAndLower;

    /**
     * For get random media filter from the API.
     * @return mediaFilter
     */
    @NonNull
    public static MediaFilter getRandomMediaFilter() {
        return getRandomMediaFilter(filters);
    }

    /**
     * For get random media filter from list provided.
     * @param filters : List where we’re going to shoot randomly
     * @return mediaFilter
     */
    @NonNull
    public static MediaFilter getRandomMediaFilter(@NonNull List<MediaFilter> filters) {
        return filters.get(new Random().nextInt(filters.size()));
    }

    /**
     * For get minimal media filter from list provided for reduce cost of request
     * @return mediaFilter
     */
    @Nullable
    public static MediaFilter getMinimalMediaFilter(@NonNull List<MediaFilter> filters) {
        if (filters.size() > 0)
            return filters.get(0);
        return null;
    }
}
