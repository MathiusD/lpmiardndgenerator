package fr.iutnantes.miar.dndgenerator.api.dnd;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * DnDSystem is a class representing the different D&D systems managed by API.
 */
public class DnDSystem implements Serializable, Comparable<DnDSystem> {

    public final String longName;
    public final String name;

    /**
     * Basic Constructor for create system if longName is missing.
     * @param name : String represent acronym of this system
     */
    public DnDSystem(@NonNull String name) {
        this(name, name);
    }

    /**
     * Basic Constructor for create system if all arguments are provided.
     * @param name : String represent acronym of this system
     * @param longName : String represent full name of this system
     */
    public DnDSystem(@NonNull String name, @NonNull String longName) {
        this.name = name;
        this.longName = longName;
    }

    /**
     * For write system.
     * @return toString
     */
    @NonNull
    @Override
    public String toString(){
        return String.format("System of %s", longName);
    }

    public static final List<DnDSystem> systems;
    public static final DnDSystem lbb = new DnDSystem("lbb",
            "Little Brown Books (1974 Original D&D)");
    public static final DnDSystem holmes = new DnDSystem("holmes", "Holmes D&D");
    public static final DnDSystem basic = new DnDSystem("basic",
            "Basic D&D (Mentzer / Moldvay)");
    public static final DnDSystem pahvelorn = new DnDSystem("pahvelorn",
            "Pahvelorn");
    public static final DnDSystem apollyon = new DnDSystem("apollyon", "Apollyon");
    public static final DnDSystem carcosa = new DnDSystem("carcosa", "Carcosa");
    public static final DnDSystem moc = new DnDSystem("moc", "Masters of Carcosa!");
    public static final DnDSystem dd = new DnDSystem("dd", "Delving Deeper");
    public static final DnDSystem lotfp = new DnDSystem("lotfp",
            "Lamentations Of The Flame Princess");
    public static final DnDSystem anyone = new DnDSystem("anyone", "Any");
    static {
        List<DnDSystem> sys = new ArrayList<>();
        sys.add(lbb);
        sys.add(holmes);
        sys.add(basic);
        sys.add(pahvelorn);
        sys.add(apollyon);
        sys.add(carcosa);
        sys.add(moc);
        sys.add(dd);
        sys.add(lotfp);
        Collections.sort(sys, DnDSystem::compareTo);
        systems = sys;
    }

    /**
     * For get random system from the API.
     * @return system
     */
    public static DnDSystem getRandomSystem() {
        return getRandomSystem(systems);
    }

    /**
     * For get random system from list provided.
     * @param systems : List where we’re going to shoot randomly
     * @return system
     */
    public static DnDSystem getRandomSystem(@NonNull List<DnDSystem> systems) {
        return systems.get(new Random().nextInt(systems.size()));
    }

    /**
     * For get random system exclude param if present
     * @param system : System that we want to exclude from our circulation
     * @return system
     */
    public static DnDSystem getRandomSystemExcludingSystem(@NonNull DnDSystem system) {
        if (systems.contains(system)) {
            List<DnDSystem> tempSystems = new ArrayList<>(systems);
            tempSystems.remove(system);
            return getRandomSystem(tempSystems);
        }
        return getRandomSystem();
    }

    /**
     * For identify system.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return longName.hashCode();
    }

    /**
     * For compare Any Object with this system.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }

    /**
     * For compare 2 system
     * @param o : Object for comparison
     * @return compareToResult
     */
    @Override
    public int compareTo(DnDSystem o) {
        return this.longName.compareTo(o.longName);
    }
}
