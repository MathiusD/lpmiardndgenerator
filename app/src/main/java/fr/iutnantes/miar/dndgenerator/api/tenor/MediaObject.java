package fr.iutnantes.miar.dndgenerator.api.tenor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Class representing a media provided by the tenor API.
 */
public class MediaObject implements Serializable {

    public final String preview;
    public final String url;
    public final Dimension dims;
    public final Integer size;
    public final Float duration;

    /**
     * Default constructor with Dimension instantiation with arguments.
     * @param preview : String represent uri for preview of this media
     * @param url : String represent uri for this media
     * @param width : Integer represent width of this media
     * @param height : Integer represent height of this media
     * @param size : Integer represent size of this media
     * @param duration : Float represent duration of this media
     */
    public MediaObject(@NonNull String preview, @NonNull String url, @Nullable Integer width,
                       @Nullable Integer height, @Nullable Integer size,
                       @Nullable Float duration) {
        this.preview = preview;
        this.url = url;
        if (width != null && height != null)
            this.dims = new Dimension(width, height);
        else
            this.dims = null;
        this.size = size;
        this.duration = duration;
    }

    /**
     * Default constructor with Dimension instantiation with argument.
     * @param preview : String represent uri for preview of this media
     * @param url : String represent uri for this media
     * @param rawDims : List of Integer represent data exploited by Dimension instantiation
     * @param size : Integer represent size of this media
     * @param duration : Float represent duration of this media
     */
    public MediaObject(@NonNull String preview, @NonNull String url, @Nullable List<Integer> rawDims,
                       @Nullable Integer size, @Nullable Float duration) {
        this.preview = preview;
        this.url = url;
        if (rawDims != null)
            this.dims = new Dimension(rawDims);
        else
            this.dims = null;
        this.size = size;
        this.duration = duration;
    }

    /**
     * Default constructor.
     * @param preview : String represent uri for preview of this media
     * @param url : String represent uri for this media
     * @param dims : Dimensions of this media
     * @param size : Integer represent size of this media
     * @param duration : Float represent duration of this media
     */
    public MediaObject(@NonNull String preview, @NonNull String url, @Nullable Dimension dims,
                       @Nullable Integer size, @Nullable Float duration) {
        this.preview = preview;
        this.url = url;
        this.dims = dims;
        this.size = size;
        this.duration = duration;
    }

    /**
     * For write media object.
     * @return toString
     */
    @NonNull
    @Override
    public String toString() {
        return url;
    }

    /**
     * For compare Any Object with this media object.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }

    /**
     * For identify media object.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(25, 27)
            .append(url.hashCode()).append(preview.hashCode()).append(dims.hashCode())
            .toHashCode();
    }
}
