package fr.iutnantes.miar.dndgenerator.api.dnd;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * DnDSaves is a class that represents the character's backup rolls, which represents his resistance
 * to certain types of states.
 */
public class DnDSaves implements Serializable {

    public final Integer wands, breath, stone, magic, poison;
    public static final String NO_SAVES ="No saves provided.";

    /**
     * Basic Constructor for create saves if all arguments are provided.
     * @param wands : Integer represent resistance to wands
     * @param breath : Integer represent resistance to breath
     * @param stone : Integer represent resistance to stone
     * @param magic : Integer represent resistance to magic
     * @param poison : Integer represent resistance to poison
     */
    public DnDSaves(@Nullable Integer wands, @Nullable Integer breath, @Nullable Integer stone,
                    @Nullable Integer magic, @Nullable Integer poison) {
        this.wands = wands;
        this.breath = breath;
        this.stone = stone;
        this.magic = magic;
        this.poison = poison;
    }

    /**
     * For write saves.
     * @return toString
     */
    @Override
    @NonNull
    public String toString() {
        String out = "";
        String[] labels = new String[]{"wands", "breath", "stone", "magic", "poison"};
        Integer[] value = new Integer[]{wands, breath, stone, magic, poison};
        for (int i = 0; i < 5; i ++)
            if (value[i] != null)
                out = String.format("%s%s%s : %d", out, !out.equals("") ? String.format("%n") : "",
                        labels[i], value[i]);
        return !out.equals("") ? out : NO_SAVES;
    }

    /**
     * For identify saves.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(5, 7)
            .append(wands != null ? wands.hashCode() : 1)
            .append(breath != null ? breath.hashCode() : 1)
            .append(stone != null ? stone.hashCode() : 1)
            .append(magic != null ? magic.hashCode() : 1)
            .append(poison != null ? poison.hashCode() : 1).toHashCode();
    }

    /**
     * For compare Any Object with this saves.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }
}
