package fr.iutnantes.miar.dndgenerator.api.tenor;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.SimpleFuture;
import com.koushikdutta.ion.Ion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.iutnantes.miar.dndgenerator.BuildConfig;

/**
 * Class representing a GIF provided by the tenor API.
 */
public class GIFObject implements Serializable {

    public static final String apiProvider = "https://api.tenor.com/v1/";
    protected static final String TAG = "GIFObject";
    public static final Integer DEFAULT_SIZE = 8;
    public static final String NOT_FOUND = "not-found";
    public static final Integer MAX_SIZE = 50;
    public static final String UNNAMED = "Unnamed";

    public final Float created;
    public final Boolean hasAudio;
    public final String id;
    public final Map<GIFFormat, MediaObject> media;
    public final List<String> tags;
    public final String title;
    public final String itemUrl;
    public final Boolean hasCaption;
    public final String url;
    public final FilterContent filterContent;
    public final MediaFilter mediaFilter;
    public final String searchQuery;
    public final GIFArtifact artifact;
    public final TenorArgs tenorArgs;

    /**
     * Basic Constructor.
     * @param created : Float represent TimeStamp of creation of this gif
     * @param hasAudio : Boolean represent if gif has audio
     * @param id : Id of this gif
     * @param media : List of media linked to this gif
     * @param tags : List of tags linked to this gif
     * @param title : String represent title of this gif
     * @param itemUrl : Uri of this gif for user (For assets see uri in media)
     * @param hasCaption : Boolean represent if gif has caption
     * @param url : Short Uri for itemUrl
     * @param filterContent : FilterContent apply in search
     * @param mediaFilter : MediaFilter apply in search
     * @param searchQuery : Query to find this gif
     */
    protected GIFObject(@NonNull Float created, @NonNull Boolean hasAudio, @NonNull String id,
                      @NonNull Map<GIFFormat, MediaObject> media, @NonNull List<String> tags,
                      @NonNull String title, @NonNull String itemUrl, @Nullable Boolean hasCaption,
                      @NonNull String url, @NonNull FilterContent filterContent,
                      @NonNull MediaFilter mediaFilter, @NonNull String searchQuery) {
        this.created = created;
        this.hasAudio = hasAudio;
        this.id = id;
        this.media = media;
        this.tags = tags;
        this.title = title;
        this.itemUrl = itemUrl;
        this.hasCaption = hasCaption;
        this.url = url;
        this.filterContent = filterContent;
        this.mediaFilter = mediaFilter;
        this.searchQuery = searchQuery;
        this.artifact = new GIFArtifact(this.searchQuery, this.mediaFilter, this.filterContent);
        this.tenorArgs = new TenorArgs(this.artifact);
    }


    /**
     * For write gif.
     * @return toString
     */
    @NonNull
    @Override
    public String toString() {
        return !title.equals("") ? title : GIFObject.UNNAMED;
    }

    /**
     * For compare Any Object with this gif.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }

    /**
     * For identify media object.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return id.hashCode();
    }

    /**
     * Method for get GIF by id
     * @param ctx : Context for make request for gif
     * @param id : Id of search GIF
     * @param artifact : artifact save for last instantiation of this gif
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObjectById(@NonNull Context ctx, @NonNull String id,
                                             @NonNull GIFArtifact artifact) {
        try {
            return getFutureGIFObjectById(ctx, id, artifact)
                    .get();
        } catch (ExecutionException | InterruptedException e) {
            return getGIFObjectById(ctx, id, artifact);
        }

    }

    /**
     * Method for get Future GIF by id
     * @param ctx : Context for make request for gif
     * @param id : Id of search GIF
     * @param artifact : artifact save for last instantiation of this gif
     * @return future
     */
    @NonNull
    public static Future<GIFObject> getFutureGIFObjectById(@NonNull Context ctx,
                                                           @NonNull String id,
                                                           @NonNull GIFArtifact artifact) {
        SimpleFuture<GIFObject> future = new SimpleFuture<>();
        getFutureGIFObjectById(ctx, id, artifact, future);
        return future;
    }

    /**
     * Method performing the api query and will place the result in the future past as argument.
     * @param ctx : Context for make request for gif
     * @param id : Id of search GIF
     * @param artifact : artifact save for last instantiation of this gif
     * @param future : SimpleFuture represents the object in which the result will be stored
     */
    public static void getFutureGIFObjectById(@NonNull Context ctx, @NonNull String id,
                                              @NonNull GIFArtifact artifact,
                                              @NonNull SimpleFuture<GIFObject> future) {
        String target = String.format("%sgifs?key=%s&ids=%s", apiProvider,
            BuildConfig.TENOR_KEY, id);
        Ion.with(ctx).load(target)
            .setLogging(String.format("%s-Ion", TAG), Log.DEBUG)
            .asJsonObject().withResponse().setCallback((e, response) -> {
                Log.d(TAG,
                        String.format("Request submit and response receveid !%nProcessing...")
                );
                if (response != null && (response.getHeaders().code() == 200 ||
                        response.getHeaders().code() == 202)) {
                    GIFObject gif = null;
                    if(!response.getResult().has("error"))
                        for (JsonElement elem: response.getResult().get("results").getAsJsonArray()) {
                            gif = extractGIFObjectFromJsonElement(elem, artifact);
                            Log.d(TAG, "Result set in OwnFuture");
                            future.setComplete(gif);
                            return;
                        }
                    Log.d(TAG, "Result not found and set null in OwnFuture");
                    future.setComplete(gif);
                } else {
                    Log.wtf(TAG, response != null ?
                            String.format("Error code %d for %s!, retrying...",
                                    response.getHeaders().code(), target):
                            String.format("Null response provided for %s", target)
                    );
                    getFutureGIFObjectById(ctx, id, artifact, future);
                }
            }
        );
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query) {
        return getGIFObject(ctx, query, FilterContent.DEFAULT);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param tenorArgs : TenorArgs for represent all arguments for research (exclude nb because
     *                  this method research 1 gif)
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @NonNull TenorArgs tenorArgs) {
        return getGIFObject(ctx, query, tenorArgs.mediaFilter, tenorArgs.filterContent,
            tenorArgs.exclude);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @NonNull GIFFormat gifFormat) {
        return getGIFObject(ctx, query, gifFormat, FilterContent.DEFAULT);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @NonNull MediaFilter mediaFilter) {
        return getGIFObject(ctx, query, mediaFilter, FilterContent.DEFAULT);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param filterContent : filter content apply in search
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @NonNull FilterContent filterContent) {
        return getGIFObject(ctx, query, MediaFilter.DEFAULT, filterContent);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param excludeGIF : gif you don’t want to see
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @Nullable GIFObject excludeGIF) {
        return getGIFObject(ctx, query, MediaFilter.DEFAULT, excludeGIF);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param filterContent : filter content apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @NonNull FilterContent filterContent,
                                         @Nullable GIFObject excludeGIF) {
        return getGIFObject(ctx, query, MediaFilter.DEFAULT, filterContent,
                excludeGIF);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param excludeGIF : gif you don’t want to see
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @NonNull GIFFormat gifFormat,
                                         @Nullable GIFObject excludeGIF) {
        return getGIFObject(ctx, query, gifFormat.getMinimalMediaFilter(), FilterContent.DEFAULT,
            excludeGIF);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @NonNull GIFFormat gifFormat,
                                         @NonNull FilterContent filterContent) {
        return getGIFObject(ctx, query, gifFormat.getMinimalMediaFilter(), filterContent,
            null);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @NonNull MediaFilter mediaFilter,
                                         @Nullable GIFObject excludeGIF) {
        return getGIFObject(ctx, query, mediaFilter, FilterContent.DEFAULT, excludeGIF);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @NonNull MediaFilter mediaFilter,
                                         @NonNull FilterContent filterContent) {
        return getGIFObject(ctx, query, mediaFilter, filterContent, null);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @NonNull GIFFormat gifFormat,
                                         @NonNull FilterContent filterContent,
                                         @Nullable GIFObject excludeGIF) {
        return getGIFObject(ctx, query, gifFormat.getMinimalMediaFilter(), filterContent,
            excludeGIF);
    }

    /**
     * Method for search one GIF.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return gif
     */
    @NonNull
    public static GIFObject getGIFObject(@NonNull Context ctx, @NonNull String query,
                                         @Nullable MediaFilter mediaFilter,
                                         @NonNull FilterContent filterContent,
                                         @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, mediaFilter, filterContent, 1, excludeGIF).get(0);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param tenorArgs : TenorArgs for represent all arguments for research (exclude nb because
     *                  this method research 1 gif)
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull TenorArgs tenorArgs) {
        return getGIFObjects(ctx, query, tenorArgs.mediaFilter, tenorArgs.filterContent,
            tenorArgs.nb, tenorArgs.exclude);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query) {
        return getGIFObjects(ctx, query, DEFAULT_SIZE);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param nb : nb of result you wish
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull Integer nb) {
        return getGIFObjects(ctx, query, FilterContent.DEFAULT, nb);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param filterContent : filter content apply in search
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull FilterContent filterContent) {
        return getGIFObjects(ctx, query, filterContent, DEFAULT_SIZE);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull GIFFormat gifFormat) {
        return getGIFObjects(ctx, query, gifFormat, DEFAULT_SIZE);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, DEFAULT_SIZE, excludeGIF);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull MediaFilter mediaFilter) {
        return getGIFObjects(ctx, query, mediaFilter, DEFAULT_SIZE);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param nb : nb of result you wish
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull GIFFormat gifFormat,
                                                @NonNull Integer nb) {
        return getGIFObjects(ctx, query, gifFormat, FilterContent.DEFAULT, nb);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull Integer nb,
                                                @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, FilterContent.DEFAULT, nb, excludeGIF);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param nb : nb of result you wish
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull MediaFilter mediaFilter,
                                                @NonNull Integer nb) {
        return getGIFObjects(ctx, query, mediaFilter, FilterContent.DEFAULT, nb);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param filterContent : filter content apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull FilterContent filterContent,
                                                @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, MediaFilter.DEFAULT, filterContent, excludeGIF);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull FilterContent filterContent,
                                                @NonNull Integer nb) {
        return getGIFObjects(ctx, query, MediaFilter.DEFAULT, filterContent, nb);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull GIFFormat gifFormat,
                                                @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, gifFormat, DEFAULT_SIZE, excludeGIF);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull GIFFormat gifFormat,
                                                @NonNull FilterContent filterContent) {
        return getGIFObjects(ctx, query, gifFormat, filterContent, DEFAULT_SIZE);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull MediaFilter mediaFilter,
                                                @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, mediaFilter, DEFAULT_SIZE, excludeGIF);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull MediaFilter mediaFilter,
                                                @NonNull FilterContent filterContent) {
        return getGIFObjects(ctx, query, mediaFilter, filterContent, DEFAULT_SIZE);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull FilterContent filterContent,
                                                @NonNull Integer nb,
                                                @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, MediaFilter.DEFAULT, filterContent,
                nb, excludeGIF);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull GIFFormat gifFormat,
                                                @NonNull Integer nb,
                                                @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, gifFormat.getMinimalMediaFilter(), FilterContent.DEFAULT,
            nb, excludeGIF);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull GIFFormat gifFormat,
                                                @NonNull FilterContent filterContent,
                                                @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, gifFormat.getMinimalMediaFilter(), filterContent,
            DEFAULT_SIZE, excludeGIF);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull GIFFormat gifFormat,
                                                @NonNull FilterContent filterContent,
                                                @NonNull Integer nb) {
        return getGIFObjects(ctx, query, gifFormat.getMinimalMediaFilter(), filterContent, nb);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull MediaFilter mediaFilter,
                                                @NonNull Integer nb,
                                                @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, mediaFilter, FilterContent.DEFAULT, nb, excludeGIF);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @Nullable MediaFilter mediaFilter,
                                                @NonNull FilterContent filterContent,
                                                @NonNull Integer nb) {
        return getGIFObjects(ctx, query, mediaFilter, filterContent, nb, null);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull MediaFilter mediaFilter,
                                                @NonNull FilterContent filterContent,
                                                @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, mediaFilter, filterContent, DEFAULT_SIZE, excludeGIF);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @NonNull GIFFormat gifFormat,
                                                @NonNull FilterContent filterContent,
                                                @NonNull Integer nb,
                                                @Nullable GIFObject excludeGIF) {
        return getGIFObjects(ctx, query, gifFormat.getMinimalMediaFilter(), filterContent, nb,
            excludeGIF);
    }

    /**
     * Method for search GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return gifs
     */
    @NonNull
    public static List<GIFObject> getGIFObjects(@NonNull Context ctx, @NonNull String query,
                                                @Nullable MediaFilter mediaFilter,
                                                @NonNull FilterContent filterContent,
                                                @NonNull Integer nb,
                                                @Nullable GIFObject excludeGIF) {
        try {
            return getFuturesGIFObjects(ctx, query, mediaFilter, filterContent, nb, excludeGIF)
                    .get();
        } catch (ExecutionException | InterruptedException e) {
            return getGIFObjects(ctx, query, mediaFilter, filterContent, nb, excludeGIF);
        }
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param tenorArgs : TenorArgs for represent all arguments for research (exclude nb because
     *                  this method research 1 gif)
     * @return futures
     */
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull TenorArgs tenorArgs) {
        return getFuturesGIFObjects(ctx, query, tenorArgs.mediaFilter, tenorArgs.filterContent,
            tenorArgs.nb, tenorArgs.exclude);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query) {
        return getFuturesGIFObjects(ctx, query, FilterContent.DEFAULT, DEFAULT_SIZE);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param filterContent : filter content apply in search
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull FilterContent
                                                                   filterContent) {
        return getFuturesGIFObjects(ctx, query, filterContent, DEFAULT_SIZE);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param nb : nb of result you wish
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull Integer nb) {
        return getFuturesGIFObjects(ctx, query, FilterContent.DEFAULT, nb);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull GIFFormat gifFormat) {
        return getFuturesGIFObjects(ctx, query, gifFormat, DEFAULT_SIZE);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull MediaFilter mediaFilter) {
        return getFuturesGIFObjects(ctx, query, mediaFilter, DEFAULT_SIZE);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @Nullable GIFObject excludeGIF) {
        return getFuturesGIFObjects(ctx, query, DEFAULT_SIZE, excludeGIF);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull GIFFormat gifFormat,
                                                               @NonNull FilterContent
                                                                       filterContent) {
        return getFuturesGIFObjects(ctx, query, gifFormat, filterContent, DEFAULT_SIZE);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull MediaFilter mediaFilter,
                                                               @NonNull FilterContent
                                                                   filterContent) {
        return getFuturesGIFObjects(ctx, query, mediaFilter, filterContent, DEFAULT_SIZE,
            null);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param filterContent : filter content apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull FilterContent filterContent,
                                                               @Nullable GIFObject excludeGIF) {
        return getFuturesGIFObjects(ctx, query, MediaFilter.DEFAULT, filterContent, DEFAULT_SIZE,
            excludeGIF);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull FilterContent filterContent,
                                                               @NonNull Integer nb) {
        return getFuturesGIFObjects(ctx, query, MediaFilter.DEFAULT, filterContent, nb,
            null);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull GIFFormat gifFormat,
                                                               @Nullable GIFObject excludeGIF) {
        return getFuturesGIFObjects(ctx, query, gifFormat, FilterContent.DEFAULT, DEFAULT_SIZE,
            excludeGIF);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param nb : nb of result you wish
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull GIFFormat gifFormat,
                                                               @NonNull Integer nb) {
        return getFuturesGIFObjects(ctx, query, gifFormat, FilterContent.DEFAULT, nb,
            null);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull Integer nb,
                                                               @Nullable GIFObject excludeGIF) {
        return getFuturesGIFObjects(ctx, query, MediaFilter.DEFAULT, FilterContent.DEFAULT, nb,
                excludeGIF);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull MediaFilter mediaFilter,
                                                               @Nullable GIFObject excludeGIF) {
        return getFuturesGIFObjects(ctx, query, mediaFilter, FilterContent.DEFAULT, DEFAULT_SIZE,
            excludeGIF);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param nb : nb of result you wish
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull MediaFilter mediaFilter,
                                                               @NonNull Integer nb) {
        return getFuturesGIFObjects(ctx, query, mediaFilter, FilterContent.DEFAULT, nb,
            null);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull FilterContent filterContent,
                                                               @NonNull Integer nb,
                                                               @Nullable GIFObject excludeGIF) {
        return getFuturesGIFObjects(ctx, query, MediaFilter.DEFAULT,
                filterContent, nb, excludeGIF);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull MediaFilter mediaFilter,
                                                               @NonNull Integer nb,
                                                               @Nullable GIFObject excludeGIF) {
        return getFuturesGIFObjects(ctx, query, mediaFilter, FilterContent.DEFAULT, nb, excludeGIF);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull GIFFormat gifFormat,
                                                               @NonNull Integer nb,
                                                               @Nullable GIFObject excludeGIF) {
        return getFuturesGIFObjects(ctx, query, gifFormat.getMinimalMediaFilter(),
                FilterContent.DEFAULT, nb, excludeGIF);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull MediaFilter mediaFilter,
                                                               @NonNull FilterContent filterContent,
                                                               @Nullable GIFObject excludeGIF) {
        return getFuturesGIFObjects(ctx, query, mediaFilter, filterContent, DEFAULT_SIZE,
            excludeGIF);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull GIFFormat gifFormat,
                                                               @NonNull FilterContent filterContent,
                                                               @Nullable GIFObject excludeGIF) {
        return getFuturesGIFObjects(ctx, query, gifFormat.getMinimalMediaFilter(),
                filterContent, DEFAULT_SIZE, excludeGIF);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull GIFFormat gifFormat,
                                                               @NonNull FilterContent filterContent,
                                                               @NonNull Integer nb) {
        return getFuturesGIFObjects(ctx, query, gifFormat.getMinimalMediaFilter(),
            filterContent, nb, null);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @NonNull GIFFormat gifFormat,
                                                               @NonNull FilterContent filterContent,
                                                               @NonNull Integer nb,
                                                               @Nullable GIFObject excludeGIF) {
        return getFuturesGIFObjects(ctx, query, gifFormat.getMinimalMediaFilter(),
                filterContent, nb, excludeGIF);
    }

    /**
     * Method for search futures GIFs.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @return futures
     */
    @NonNull
    public static Future<List<GIFObject>> getFuturesGIFObjects(@NonNull Context ctx,
                                                               @NonNull String query,
                                                               @Nullable MediaFilter mediaFilter,
                                                               @NonNull FilterContent filterContent,
                                                               @NonNull Integer nb,
                                                               @Nullable GIFObject excludeGIF) {
        SimpleFuture<List<GIFObject>> future = new SimpleFuture<>();
        getFuturesGIFObjects(ctx, query, mediaFilter != null ? mediaFilter : MediaFilter.DEFAULT,
            filterContent, nb, excludeGIF, future);
        return future;
    }

    /**
     * Method performing the api query and will place the result in the futures past as argument.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @param future : SimpleFuture represents the object in which the result will be stored
     */
    public static void getFuturesGIFObjects(@NonNull Context ctx, @NonNull String query,
                                            @NonNull MediaFilter mediaFilter,
                                            @NonNull FilterContent filterContent,
                                            @NonNull Integer nb,
                                            @Nullable GIFObject excludeGIF,
                                            @NonNull SimpleFuture<List<GIFObject>> future) {
        getFuturesGIFObjects(ctx, query, mediaFilter, filterContent, nb, excludeGIF,
            0, future);
    }

    /**
     * Method performing the api query and will place the result in the future past as argument.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @param startAt : Integer represent at which index the query should start
     * @param future : SimpleFuture represents the object in which the result will be stored
     */
    public static void getFuturesGIFObjects(@NonNull Context ctx, @NonNull String query,
                                            @NonNull MediaFilter mediaFilter,
                                            @NonNull FilterContent filterContent,
                                            @NonNull Integer nb,
                                            @Nullable GIFObject excludeGIF,
                                            @NonNull Integer startAt,
                                            @NonNull SimpleFuture<List<GIFObject>> future) {
        getFuturesGIFObjects(ctx, query, mediaFilter, filterContent, nb, excludeGIF, startAt,
                null, -1, future);
    }

    /**
     * Method performing the api query and will place the result in the future past as argument.
     * @param ctx : Context for make request for gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param excludeGIF : gif you don’t want to see
     * @param listData : List of gifs found
     * @param startAt : Integer represent at which index the query should start
     * @param future : SimpleFuture represents the object in which the result will be stored
     */
    public static void getFuturesGIFObjects(@NonNull Context ctx, @NonNull String query,
                                            @NonNull MediaFilter mediaFilter,
                                            @NonNull FilterContent filterContent,
                                            @NonNull Integer nb,
                                            @Nullable GIFObject excludeGIF,
                                            @NonNull Integer startAt,
                                            @Nullable List<GIFObject> listData,
                                            @NonNull Integer indexQuery,
                                            @NonNull SimpleFuture<List<GIFObject>> future) {
        String baseQuery = query.replace("\"", "\\\"")
            .replace("\'", "\\\'")
            .replace(" ,", ",").replace(", ", ",")
            .replace(",","+").replace(" ", "-");
        String[] splitedQuery = baseQuery.split("\\+");
        String[] splitedQueryLastChance = baseQuery.split("[+-]");
        String target = String.format("%ssearch?key=%s&q=\"%s\"&limit=%d&contentfilter=%s%s%s",
            apiProvider, BuildConfig.TENOR_KEY,
            indexQuery == -1 ? baseQuery : indexQuery < splitedQuery.length ?
                splitedQuery[indexQuery] : indexQuery <  splitedQuery.length +
                    splitedQueryLastChance.length ? splitedQueryLastChance[indexQuery -
                        splitedQuery.length] : NOT_FOUND,
            nb <= MAX_SIZE ? nb : MAX_SIZE, filterContent.name,
            mediaFilter != MediaFilter.DEFAULT ?
                String.format("&media_filter=%s", mediaFilter.name) : "",
            startAt > 0 ?
                String.format("&pos=%d", startAt) : ""
        );
        Ion.with(ctx).load(target)
            .setLogging(String.format("%s-Ion", TAG), Log.DEBUG)
            .asJsonObject().withResponse().setCallback((e, response) -> {
                Log.d(TAG,
                    String.format("Request submit and response receveid !%nProcessing...")
                );
                if (response != null && (response.getHeaders().code() == 200 ||
                    response.getHeaders().code() == 202)) {
                    int found = 0;
                    List<GIFObject> gifs = listData == null ? new ArrayList<>() : listData;
                    for (JsonElement elem: response.getResult().get("results").getAsJsonArray()) {
                        GIFObject gif = extractGIFObjectFromJsonElement(elem, filterContent,
                            mediaFilter, query);
                        if (!gif.equals(excludeGIF)) {
                            gifs.add(gif);
                            found += 1;
                        }
                    }
                    int skipped = ((nb > MAX_SIZE ? MAX_SIZE : nb) - found);
                    if (nb > MAX_SIZE)
                        getFuturesGIFObjects(ctx, query, mediaFilter, filterContent,
                            (nb - MAX_SIZE) + skipped, excludeGIF,
                            found != MAX_SIZE ? 0 : ((nb - MAX_SIZE - 1) + startAt) - skipped,
                            gifs, found != MAX_SIZE ? indexQuery + 1 : indexQuery,
                            future);
                    else {
                        if (skipped > 0)
                            getFuturesGIFObjects(ctx, query, mediaFilter, filterContent,
                                skipped, excludeGIF,
                                found != nb ? 0 : ((nb - 1) + startAt) - skipped, gifs,
                                found != nb ? indexQuery + 1 : indexQuery,
                                future);
                        else {
                            future.setComplete(gifs);
                            Log.d(TAG, "Result set in OwnFuture");
                        }
                    }
                } else {
                    Log.wtf(TAG, response != null ?
                            String.format("Error code %d for %s!, retrying...",
                                    response.getHeaders().code(), target):
                            String.format("Null response provided for %s", target)
                    );
                    getFuturesGIFObjects(ctx, query, mediaFilter, filterContent, nb, excludeGIF,
                        startAt, listData, indexQuery, future);
                }
            }
        );
    }

    /**
     * Method for extraction of jsonElement contains data of gif
     * @param elem : JsonElement with data of gif
     * @param artifact : Artifact contains data linked to gif
     * @return gif
     */
    @NonNull
    public static GIFObject extractGIFObjectFromJsonElement(@NonNull JsonElement elem,
                                                            @Nullable GIFArtifact artifact) {
        artifact = artifact != null ? artifact : new GIFArtifact();
        return extractGIFObjectFromJsonElement(elem, artifact.filterContent, artifact.mediaFilter,
            artifact.query);
    }

    /**
     * Method for extraction of jsonElement contains data of gif
     * @param elem : JsonElement with data of gif
     * @param query : String represent base argument for research
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @return gif
     */
    @NonNull
    public static GIFObject extractGIFObjectFromJsonElement(@NonNull JsonElement elem,
                                                            @NonNull FilterContent filterContent,
                                                            @NonNull MediaFilter mediaFilter,
                                                            @NonNull String query) {
        JsonElement jsonNull = JsonNull.INSTANCE;
        JsonObject data = elem.getAsJsonObject();
        Map<GIFFormat, MediaObject> tempMedia = new HashMap<>();
        for(JsonElement ele: data.getAsJsonArray("media")) {
            for (Map.Entry<String, JsonElement> el: ele.getAsJsonObject()
                    .entrySet()) {
                JsonObject dat = el.getValue().getAsJsonObject();
                JsonArray dims = dat.get("dims") != null
                        && dat.get("dims") != jsonNull ?
                        dat.get("dims").getAsJsonArray() : null;
                String keyName = el.getKey();
                GIFFormat key = GIFFormat.get(keyName);
                tempMedia.put(
                        key != null ? key : new GIFFormat(keyName),
                        new MediaObject(
                                dat.get("preview").getAsString(),
                                dat.get("url").getAsString(),
                                dims != null ? dims.get(0).getAsInt() : null,
                                dims != null ? dims.get(1).getAsInt() : null,
                                dat.get("size") != jsonNull && dat.get("size") != null ?
                                        dat.get("size").getAsInt() : null,
                                dat.get("duration") != jsonNull
                                        && dat.get("duration") != null ?
                                        dat.get("duration").getAsFloat() : null
                        )
                );
            }
        }
        List<String> tags = new ArrayList<>();
        for (JsonElement element : data.getAsJsonArray("tags")) {
            tags.add(element.getAsString());
        }
        return new GIFObject(
                data.get("created").getAsFloat(),
                data.get("hasaudio").getAsBoolean(),
                data.get("id").getAsString(),
                tempMedia,
                tags,
                data.get("title").getAsString(),
                data.get("itemurl").getAsString(),
                data.get("hascaption") != jsonNull &&
                        data.get("hascaption") != null ? data.get("hascaption").
                        getAsBoolean() : null,
                data.get("url").getAsString(),
                filterContent, mediaFilter, query
        );
    }
}
