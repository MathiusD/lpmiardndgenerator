package fr.iutnantes.miar.dndgenerator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.iutnantes.miar.dndgenerator.adapter.ListResultsAdapter;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDClass;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSheet;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSystem;

public class HomeActivity extends AppCompatActivity {
    protected static String TAG = "HomeActivity :: ";
    private static Bundle m_savedInstanceState;
    private static List<DnDSheet> m_favourites;
    public static final String SYSTEM_KEY = "system";
    public static final String CLASS_KEY = "class";
    public static final String NUMBER_RESULT = "nbResult";
    Spinner spnr_home_system, spnr_home_class;
    EditText txt_home_nbResult;
    SeekBar skbr_home_nbResult;
    Button btn_home_generate, btn_home_delFav;
    RadioButton rdbtn_home_bySystem, rdbtn_home_byClass;
    RecyclerView rclr_home_favourites;
    private ListResultsAdapter m_adapter;
    private static ProgressDialog m_progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home);

        m_savedInstanceState = savedInstanceState;

        // Initialising elements
        spnr_home_system = findViewById(R.id.spnr_home_system);
        spnr_home_class = findViewById(R.id.spnr_home_class);
        txt_home_nbResult = findViewById(R.id.txt_home_nbResult);
        skbr_home_nbResult = findViewById(R.id.skbr_home_nbResult);
        btn_home_generate = findViewById(R.id.btn_home_generate);
        btn_home_delFav = findViewById(R.id.btn_home_delFav);
        rdbtn_home_bySystem = findViewById(R.id.rdbtn_home_bySystem);
        rdbtn_home_byClass = findViewById(R.id.rdbtn_home_byClass);
        rclr_home_favourites = findViewById(R.id.rclr_home_favourites);

        List<String> v_lstSystemName = new ArrayList<>();
        v_lstSystemName.add("Any");
        for (DnDSystem system : DnDSystem.systems) {
            v_lstSystemName.add(system.longName);
        }

        ArrayAdapter<String> v_adapterSystem = new ArrayAdapter<String>(
                this, R.layout.layout_pattern_spinner, v_lstSystemName);
        v_adapterSystem.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spnr_home_system.setAdapter(v_adapterSystem);

        List<String> v_lstClassName = new ArrayList<>();
        v_lstClassName.add("Any");
        for (DnDClass v_dnDClass : DnDClass.classes) {
            v_lstClassName.add(v_dnDClass.name);
        }
        ArrayAdapter<String> v_adapterClass = new ArrayAdapter<String>(
                this, R.layout.layout_pattern_spinner, v_lstClassName);
        v_adapterClass.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spnr_home_class.setAdapter(v_adapterClass);

        skbr_home_nbResult.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                txt_home_nbResult.setText(Integer.toString(i+1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btn_home_generate.setOnClickListener(view -> {
            int v_nbResult = 0;
            try {
                v_nbResult = Integer.parseInt(txt_home_nbResult.getText().toString());
            }
            catch (Exception e) {
                Log.d(TAG, e.getMessage());
                Toast.makeText(this, "'Nb Result' must be an integer !", Toast.LENGTH_LONG).show();
                return;
            }
            if (v_nbResult == 0) {
                Toast.makeText(this, "The 'Nb Result' must be an integer greater than '0'", Toast.LENGTH_LONG).show();
                return;
            }
            if(v_nbResult > 100) {
                Toast.makeText(this, "Please not more than '100' results, it's too many !", Toast.LENGTH_LONG).show();
                return;
            }
            Intent v_intent = new Intent(HomeActivity.this,
                    ResearchResultsActivity.class);
            v_intent.putExtra(SYSTEM_KEY, spnr_home_system.getSelectedItemPosition() - 1);
            v_intent.putExtra(CLASS_KEY, spnr_home_class.getSelectedItemPosition() - 1);
            v_intent.putExtra(NUMBER_RESULT, v_nbResult);
            startActivity(v_intent);
        });

        btn_home_delFav.setOnClickListener(onClickDelFav());

        View.OnClickListener v_rdbtnListener = view -> {
            sortFavourites();
            m_adapter.notifyDataSetChanged();
        };
        rdbtn_home_bySystem.setOnClickListener(v_rdbtnListener);
        rdbtn_home_byClass.setOnClickListener(v_rdbtnListener);


        m_favourites = DnDSheet.getFavs(this);

        m_adapter = new ListResultsAdapter(m_favourites, HomeActivity.this);
        rclr_home_favourites.setAdapter(m_adapter);
        rclr_home_favourites.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        AsyncTaskGetFavourites v_asyncTaskGetFavourites = new AsyncTaskGetFavourites();
        v_asyncTaskGetFavourites.execute((Nullable) null);
    }

    private void getFav() {
        m_favourites.clear();
        m_favourites.addAll(DnDSheet.getFavs(this));
        sortFavourites();
    }

    private void sortFavourites() {
        if(rdbtn_home_bySystem.isChecked()) {
            Collections.sort(m_favourites, DnDSheet.COMPARATOR_BY_SYSTEM);
        }
        else {
            Collections.sort(m_favourites, DnDSheet.COMPARATOR_BY_CLASS);
        }
    }

    private View.OnClickListener onClickDelFav() {
        View.OnClickListener v_onClickListener = view -> {
            AlertDialog.Builder v_builder = new AlertDialog.Builder(HomeActivity.this);
            v_builder.setMessage("Do you realy what to delete all favourites ?");
            v_builder.setPositiveButton("OK", (dialogInterface, i) -> {
                DnDSheet.removeAllFavs(HomeActivity.this);
                m_favourites.clear();
                m_adapter.notifyDataSetChanged();
            });
            v_builder.setNegativeButton("CANCEL", (dialogInterface, i) -> {});

            v_builder.create().show();
        };
        return v_onClickListener;
    }

    private class AsyncTaskGetFavourites extends AsyncTask<Nullable, String, Nullable> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            m_progressDialog = new ProgressDialog(HomeActivity.this);
            m_progressDialog.setMessage("Please wait...It is downloading");
            m_progressDialog.setIndeterminate(false);
            m_progressDialog.setCancelable(false);
            m_progressDialog.show();
            m_adapter.notifyDataSetChanged();
            rclr_home_favourites.setEnabled(false);
        }

        @Override
        protected Nullable doInBackground(Nullable... nullables) {
            getFav();
            return null;
        }

        @Override
        protected void onPostExecute(Nullable nullable) {
            super.onPostExecute(nullable);
            m_adapter.notifyDataSetChanged();
            rclr_home_favourites.setEnabled(true);
            m_progressDialog.hide();
        }
    }
}