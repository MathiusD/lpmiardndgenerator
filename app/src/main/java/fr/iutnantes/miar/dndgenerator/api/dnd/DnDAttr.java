package fr.iutnantes.miar.dndgenerator.api.dnd;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * DnDAttr is a class to represent D&D attributes such as DEX (aka Dexterity).
 */
public class DnDAttr implements Serializable {

    public final String nameAttr, valueUserReadable;
    public final Integer value;

    /**
     * Basic Constructor for create attribute if beautiful value is missing.
     * @param nameAttr : String represent Name of attribute
     * @param value : Integer represent Value of attribute
     */
    public DnDAttr(@NonNull String nameAttr, @NonNull Integer value) {
        this(nameAttr, value, String.valueOf(value));
    }

    /**
     * Basic Constructor for create attribute if all arguments are provided.
     * @param nameAttr : String represent Name of attribute
     * @param value : Integer represent Value of attribute
     * @param valueUserReadable : String represent Value with modifier, ex : "14 (+2)"
     */
    public DnDAttr(@NonNull String nameAttr, @NonNull Integer value,
                   @NonNull String valueUserReadable) {
        this.nameAttr = nameAttr;
        this.value = value;
        this.valueUserReadable = valueUserReadable;
    }

    /**
     * For write attribute.
     * @return toString
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("%s : %s", nameAttr, valueUserReadable);
    }

    /**
     * For identify attribute.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(1, 3)
            .append(nameAttr.hashCode()).append(value.hashCode()).toHashCode();
    }

    /**
     * For compare Any Object with this attribute.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }
}
