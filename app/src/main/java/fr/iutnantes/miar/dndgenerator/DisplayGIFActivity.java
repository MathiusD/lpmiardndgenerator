package fr.iutnantes.miar.dndgenerator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import fr.iutnantes.miar.dndgenerator.adapter.ListGifAdapter;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSheet;
import fr.iutnantes.miar.dndgenerator.api.tenor.GIFFormat;
import fr.iutnantes.miar.dndgenerator.api.tenor.GIFObject;
import fr.iutnantes.miar.dndgenerator.api.tenor.TenorArgs;

public class DisplayGIFActivity extends AppCompatActivity {
    private static String TAG = "DisplayGIFActivity :: ";
    private static final String GIF_LIST = "gifList";
    private boolean m_searchPostChange = true;
    private boolean m_enableResearchListener = false;
    private int m_nbResult;
    private Context m_context = this;
    private DnDSheet m_sheet;
    private List<GIFObject> m_listGif;
    private ListGifAdapter m_adapter;
    private AsyncTaskGetGifList m_asyncTaskGetGifList;
    private TextView lbl_displayGif_name, lbl_displayGif_tag;
    private EditText txt_displayGif_nbResult, txt_displayGif_recherche;
    private ImageView img_displayGif_gif;
    private RecyclerView rclr_displayGif_relatedGif;
    private SeekBar skbr_displayGif_nbResult;
    private Switch swtch_displayGif_isNSFW;
    private RadioButton rdbtn_displayGif_withSheet, rdbtn_displayGif_withGif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_display_gif);

        img_displayGif_gif = findViewById(R.id.img_displayGif_gif);
        rclr_displayGif_relatedGif = findViewById(R.id.rclr_displayGif_relatedGif);
        lbl_displayGif_name = findViewById(R.id.lbl_displayGif_name);
        lbl_displayGif_tag = findViewById(R.id.lbl_displayGif_tag);
        txt_displayGif_nbResult = findViewById(R.id.txt_displayGif_nbResult);
        txt_displayGif_recherche = findViewById(R.id.txt_displayGif_recherche);
        skbr_displayGif_nbResult = findViewById(R.id.skbr_displayGif_nbResult);
        swtch_displayGif_isNSFW = findViewById(R.id.swtch_displayGif_isNSFW);
        rdbtn_displayGif_withSheet = findViewById(R.id.rdbtn_displayGif_withSheet);
        rdbtn_displayGif_withGif = findViewById(R.id.rdbtn_displayGif_withGif);

        Intent v_intent = getIntent();
        Bundle v_bundle = v_intent.getExtras();
        m_sheet = (DnDSheet) v_bundle.getSerializable(DisplayResultActivity.SHEET);

        setDataList(savedInstanceState, checkNbResult());
        if(savedInstanceState != null) {
            savedInstanceState.clear();
        }
        //Recycler View
        m_adapter = new ListGifAdapter(m_listGif, this);
        rclr_displayGif_relatedGif.setAdapter(m_adapter);
        rclr_displayGif_relatedGif.setLayoutManager(new GridLayoutManager(this, 2));
        lbl_displayGif_name.setText(m_sheet.getGIF().toString());
        String v_tags = "";
        if(m_sheet.getGIF().tags.size() > 0) {
            v_tags = m_sheet.getGIF().tags.get(0);
        }
        for (int compt = 1; compt < m_sheet.getGIF().tags.size(); compt++) {
            v_tags += String.format(", %s", m_sheet.getGIF().tags.get(compt));
        }
        lbl_displayGif_tag.setText(v_tags);
        if (v_tags.isEmpty()){
            lbl_displayGif_tag.setText("No tags attached on this GIF");
        }
        String v_urlGif = Objects.requireNonNull(m_sheet.getGIF().media.get(GIFFormat.GIF)).url;

        Glide.with(this)
                .load(v_urlGif)
                .into(img_displayGif_gif);

        // Listeners
        txt_displayGif_nbResult.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(m_listGif == null || !m_searchPostChange) {
                    return;
                }
                launchAsyncSearch();
            }
        });

        txt_displayGif_recherche.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(m_enableResearchListener) {
                    rdbtn_displayGif_withGif.setChecked(false);
                    rdbtn_displayGif_withSheet.setChecked(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(m_enableResearchListener) {
                    launchAsyncSearch();
                    if (txt_displayGif_recherche.getText().toString().isEmpty()) {
                        rdbtn_displayGif_withSheet.setChecked(true);
                    }
                }
            }
        });

        skbr_displayGif_nbResult.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                txt_displayGif_nbResult.setText(Integer.toString(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                m_searchPostChange = false;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                m_searchPostChange = true;
                launchAsyncSearch();
            }
        });

        swtch_displayGif_isNSFW.setOnCheckedChangeListener((compoundButton, b) -> {
            launchAsyncSearch();
        });

        View.OnClickListener v_onClickRadioButtonListener = view -> {
            m_enableResearchListener = false;
            txt_displayGif_recherche.setText("");
            launchAsyncSearch();
            m_enableResearchListener = true;
        };
        rdbtn_displayGif_withSheet.setOnClickListener(v_onClickRadioButtonListener);
        rdbtn_displayGif_withGif.setOnClickListener(v_onClickRadioButtonListener);

        if(rdbtn_displayGif_withSheet.isChecked() || rdbtn_displayGif_withGif.isChecked()){
            launchAsyncSearch();
        }
    }

    private void launchAsyncSearch() {
        if (m_asyncTaskGetGifList != null) {
            m_asyncTaskGetGifList.cancel(true);
        }
        m_asyncTaskGetGifList = new AsyncTaskGetGifList();
        m_asyncTaskGetGifList.execute((Nullable) null);
    }

    private int checkNbResult() {
        int v_nbResult = 0;
        try {
            v_nbResult = Integer.parseInt(txt_displayGif_nbResult.getText().toString());
        }
        catch (Exception e) {
            Log.d(TAG, e.getMessage());
            Toast.makeText(m_context, "'Nb Result' must be an integer !", Toast.LENGTH_LONG).show();
        }
        if (v_nbResult == 0) {
            Toast.makeText(m_context, "The 'Nb Result' must be an integer greater than '0'", Toast.LENGTH_LONG).show();
        }
        return v_nbResult;
    }

    private void setDataList(Bundle savedInstanceState, int nbResult) {
        if(savedInstanceState != null) {
            m_listGif = (List<GIFObject>) savedInstanceState.getSerializable(GIF_LIST);
            return;
        }
        m_listGif = GIFObject.getGIFObjects(this, m_sheet.getGIF().searchQuery, nbResult,
            m_sheet.getGIF());
    }

    private void doSearch() {
        int v_nbResult = m_nbResult;
        String v_recherche = m_sheet.searchGIF;
        Log.d(TAG, v_recherche);
        TenorArgs v_tenorArgs = TenorArgs.GIF_SAFE;
        if(rdbtn_displayGif_withGif.isChecked()) {
            v_recherche = m_sheet.getGIF().searchQuery;
        }
        if(!txt_displayGif_recherche.getText().toString().isEmpty()) {
            v_recherche = txt_displayGif_recherche.getText().toString();
        }
        if(swtch_displayGif_isNSFW.isChecked()) {
            v_tenorArgs = TenorArgs.GIF;
        }
        if (v_nbResult > 0) {
            List<GIFObject> v_newGifs = GIFObject.getGIFObjects(m_context,
                    v_recherche, v_tenorArgs.fork(v_nbResult, m_sheet.getGIF()));
            m_listGif.clear();
            m_listGif.addAll(v_newGifs);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(GIF_LIST, (Serializable) m_listGif);
        super.onSaveInstanceState(outState);
    }

    private class AsyncTaskGetGifList extends AsyncTask<Nullable, String, Nullable> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            m_nbResult = checkNbResult();
        }

        @Override
        protected Nullable doInBackground(Nullable... nullables) {
            doSearch();
            return null;
        }

        @Override
        protected void onPostExecute(Nullable nullable) {
            super.onPostExecute(nullable);
            m_adapter.notifyDataSetChanged();
        }
    }
}