package fr.iutnantes.miar.dndgenerator.api.dnd.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import fr.iutnantes.miar.dndgenerator.api.BaseDB;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSaves;

/**
 * Class representing the storage of the Saves of a bookmarks.
 */
public class DnDSavesDB extends BaseDB {
    /**
     * Method for create table in database
     * @param db : sql connector linked to database
     * @param dbVersion : Version of database
     */
    @Override
    public void onCreate(@NonNull SQLiteDatabase db, @NonNull Integer dbVersion) {
        onCreate(db, allStringAttrs, intAttrs, DATABASE_NAME, dbVersion, PRIMARY_KEY);
    }
    /**
     * Constructor for build database in specific version
     * @param context : Context for access database
     * @param dbVersion : Version of database
     */
    public DnDSavesDB(@NonNull Context context, int dbVersion) {
        super(context, DATABASE_NAME, dbVersion);
    }
    /**
     * Constructor for build default database
     * @param context : Context for access database
     */
    public DnDSavesDB(@NonNull Context context) {
        this(context, DATABASE_VERSION);
    }
    public final static String PRIMARY_KEY;
    public final static String TABLE_NAME;
    public final static int DATABASE_VERSION;
    public final static String DATABASE_NAME;
    public final static List<String> intAttrs;
    public final static List<String> stringAttrs = new ArrayList<>();
    public final static List<String> allStringAttrs;
    public final static List<String> listStringAttrs = new ArrayList<>();
    /**
     * Class represent fields in current model
     */
    protected static class Vars implements BaseColumns {
        public static final String TABLE_NAME = "DnDSaves";
        public static final String WANDS = "wands";
        public static final String BREATH = "breath";
        public static final String STONE = "stone";
        public static final String MAGIC = "magic";
        public static final String POISON = "poison";
    }
    static {
        PRIMARY_KEY = Vars._ID;
        TABLE_NAME = Vars.TABLE_NAME;
        DATABASE_VERSION = DnDSheetDB.DATABASE_VERSION;
        DATABASE_NAME = TABLE_NAME;
        List<String> tempInt = new ArrayList<>();
        tempInt.add(Vars.WANDS);
        tempInt.add(Vars.BREATH);
        tempInt.add(Vars.STONE);
        tempInt.add(Vars.MAGIC);
        tempInt.add(Vars.POISON);
        intAttrs = tempInt;
        List<String> tempAllString = new ArrayList<>();
        tempAllString.addAll(stringAttrs);
        tempAllString.addAll(listStringAttrs);
        allStringAttrs = tempAllString;
    }
    /**
     * Method for get saves
     * @param id : HashCode of saves
     * @return saves
     */
    @Nullable
    public DnDSaves getDnDSaves(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(tableName(), null, String.format("%s = ?", Vars._ID),
                new String[]{String.valueOf(id)}, null, null, null);
        if (cursor.moveToFirst()) {
            return new DnDSaves(
                    cursor.getInt(cursor.getColumnIndex(Vars.WANDS)),
                    cursor.getInt(cursor.getColumnIndex(Vars.BREATH)),
                    cursor.getInt(cursor.getColumnIndex(Vars.STONE)),
                    cursor.getInt(cursor.getColumnIndex(Vars.MAGIC)),
                    cursor.getInt(cursor.getColumnIndex(Vars.POISON))
            );
        }
        return null;
    }

    /**
     * Method for insert Saves if not already in database
     * @param saves : Saves to save
     * @return isInserted
     */
    public boolean insertDnDSaves(DnDSaves saves) {
        if (getDnDSaves(saves.hashCode()) != null)
            return false;
        ContentValues values = new ContentValues();
        values.put(Vars.WANDS, saves.wands);
        values.put(Vars.BREATH, saves.breath);
        values.put(Vars.STONE, saves.stone);
        values.put(Vars.MAGIC, saves.magic);
        values.put(Vars.POISON, saves.poison);
        values.put(Vars._ID, saves.hashCode());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(tableName(), null, values);
        return true;
    }
}
