package fr.iutnantes.miar.dndgenerator.api.tenor;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class representing Content filters (such as PEGI)
 */
public class FilterContent implements Serializable {

    public final String name;

    /**
     * Basic Constructor for create attribute if all arguments are provided.
     * @param name : String represent name of this filter
     */
    public FilterContent(@NonNull String name) {
        this.name = name;
    }

    /**
     * For write filter content.
     * @return toString
     */
    @NonNull
    @Override
    public String toString() {
        return name;
    }

    /**
     * For compare Any Object with this filter content.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }

    /**
     * For identify filter content.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public final static FilterContent OFF = new FilterContent("off");
    public final static FilterContent LOW = new FilterContent("low");
    public final static FilterContent MEDIUM = new FilterContent("medium");
    public final static FilterContent HIGH = new FilterContent("high");
    public final static List<FilterContent> filters;
    static {
        List<FilterContent> filts = new ArrayList<>();
        filts.add(OFF);
        filts.add(LOW);
        filts.add(MEDIUM);
        filts.add(HIGH);
        filters = filts;
    }
    public static final FilterContent DEFAULT = OFF;

    /**
     * For get random content filter from the API.
     * @return filterContent
     */
     @NonNull
    public static FilterContent getRandomFilterContent() {
        return getRandomFilterContent(filters);
    }

    /**
     * For get random system from list provided.
     * @param filters : List where we’re going to shoot randomly
     * @return filterContent
     */
    @NonNull
    public static FilterContent getRandomFilterContent(@NonNull List<FilterContent> filters) {
        return filters.get(new Random().nextInt(filters.size()));
    }
}
