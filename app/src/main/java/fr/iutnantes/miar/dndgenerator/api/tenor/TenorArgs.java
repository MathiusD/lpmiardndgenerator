package fr.iutnantes.miar.dndgenerator.api.tenor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;

/**
 * Class grouping the different arguments that can be passed in a Tenor search.
 */
public class TenorArgs implements Serializable {

    public final MediaFilter mediaFilter;
    public final FilterContent filterContent;
    public final Integer nb;
    public final GIFObject exclude;

    /**
     * Default Constructor;
     */
    public TenorArgs() {
        this(GIFObject.DEFAULT_SIZE);
    }

    /**
     * Constructor with only media filter
     * @param mediaFilter : MediaFilter apply in search
     */
    public TenorArgs(@NonNull MediaFilter mediaFilter) {
        this(mediaFilter, GIFObject.DEFAULT_SIZE);
    }
    /**
     * Constructor with only gif format
     * @param gifFormat : gif format you want to see
     */
    public TenorArgs(@NonNull GIFFormat gifFormat) {
        this(gifFormat, GIFObject.DEFAULT_SIZE);
    }
    /**
     * Constructor with only filter content
     * @param filterContent : filter content apply in search
     */
    public TenorArgs(@NonNull FilterContent filterContent) {
        this(filterContent, GIFObject.DEFAULT_SIZE);
    }
    /**
     * Constructor with only nb results
     * @param nb : nb of result you wish
     */
    public TenorArgs(@NonNull Integer nb) {
        this(nb, null);
    }
    /**
     * Constructor with only excluded gif
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@Nullable GIFObject exclude) {
        this(GIFObject.DEFAULT_SIZE, exclude);
    }
    /**
     * Basic Constructor.
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     */
    public TenorArgs(@NonNull GIFFormat gifFormat, @NonNull FilterContent filterContent) {
        this(gifFormat, filterContent, GIFObject.DEFAULT_SIZE);
    }
    /**
     * Basic Constructor.
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     */
    public TenorArgs(@NonNull MediaFilter mediaFilter, @NonNull FilterContent filterContent) {
        this(mediaFilter, filterContent, GIFObject.DEFAULT_SIZE);
    }
    /**
     * Basic Constructor.
     * @param gifFormat : gif format you want to see
     * @param nb : nb of result you wish
     */
    public TenorArgs(@NonNull GIFFormat gifFormat, @NonNull Integer nb) {
        this(gifFormat, nb, null);
    }
    /**
     * Basic Constructor.
     * @param mediaFilter : media filter apply in search
     * @param nb : nb of result you wish
     */
    public TenorArgs(@NonNull MediaFilter mediaFilter, @NonNull Integer nb) {
        this(mediaFilter, nb, null);
    }
    /**
     * Basic Constructor.
     * @param gifFormat : gif format you want to see
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@NonNull GIFFormat gifFormat, @Nullable GIFObject exclude) {
        this(gifFormat, GIFObject.DEFAULT_SIZE, exclude);
    }
    /**
     * Basic Constructor.
     * @param mediaFilter : media filter apply in search
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@NonNull MediaFilter mediaFilter, @Nullable GIFObject exclude) {
        this(mediaFilter, GIFObject.DEFAULT_SIZE, exclude);
    }
    /**
     * Basic Constructor.
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     */
    public TenorArgs(@NonNull FilterContent filterContent, @NonNull Integer nb) {
        this(filterContent, nb, null);
    }
    /**
     * Basic Constructor.
     * @param filterContent : filter content apply in search
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@NonNull FilterContent filterContent, @Nullable GIFObject exclude) {
        this(filterContent, GIFObject.DEFAULT_SIZE, exclude);
    }
    /**
     * Basic Constructor.
     * @param nb : nb of result you wish
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@NonNull Integer nb, @Nullable GIFObject exclude) {
        this(MediaFilter.DEFAULT, nb, exclude);
    }
    /**
     * Basic Constructor.
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     */
    public TenorArgs(@NonNull GIFFormat gifFormat, @NonNull FilterContent filterContent,
                     @NonNull Integer nb) {
        this(gifFormat, filterContent, nb, null);
    }
    /**
     * Basic Constructor.
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     */
    public TenorArgs(@NonNull MediaFilter mediaFilter, @NonNull FilterContent filterContent,
                     @NonNull Integer nb) {
        this(mediaFilter, filterContent, nb, null);
    }
    /**
     * Basic Constructor.
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@NonNull GIFFormat gifFormat, @NonNull FilterContent filterContent,
                     @Nullable GIFObject exclude) {
        this(gifFormat, filterContent, GIFObject.DEFAULT_SIZE, exclude);
    }
    /**
     * Basic Constructor.
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@NonNull MediaFilter mediaFilter, @NonNull FilterContent filterContent,
                     @Nullable GIFObject exclude) {
        this(mediaFilter, filterContent, GIFObject.DEFAULT_SIZE, exclude);
    }
    /**
     * Basic Constructor.
     * @param gifFormat : gif format you want to see
     * @param nb : nb of result you wish
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@NonNull GIFFormat gifFormat, @NonNull Integer nb,
                     @Nullable GIFObject exclude) {
        this(gifFormat, FilterContent.DEFAULT, nb, exclude);
    }
    /**
     * Basic Constructor.
     * @param mediaFilter : media filter apply in search
     * @param nb : nb of result you wish
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@NonNull MediaFilter mediaFilter, @NonNull Integer nb,
                     @Nullable GIFObject exclude) {
        this(mediaFilter, FilterContent.DEFAULT, nb, exclude);
    }

    /**
     * Basic Constructor.
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@NonNull FilterContent filterContent, @NonNull Integer nb,
                     @Nullable GIFObject exclude) {
        this(MediaFilter.DEFAULT, filterContent, nb, exclude);
    }

    /**
     * Constructor with all arguments.
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@NonNull GIFFormat gifFormat, @NonNull FilterContent filterContent,
                     @NonNull Integer nb, @Nullable GIFObject exclude) {
        this(gifFormat.getMinimalMediaFilter(), filterContent, nb, exclude);
    }

    /**
     * Constructor with all arguments.
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param exclude : gif you don’t want to see
     */
    public TenorArgs(@Nullable MediaFilter mediaFilter, @NonNull FilterContent filterContent,
                     @NonNull Integer nb, @Nullable GIFObject exclude) {
        this.mediaFilter = mediaFilter != null ? mediaFilter : MediaFilter.DEFAULT;
        this.filterContent = filterContent;
        this.nb = nb;
        this.exclude = exclude;
    }

    /**
     * Constructor by copy
     * @param tenorArgs : original instance of TenorArgs
     */
    public TenorArgs(@NonNull TenorArgs tenorArgs) {
        this.mediaFilter = tenorArgs.mediaFilter;
        this.filterContent = tenorArgs.filterContent;
        this.nb = tenorArgs.nb;
        this.exclude = tenorArgs.exclude;
    }

    /**
     * Constructor by artifact
     * @param artifact : Artifact of gif for recreate TenorArgs associated
     */
    public TenorArgs(@NonNull GIFArtifact artifact) {
        this(artifact.mediaFilter, artifact.filterContent);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork() {
        return new TenorArgs(this);
    }

    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param mediaFilter : media filter apply in search
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull MediaFilter mediaFilter) {
        return fork(mediaFilter, nb);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param gifFormat : gif format you want to see
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull GIFFormat gifFormat) {
        return fork(gifFormat, nb);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param filterContent : filter content apply in search
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull FilterContent filterContent) {
        return fork(filterContent, nb);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param nb : nb of result you wish
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull Integer nb) {
        return fork(filterContent, nb);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param exclude : gif you don’t want to see
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@Nullable GIFObject exclude) {
        return fork(filterContent, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull GIFFormat gifFormat, @NonNull FilterContent filterContent) {
        return fork(gifFormat, filterContent, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull MediaFilter mediaFilter, @NonNull FilterContent filterContent) {
        return fork(mediaFilter, filterContent, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param gifFormat : gif format you want to see
     * @param nb : nb of result you wish
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull GIFFormat gifFormat, @NonNull Integer nb) {
        return fork(gifFormat, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param mediaFilter : media filter apply in search
     * @param nb : nb of result you wish
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull MediaFilter mediaFilter, @NonNull Integer nb) {
        return fork(mediaFilter, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param gifFormat : gif format you want to see
     * @param exclude : gif you don’t want to see
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull GIFFormat gifFormat, @Nullable GIFObject exclude) {
        return fork(gifFormat, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param mediaFilter : media filter apply in search
     * @param exclude : gif you don’t want to see
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull MediaFilter mediaFilter, @Nullable GIFObject exclude) {
        return fork(mediaFilter, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull FilterContent filterContent, @NonNull Integer nb) {
        return fork(filterContent, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param filterContent : filter content apply in search
     * @param exclude : gif you don’t want to see
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull FilterContent filterContent, @Nullable GIFObject exclude) {
        return fork(filterContent, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param nb : nb of result you wish
     * @param exclude : gif you don’t want to see
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull Integer nb, @Nullable GIFObject exclude) {
        return fork(filterContent, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull GIFFormat gifFormat, @NonNull FilterContent filterContent,
                     @NonNull Integer nb) {
        return new TenorArgs(gifFormat, filterContent, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull MediaFilter mediaFilter, @NonNull FilterContent filterContent,
                     @NonNull Integer nb) {
        return new TenorArgs(mediaFilter, filterContent, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param gifFormat : gif format you want to see
     * @param filterContent : filter content apply in search
     * @param exclude : gif you don’t want to see
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull GIFFormat gifFormat, @NonNull FilterContent filterContent,
                     @Nullable GIFObject exclude) {
        return new TenorArgs(gifFormat, filterContent, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param mediaFilter : media filter apply in search
     * @param filterContent : filter content apply in search
     * @param exclude : gif you don’t want to see
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull MediaFilter mediaFilter, @NonNull FilterContent filterContent,
                     @Nullable GIFObject exclude) {
        return new TenorArgs(mediaFilter, filterContent, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param gifFormat : gif format you want to see
     * @param nb : nb of result you wish
     * @param exclude : gif you don’t want to see
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull GIFFormat gifFormat, @NonNull Integer nb,
                     @Nullable GIFObject exclude) {
        return new TenorArgs(gifFormat, filterContent, nb, exclude);
    }
    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param mediaFilter : media filter apply in search
     * @param nb : nb of result you wish
     * @param exclude : gif you don’t want to see
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull MediaFilter mediaFilter, @NonNull Integer nb,
                     @Nullable GIFObject exclude) {
        return new TenorArgs(mediaFilter, filterContent, nb, exclude);
    }

    /**
     * Fork this TenorArgs to new TenorArgs with old arguments and override given arguments
     * @param filterContent : filter content apply in search
     * @param nb : nb of result you wish
     * @param exclude : gif you don’t want to see
     * @return tenorArgs
     */
    @NonNull
    public TenorArgs fork(@NonNull FilterContent filterContent, @NonNull Integer nb,
                     @Nullable GIFObject exclude) {
        return new TenorArgs(mediaFilter, filterContent, nb, exclude);
    }

    public final static TenorArgs GIF = new TenorArgs(GIFFormat.GIF);
    public final static TenorArgs GIF_SAFE = GIF.fork(FilterContent.HIGH);
    public final static TenorArgs DEFAULT = GIF_SAFE;
}
