package fr.iutnantes.miar.dndgenerator;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

import fr.iutnantes.miar.dndgenerator.adapter.ListGifAdapter;
import fr.iutnantes.miar.dndgenerator.adapter.ListResultsAdapter;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSheet;
import fr.iutnantes.miar.dndgenerator.api.tenor.GIFFormat;
import fr.iutnantes.miar.dndgenerator.api.tenor.GIFObject;

public class DisplayResultActivity extends AppCompatActivity {
    private static final String TAG = "DisplayResultActivity :: ";
    public static final String SHEET = "sheet";
    public static final int GIF_RESULT_CODE = 42;
    private int m_index;
    private DnDSheet m_sheet;
    private TextView lbl_displayResult_name, lbl_displayResult_class, lbl_displayResult_description, lbl_displayResult_system,
            lbl_displayResult_personality, lbl_displayResult_skills, lbl_displayResult_spells,
            lbl_displayResult_saves, lbl_displayResult_str, lbl_displayResult_dex,
            lbl_displayResult_int, lbl_displayResult_con, lbl_displayResult_wis,
            lbl_displayResult_cha, lbl_displayResult_equipements, lbl_displayResult_hp, lbl_displayResult_languages;
    private ImageView img_displayResult_gif, img_displayResult_reloadName;
    private FloatingActionButton btn_displayResult_favourite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_display_result);

        lbl_displayResult_name = findViewById(R.id.lbl_displayResult_name);
        lbl_displayResult_class = findViewById(R.id.lbl_displayResult_class);
        lbl_displayResult_description = findViewById(R.id.lbl_displayResult_description);
        lbl_displayResult_system = findViewById(R.id.lbl_displayResult_system);
        lbl_displayResult_personality = findViewById(R.id.lbl_displayResult_personality);
        lbl_displayResult_skills = findViewById(R.id.lbl_displayResult_skills);
        lbl_displayResult_spells = findViewById(R.id.lbl_displayResult_spells);
        lbl_displayResult_saves = findViewById(R.id.lbl_displayResult_saves);
        lbl_displayResult_str = findViewById(R.id.lbl_displayResult_str);
        lbl_displayResult_dex = findViewById(R.id.lbl_displayResult_dex);
        lbl_displayResult_int = findViewById(R.id.lbl_displayResult_int);
        lbl_displayResult_con = findViewById(R.id.lbl_displayResult_con);
        lbl_displayResult_wis = findViewById(R.id.lbl_displayResult_wis);
        lbl_displayResult_cha = findViewById(R.id.lbl_displayResult_cha);
        lbl_displayResult_equipements = findViewById(R.id.lbl_displayResult_equipements);
        img_displayResult_gif = findViewById(R.id.img_displayResult_gif);
        img_displayResult_reloadName = findViewById(R.id.img_displayResult_reloadName);
        btn_displayResult_favourite = findViewById(R.id.btn_displayResult_favourite);
        lbl_displayResult_hp = findViewById(R.id.lbl_displayResult_hp);
        lbl_displayResult_languages = findViewById(R.id.lbl_displayResult_languages);


        Intent v_getIntent = getIntent();
        m_sheet = (DnDSheet) v_getIntent.getSerializableExtra(ResearchResultsActivity.SHEET);
        m_index = v_getIntent.getIntExtra(ListResultsAdapter.INDEX, -1);
        String v_spells;
        if(!m_sheet.spells.isEmpty()) {
            v_spells = m_sheet.spells.get(0);
            for (int v_compt = 1; v_compt < m_sheet.spells.size(); v_compt++) {
                v_spells += String.format("\n%s", m_sheet.spells.get(v_compt));
            }
        }
        else {
            v_spells = "There is no such a thing as a spell in this sheet.";
        }

        String v_equipement;
        if(!m_sheet.equipment.isEmpty()) {
            v_equipement = m_sheet.equipment.get(0);
            for(int v_compt = 1; v_compt < m_sheet.equipment.size(); v_compt++) {
                v_equipement += String.format("\n%s", m_sheet.equipment.get(v_compt));
            }
        }
        else {
            v_equipement = "There is no such a thing as equipments in this sheet.";
        }

        String v_languages;
        if(!m_sheet.languages.isEmpty()) {
            v_languages = m_sheet.languages.get(0);
            for(int v_compt = 1; v_compt < m_sheet.languages.size(); v_compt++) {
                v_languages += String.format("\n%s", m_sheet.languages.get(v_compt));
            }
        }
        else {
            v_languages = "The language.s known is.are not referenced in this sheet.";
        }

        String v_hp;
        if(m_sheet.hp != null) {
            v_hp = String.format("HP : %d", m_sheet.hp);
        }
        else {
            v_hp = "No HP";
        }

        lbl_displayResult_name.setText(m_sheet.getName());
        lbl_displayResult_class.setText(m_sheet.caracterClass.toString());
        lbl_displayResult_description.setText(m_sheet.appearance);
        lbl_displayResult_system.setText(m_sheet.system.longName);
        lbl_displayResult_personality.setText(m_sheet.personality);
        lbl_displayResult_skills.setText(m_sheet.skills.toString());
        lbl_displayResult_spells.setText(v_spells);
        lbl_displayResult_str.setText(m_sheet.attr.get("str").toString());
        lbl_displayResult_dex.setText(m_sheet.attr.get("dex").toString());
        lbl_displayResult_int.setText(m_sheet.attr.get("int").toString());
        lbl_displayResult_con.setText(m_sheet.attr.get("con").toString());
        lbl_displayResult_wis.setText(m_sheet.attr.get("wis").toString());
        lbl_displayResult_cha.setText(m_sheet.attr.get("cha").toString());
        lbl_displayResult_saves.setText(m_sheet.saves.toString());
        lbl_displayResult_equipements.setText(v_equipement);
        lbl_displayResult_hp.setText(v_hp);
        lbl_displayResult_languages.setText(v_languages);

        setFavouriteBtn();

        loadGif();

        btn_displayResult_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(m_sheet.isFav(DisplayResultActivity.this)) {
                    m_sheet.removeFav(DisplayResultActivity.this);
                }
                else {
                    m_sheet.addFav(DisplayResultActivity.this);
                }
                setFavouriteBtn();
            }
        });

        img_displayResult_reloadName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_displayResult_reloadName.setEnabled(false);
                m_sheet.generateName(DisplayResultActivity.this);
                lbl_displayResult_name.setText(m_sheet.getName());
                img_displayResult_reloadName.setEnabled(true);
            }
        });
    }

    private void setFavouriteBtn() {
        if(m_sheet.isFav(this)) {
            btn_displayResult_favourite.setImageResource(R.drawable.ic_baseline_star_24);
        }
        else {
            btn_displayResult_favourite.setImageResource(R.drawable.ic_baseline_star_border_24);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == requestCode) {
            GIFObject v_gif = (GIFObject) Objects.requireNonNull(data).getExtras()
                .getSerializable(ListGifAdapter.GIF_OBJECT);
            m_sheet.changeGIF(DisplayResultActivity.this, v_gif);
            loadGif();
        }
    }

    private void loadGif() {
        String imgUrl = Objects.requireNonNull(m_sheet.getGIF().media.get(GIFFormat.GIF)).url;
        Glide.with(this)
                .load(imgUrl)
                .into(img_displayResult_gif);

        img_displayResult_gif.setOnClickListener(view -> {
            Intent v_intent = new Intent(
                    DisplayResultActivity.this, DisplayGIFActivity.class);
            Bundle v_bundle = new Bundle();
            v_bundle.putSerializable(SHEET, m_sheet);
            v_intent.putExtras(v_bundle);
            startActivityForResult(v_intent, GIF_RESULT_CODE);
        });
    }

    @Override
    public void onBackPressed() {
        Intent v_intentRetour = new Intent();
        v_intentRetour.putExtra(SHEET, m_sheet);
        if(m_index > -1) {
            v_intentRetour.putExtra(ListResultsAdapter.INDEX, m_index);
        }
        setResult(ListResultsAdapter.RESULT_DISPLAYRESULT, v_intentRetour);
        finish();
    }
}