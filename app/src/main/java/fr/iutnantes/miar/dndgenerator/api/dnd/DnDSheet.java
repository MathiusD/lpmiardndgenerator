package fr.iutnantes.miar.dndgenerator.api.dnd;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.SimpleFuture;
import com.koushikdutta.ion.Ion;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.iutnantes.miar.dndgenerator.api.dnd.models.DnDSavesDB;
import fr.iutnantes.miar.dndgenerator.api.dnd.models.DnDSheetDB;
import fr.iutnantes.miar.dndgenerator.api.donjon.bin.sh.Name;
import fr.iutnantes.miar.dndgenerator.api.tenor.GIFObject;
import fr.iutnantes.miar.dndgenerator.api.tenor.TenorArgs;
import fr.iutnantes.miar.dndgenerator.api.tenor.models.GIFArtifactDB;

/**
 * DnDSheet is a class that represents a D&D character sheet.
 * It is also used as a main class to communicate with the API.
 */
public class DnDSheet implements Serializable {

    public final static String apiProvider = "http://character.totalpartykill.ca/";
    protected static final String TAG = "DnDSheet";
    public final static int DEFAULT_SIZE_LIST_SHEET = 10;
    public final static int MAX_TRY = 10;
    public final static Comparator<DnDSheet> COMPARATOR_BY_SYSTEM = (o1, o2) ->
        o1.system.compareTo(o2.system);
    public final static Comparator<DnDSheet> COMPARATOR_BY_CLASS = (o1, o2) ->
        o1.caracterClass.compareTo(o2.caracterClass);

    public final DnDAttrs attr;
    public final DnDSaves saves;
    public final Integer hp;
    public final Integer thac9;
    public final Integer ac;
    public final String appearance;
    public final DnDClass caracterClass;
    public final String personality;
    public final DnDSystem system;
    public final List<String> languages;
    public final List<String> equipment;
    public final List<String> notes;
    public final List<String> spells;
    public final DnDSkills skills;
    protected GIFObject gif;
    public final String searchGIF;
    public final TenorArgs tenorArgs;
    protected String name;
    /**
     * Basic Constructor for create sheet if all arguments are provided.
     * @param hp : Integer represent hp of this character
     * @param thac9 : Integer represent thac9 of this character
     * @param ac : Integer represent ac of this character
     * @param appearance : String represent appearance of this character
     * @param caracterClass : DnDClass represent class of this character
     * @param personality : String represent personality of this character
     * @param system : DnDSystem represent system of this sheet
     * @param languages : List of all languages known by this character
     * @param equipment : List of all equipment of this character
     * @param notes : List of all note concern this character
     * @param spells : List of all spells of this character
     * @param saves : DnDSaves represent resistances of this character
     * @param attr : DnDAttrs represent attributes of this character
     * @param skills : DnDSkills represent all skills of this character
     * @param gifObject : GIFObject represent a Media associated to this character
     * @param name : String represent name of this sheet
     */
    public DnDSheet(@Nullable Integer hp, @Nullable Integer thac9, @Nullable Integer ac,
                     @Nullable String appearance, @NonNull DnDClass caracterClass,
                     @Nullable String personality, @NonNull DnDSystem system,
                     @NonNull List<String> languages, @NonNull List<String> equipment,
                     @NonNull List<String> notes,  @NonNull List<String> spells,
                     @NonNull DnDSaves saves, @NonNull DnDAttrs attr,
                     @NonNull DnDSkills skills, @NonNull GIFObject gifObject,
                     @NonNull String name) {
        this.hp = hp != null && hp != -1 ? hp : null;
        this.thac9 = thac9 != null && thac9 != -1 ? thac9 : null;
        this.ac = ac != null && ac != -1 ? ac : null;
        this.appearance = appearance != null && !appearance.equals("") ? appearance : null;
        this.caracterClass = caracterClass;
        this.personality = personality != null && !personality.equals("") ? personality : null;
        this.system = system;
        this.languages = languages;
        this.equipment = equipment;
        this.notes = notes;
        this.saves = saves;
        this.attr = attr;
        this.skills = skills;
        this.spells = spells;
        this.gif = gifObject;
        this.searchGIF = String.format("%s,%s,%s", this.caracterClass, this.appearance,
                this.system);
        this.tenorArgs = this.gif.tenorArgs;
        this.name = name;
    }
    /**
     * Basic Constructor for create sheet from Funkaoshi API.
     * @param hp : Integer represent hp of this character
     * @param thac9 : Integer represent thac9 of this character
     * @param ac : Integer represent ac of this character
     * @param appearance : String represent appearance of this character
     * @param caracterClass : DnDClass represent class of this character
     * @param personality : String represent personality of this character
     * @param system : DnDSystem represent system of this sheet
     * @param languages : List of all languages known by this character
     * @param equipment : List of all equipment of this character
     * @param notes : List of all note concern this character
     * @param spells : List of all spells of this character
     * @param saves : DnDSaves represent resistances of this character
     * @param attr : DnDAttrs represent attributes of this character
     * @param skills : DnDSkills represent all skills of this character
     * @param ctx : Context for search media related to this character
     * @param tenorArgs : TenorArgs for search media related to this character
     */
    protected DnDSheet(@Nullable Integer hp, @Nullable Integer thac9, @Nullable Integer ac,
                       @Nullable String appearance, @NonNull DnDClass caracterClass,
                       @Nullable String personality, @NonNull DnDSystem system,
                       @NonNull List<String> languages, @NonNull List<String> equipment,
                       @NonNull List<String> notes, @NonNull List<String> spells,
                       @NonNull DnDSaves saves, @NonNull DnDAttrs attr,
                       @NonNull DnDSkills skills, @NonNull Context ctx,
                       @NonNull TenorArgs tenorArgs) {
        this(hp, thac9, ac, appearance, caracterClass, personality, system, languages, equipment,
            notes, spells, saves, attr, skills, GIFObject.getGIFObject(ctx, String.format(
                "%s,%s,%s", caracterClass, appearance, system), tenorArgs),
                generateName(ctx, personality, appearance, caracterClass));
    }

    /**
     * For change GIF and update Fav is necessary (In Default DB)
     * @param ctx : Context for access database and fetch name
     */
    public void generateName(@NonNull Context ctx) {
        generateName(ctx, DnDSheetDB.DATABASE_VERSION);
    }

    /**
     * For change GIF and update Fav is necessary
     * @param ctx : Context for access database and fetch name
     * @param dbVersion : Version of database
     */
    public void generateName(@NonNull Context ctx, int dbVersion) {
        this.name = generateName(ctx, appearance, personality, caracterClass);
        if (isFav(ctx, dbVersion))
            updateFav(ctx, dbVersion);
    }

    /**
     * Method for generate name related to information given
     * @param ctx : Context for fetch name
     * @param appearance : String represent appearance of this character
     * @param personality : String represent personality of this character
     * @param caracterClass : DnDClass represent class of this character
     * @return name
     */
    public static String generateName(@NonNull Context ctx, @Nullable String appearance,
                                      @Nullable String personality,
                                      @NonNull DnDClass caracterClass) {
        return Name.getRandomName(ctx, Name.allPeopleNames,
            personality != null ? personality : "",
            appearance != null ? appearance : "",
            caracterClass.name
        );
    }

    /**
     * Accessor for name property
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Accessor for gif property
     * @return gif
     */
    public GIFObject getGIF() {
        return gif;
    }

    /**
     * For change GIF and update Fav is necessary
     * @param ctx : Context for access database
     * @param gif : New gif for this character
     * @param dbVersion : Version of database
     */
    public void changeGIF(@NonNull Context ctx, @NonNull GIFObject gif, int dbVersion) {
        this.gif = gif;
        if (isFav(ctx, dbVersion))
            updateFav(ctx, dbVersion);
    }

    /**
     * For update Fav
     * @param ctx : Context for access database
     * @param dbVersion : Version of database
     * @return favIsUpdated
     */
    public boolean updateFav(@NonNull Context ctx, int dbVersion) {
        DnDSheetDB db = new DnDSheetDB(ctx, dbVersion);
        return db.updateGIF(this) && db.updateName(this);
    }

    /**
     * For remove Fav
     * @param ctx : Context for access database
     * @param dbVersion : Version of database
     * @return favIsRemoved
     */
    public boolean removeFav(@NonNull Context ctx, int dbVersion) {
        return new DnDSheetDB(ctx, dbVersion).removeDnDSheet(this);
    }

    /**
     * For add Fav
     * @param ctx : Context for access database
     * @param dbVersion : Version of database
     * @return favIsAdded
     */
    public boolean addFav(@NonNull Context ctx, int dbVersion) {
        return new DnDSheetDB(ctx, dbVersion).insertDnDSheet(this);
    }

    /**
     * To find out if he's a fav
     * @param ctx : Context for access database
     * @param dbVersion : Version of database
     * @return isFav
     */
    public boolean isFav(@NonNull Context ctx, int dbVersion) {
        return new DnDSheetDB(ctx, dbVersion).getDnDSheet(hashCode()) != null;
    }

    /**
     * For get all Favs
     * @param ctx : Context for access database
     * @param dbVersion : Version of database
     * @return allFavs
     */
    @NonNull
    public static List<DnDSheet> getFavs(@NonNull Context ctx, int dbVersion) {
        return new DnDSheetDB(ctx, dbVersion).getAllDnDSheet();
    }

    /**
     * For change GIF and update Fav is necessary (In Default DB)
     * @param ctx : Context for access database
     * @param gif : New gif for this character
     */
    public void changeGIF(@NonNull Context ctx, @NonNull GIFObject gif) {
        changeGIF(ctx, gif, DnDSheetDB.DATABASE_VERSION);
    }

    /**
     * For update Fav (In Default DB)
     * @param ctx : Context for access database
     * @return favIsUpdated
     */
    public boolean updateFav(@NonNull Context ctx) {
        return updateFav(ctx, DnDSheetDB.DATABASE_VERSION);
    }

    /**
     * For remove Fav (In Default DB)
     * @param ctx : Context for access database
     * @return favIsRemoved
     */
    public boolean removeFav(@NonNull Context ctx) {
        return removeFav(ctx, DnDSheetDB.DATABASE_VERSION);
    }

    /**
     * For add Fav (In Default DB)
     * @param ctx : Context for access database
     * @return favIsAdded
     */
    public boolean addFav(@NonNull Context ctx) {
        return addFav(ctx, DnDSheetDB.DATABASE_VERSION);
    }

    /**
     * To find out if he's a fav (In default DB)
     * @param ctx : Context for access database
     * @return isFav
     */
    public boolean isFav(@NonNull Context ctx) {
        return isFav(ctx, DnDSheetDB.DATABASE_VERSION);
    }

    /**
     * For get all Favs (In default DB)
     * @param ctx : Context for access database
     * @return allFavs
     */
    @NonNull
    public static List<DnDSheet> getFavs(@NonNull Context ctx) {
        return getFavs(ctx, DnDSheetDB.DATABASE_VERSION);
    }

    /**
     * For remove all Favs
     * @param ctx : Context for access database
     * @param dbVersion : Version of database
     */
    public static void removeAllFavs(@NonNull Context ctx, int dbVersion) {
        new DnDSheetDB(ctx, dbVersion).onClear();
        new GIFArtifactDB(ctx, dbVersion).onClear();
        new DnDSavesDB(ctx, dbVersion).onClear();
    }

    /**
     * For remove all Favs (In default DB)
     * @param ctx : Context for access database
     */
    public static void removeAllFavs(@NonNull Context ctx) {
        removeAllFavs(ctx, DnDSheetDB.DATABASE_VERSION);
    }

    /**
     * For write sheet.
     * @return toString
     */
    @Override
    @NonNull
    public String toString() {
        return String.format(
            "DnDSheet of %s with %s", this.caracterClass, this.system
        );
    }

    /**
     * For identify sheet.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(9, 11)
            .append(hp != null ? hp.hashCode() : 1)
            .append(thac9 != null ? thac9.hashCode() : 1)
            .append(ac != null ? ac.hashCode() : 1)
            .append(appearance != null ? appearance.hashCode() : 1)
            .append(caracterClass.hashCode())
            .append(personality != null ? personality.hashCode() : 1)
            .append(system.hashCode()).append(languages.hashCode()).append(equipment.hashCode())
            .append(notes.hashCode()).append(spells.hashCode()).append(saves.hashCode())
            .append(attr.hashCode()).append(skills.hashCode()).toHashCode();
    }

    /**
     * For compare Any Object with this sheet.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @return futures
     */
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx) {
        return getRandomsFutureCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx) {
        return getRandomsCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx,
                                                                       int nb) {
        return getRandomsFutureCaracterSheet(ctx, nb, DnDSystem.getRandomSystem());
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx, int nb) {
        return getRandomsCaracterSheet(ctx, nb, DnDSystem.getRandomSystem());
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx,
                                                                       @NonNull DnDSystem system) {
        return getRandomsFutureCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, system);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx,
                                                         @NonNull DnDSystem system) {
        return getRandomsCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, system);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx,
                                                                       @Nullable DnDClass
                                                                           dnDClass) {
        return getRandomsFutureCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, dnDClass);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx,
                                                         @Nullable DnDClass dnDClass) {
        return getRandomsCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, dnDClass);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx,
                                                                       @NonNull TenorArgs
                                                                               tenorArgs) {
        return getRandomsFutureCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, tenorArgs);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx,
                                                         @NonNull TenorArgs tenorArgs) {
        return getRandomsCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, tenorArgs);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx, int nb,
                                                                       @NonNull TenorArgs
                                                                           tenorArgs) {
        return getRandomsFutureCaracterSheet(ctx, nb, (DnDClass) null, tenorArgs);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx,
                                                                       @NonNull DnDSystem system,
                                                                       @NonNull TenorArgs
                                                                           tenorArgs) {
        return getRandomsFutureCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, system,
            tenorArgs);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param system : DnDSystem represent from which system will be drawn the card
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx, int nb,
                                                                       @NonNull DnDSystem system) {
        return getRandomsFutureCaracterSheet(ctx, nb, system, (DnDClass) null);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx, int nb,
                                                         @NonNull TenorArgs tenorArgs) {
        return getRandomsCaracterSheet(ctx, nb, (DnDClass) null, tenorArgs);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx,
                                                         @NonNull DnDSystem system,
                                                         @NonNull TenorArgs tenorArgs) {
        return getRandomsCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, system, tenorArgs);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param system : DnDSystem represent from which system will be drawn the card
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx, int nb,
                                                         @NonNull DnDSystem system) {
        return getRandomsCaracterSheet(ctx, nb, system, (DnDClass) null);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx,
                                                                       @Nullable DnDClass dnDClass,
                                                                        @NonNull TenorArgs
                                                                               tenorArgs) {
        return getRandomsFutureCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, dnDClass, tenorArgs);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx,
                                                                       @NonNull DnDSystem system,
                                                                       @Nullable DnDClass
                                                                           dnDClass) {
        return getRandomsFutureCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, system, dnDClass);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx,
                                                         @Nullable DnDClass dnDClass,
                                                         @NonNull TenorArgs tenorArgs) {
        return getRandomsCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, dnDClass, tenorArgs);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx,
                                                         @NonNull DnDSystem system,
                                                         @Nullable DnDClass
                                                                 dnDClass) {
        return getRandomsCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, system, dnDClass);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx, int nb,
                                                                       @Nullable DnDClass
                                                                               dnDClass) {
        return getRandomsFutureCaracterSheet(ctx, nb, DnDSystem.getRandomSystem(), dnDClass);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx, int nb,
                                                         @Nullable DnDClass dnDClass) {
        return getRandomsCaracterSheet(ctx, nb, DnDSystem.getRandomSystem(), dnDClass);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx,
                                                                       @NonNull DnDSystem system,
                                                                       @Nullable DnDClass dnDClass,
                                                                       @NonNull TenorArgs
                                                                               tenorArgs) {
        return getRandomsFutureCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, system, dnDClass,
            tenorArgs);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx, int nb,
                                                                       @Nullable DnDClass
                                                                               dnDClass,
                                                                       @NonNull TenorArgs
                                                                               tenorArgs) {
        return getRandomsFutureCaracterSheet(ctx, nb, DnDSystem.getRandomSystem(), dnDClass,
                tenorArgs);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx, int nb,
                                                                       @NonNull DnDSystem system,
                                                                       @NonNull TenorArgs
                                                                               tenorArgs) {
        return getRandomsFutureCaracterSheet(ctx, nb, system, null, tenorArgs);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx, int nb,
                                                                       @NonNull DnDSystem system,
                                                                       @Nullable DnDClass
                                                                               dnDClass) {
        return getRandomsFutureCaracterSheet(ctx, nb, system, dnDClass,
                TenorArgs.DEFAULT);
    }

    /**
     * Method for obtaining a list of future character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheet(@NonNull Context ctx, int nb,
                                                                       @NonNull DnDSystem system,
                                                                       @Nullable DnDClass dnDClass,
                                                                       @NonNull TenorArgs
                                                                               tenorArgs) {
        List<Future<DnDSheet>> futures = new ArrayList<>();
        for (int compt = 0; compt < nb; compt++) {
            futures.add(getRandomFutureCaracterSheet(ctx, system, dnDClass, tenorArgs));
        }
        return futures;
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx,
                                                         @NonNull DnDSystem system,
                                                         @Nullable DnDClass dnDClass,
                                                         @NonNull TenorArgs tenorArgs) {
        return getRandomsCaracterSheet(ctx, DEFAULT_SIZE_LIST_SHEET, system, dnDClass, tenorArgs);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx, int nb,
                                                         @Nullable DnDClass dnDClass,
                                                         @NonNull TenorArgs tenorArgs) {
        return getRandomsCaracterSheet(ctx, nb, DnDSystem.getRandomSystem(), dnDClass, tenorArgs);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx, int nb,
                                                         @NonNull DnDSystem system,
                                                         @NonNull TenorArgs tenorArgs) {
        return getRandomsCaracterSheet(ctx, nb, system, null, tenorArgs);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx, int nb,
                                                         @NonNull DnDSystem system,
                                                         @Nullable DnDClass dnDClass) {
        return getRandomsCaracterSheet(ctx, nb, system, dnDClass, TenorArgs.DEFAULT);
    }

    /**
     * Method for obtaining a list of character cards.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheet(@NonNull Context ctx, int nb,
                                                         @NonNull DnDSystem system,
                                                         @Nullable DnDClass dnDClass,
                                                         @Nullable TenorArgs tenorArgs) {
        return getRandomsCaracterSheetProcess(ctx, nb, system, dnDClass, tenorArgs);
    }

    /**
     * Method for obtaining a list of future character cards from random system.
     * @param ctx : Context for make request for character cards
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>> getRandomsFutureCaracterSheetFromRandomSystem(
            Context ctx) {
        return getRandomsFutureCaracterSheetFromRandomSystem(ctx, DEFAULT_SIZE_LIST_SHEET);
    }

    /**
     * Method for obtaining a list of character cards from random system.
     * @param ctx : Context for make request for character cards
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheetFromRandomSystem(@NonNull Context ctx) {
        return getRandomsCaracterSheetFromRandomSystem(ctx, DEFAULT_SIZE_LIST_SHEET);
    }

    /**
     * Method for obtaining a list of future character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>>
    getRandomsFutureCaracterSheetFromRandomSystem(@NonNull Context ctx, int nb) {
        return getRandomsFutureCaracterSheetFromRandomSystem(ctx, nb, (DnDClass) null);
    }

    /**
     * Method for obtaining a list of character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheetFromRandomSystem(@NonNull Context ctx,
                                                                         int nb) {
        return getRandomsCaracterSheetFromRandomSystem(ctx, nb, (DnDClass) null);
    }

    /**
     * Method for obtaining a list of future character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>>
    getRandomsFutureCaracterSheetFromRandomSystem(@NonNull Context ctx,
                                                  @Nullable DnDClass dnDClass) {
        return getRandomsFutureCaracterSheetFromRandomSystem(ctx, DEFAULT_SIZE_LIST_SHEET,
            dnDClass);
    }

    /**
     * Method for obtaining a list of character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheetFromRandomSystem(@NonNull Context ctx,
                                                                         @Nullable DnDClass
                                                                             dnDClass) {
        return getRandomsCaracterSheetFromRandomSystem(ctx, DEFAULT_SIZE_LIST_SHEET, dnDClass);
    }

    /**
     * Method for obtaining a list of future character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>>
    getRandomsFutureCaracterSheetFromRandomSystem(@NonNull Context ctx,
                                                  @Nullable TenorArgs tenorArgs) {
        return getRandomsFutureCaracterSheetFromRandomSystem(ctx, DEFAULT_SIZE_LIST_SHEET,
                tenorArgs);
    }

    /**
     * Method for obtaining a list of character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheetFromRandomSystem(@NonNull Context ctx,
                                                                         @Nullable TenorArgs
                                                                                 tenorArgs) {
        return getRandomsCaracterSheetFromRandomSystem(ctx, DEFAULT_SIZE_LIST_SHEET, tenorArgs);
    }

    /**
     * Method for obtaining a list of future character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>>
    getRandomsFutureCaracterSheetFromRandomSystem(@NonNull Context ctx,
                                                  @Nullable DnDClass dnDClass,
                                                  @NonNull TenorArgs tenorArgs) {
        return getRandomsFutureCaracterSheetFromRandomSystem(ctx, DEFAULT_SIZE_LIST_SHEET, dnDClass,
            tenorArgs);
    }

    /**
     * Method for obtaining a list of future character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>>
    getRandomsFutureCaracterSheetFromRandomSystem(@NonNull Context ctx, int nb,
                                                  @NonNull TenorArgs tenorArgs) {
        return getRandomsFutureCaracterSheetFromRandomSystem(ctx, nb, null, tenorArgs);
    }

    /**
     * Method for obtaining a list of future character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>>
    getRandomsFutureCaracterSheetFromRandomSystem(@NonNull Context ctx, int nb,
                                                  @Nullable DnDClass dnDClass) {
        return getRandomsFutureCaracterSheetFromRandomSystem(ctx, nb, dnDClass, TenorArgs.DEFAULT);
    }

    /**
     * Method for obtaining a list of character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheetFromRandomSystem(@NonNull Context ctx,
                                                                         @Nullable DnDClass
                                                                                 dnDClass,
                                                                         @NonNull TenorArgs
                                                                                 tenorArgs) {
        return getRandomsCaracterSheetFromRandomSystem(ctx, DEFAULT_SIZE_LIST_SHEET, dnDClass,
                tenorArgs);
    }

    /**
     * Method for obtaining a list of character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheetFromRandomSystem(@NonNull Context ctx,
                                                                         int nb,
                                                                         @NonNull TenorArgs
                                                                                 tenorArgs) {
        return getRandomsCaracterSheetFromRandomSystem(ctx, nb, null, tenorArgs);
    }

    /**
     * Method for obtaining a list of character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheetFromRandomSystem(@NonNull Context ctx,
                                                                         int nb,
                                                                         @Nullable DnDClass
                                                                             dnDClass) {
        return getRandomsCaracterSheetFromRandomSystem(ctx, nb, dnDClass, TenorArgs.DEFAULT);
    }

    /**
     * Method for obtaining a list of future character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return futures
     */
    @NonNull
    public static List<Future<DnDSheet>>
    getRandomsFutureCaracterSheetFromRandomSystem(@NonNull Context ctx, int nb,
                                                  @Nullable DnDClass dnDClass,
                                                  @Nullable TenorArgs tenorArgs) {
        List<Future<DnDSheet>> futures = new ArrayList<>();
        for (int compt = 0; compt < nb; compt++) {
            futures.add(getRandomFutureCaracterSheet(ctx, DnDSystem.getRandomSystem(), dnDClass,
                tenorArgs));
        }
        return futures;
    }

    /**
     * Method for obtaining a list of character cards from random system.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    public static List<DnDSheet> getRandomsCaracterSheetFromRandomSystem(@NonNull Context ctx,
                                                                         int nb,
                                                                         @Nullable DnDClass
                                                                             dnDClass,
                                                                         @Nullable TenorArgs
                                                                             tenorArgs) {
        return getRandomsCaracterSheetProcess(ctx, nb, DnDSystem.getRandomSystem(), dnDClass,
            tenorArgs);
    }

    /**
     * Method for get n unique character sheet.
     * @param ctx : Context for make request for character cards
     * @param nb : Integer represent number of character cards you want
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheets
     */
    @NonNull
    protected static List<DnDSheet> getRandomsCaracterSheetProcess(@NonNull Context ctx, int nb,
                                                                 @Nullable DnDSystem system,
                                                                 @Nullable DnDClass dnDClass,
                                                                 @Nullable TenorArgs tenorArgs) {
        boolean randomSys = system == null;
        List<DnDSheet> sheets = new ArrayList<>();
        int tryValue = 0;
        for (int compt = 0; compt < nb;) {
            tryValue++;
            if (randomSys)
                system = DnDSystem.getRandomSystem();
            DnDSheet sheet = getRandomCaracterSheet(ctx, system, dnDClass, tenorArgs);
            if (!sheets.contains(sheet) || tryValue >= MAX_TRY) {
                sheets.add(sheet);
                compt++;
                tryValue = 0;
            }
        }
        return sheets;
    }

    /**
     * Method for get a future character cards from random system.
     * @param ctx : Context for make request for character cards
     * @return future
     */
    @NonNull
    public static Future<DnDSheet> getRandomFutureCaracterSheet(@NonNull Context ctx) {
        return getRandomFutureCaracterSheet(ctx, DnDSystem.getRandomSystem());
    }

    /**
     * Method for get a character cards from random system.
     * @param ctx : Context for make request for character cards
     * @return sheet
     */
    @NonNull
    public static DnDSheet getRandomCaracterSheet(@NonNull Context ctx) {
        return getRandomCaracterSheet(ctx, (DnDClass) null);
    }

    /**
     * Method for get a future character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @return future
     */
    @NonNull
    public static Future<DnDSheet> getRandomFutureCaracterSheet(@NonNull Context ctx,
                                                                @NonNull DnDSystem system) {
        return getRandomFutureCaracterSheet(ctx, system, (DnDClass) null);
    }

    /**
     * Method for get a character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @return sheet
     */
    @NonNull
    public static DnDSheet getRandomCaracterSheet(@NonNull Context ctx, @NonNull DnDSystem system) {
        return getRandomCaracterSheet(ctx, system, (DnDClass) null);
    }

    /**
     * Method for get a future character cards.
     * @param ctx : Context for make request for character cards
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return future
     */
    @NonNull
    public static Future<DnDSheet> getRandomFutureCaracterSheet(@NonNull Context ctx,
                                                                @NonNull DnDClass dnDClass) {
        return getRandomFutureCaracterSheet(ctx, DnDSystem.getRandomSystem(), dnDClass);
    }

    /**
     * Method for get a character cards.
     * @param ctx : Context for make request for character cards
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @return sheet
     */
    @NonNull
    public static DnDSheet getRandomCaracterSheet(@NonNull Context ctx,
                                                  @NonNull DnDClass dnDClass) {
        return getRandomCaracterSheet(ctx, DnDSystem.getRandomSystem(), dnDClass);
    }

    /**
     * Method for get a character cards.
     * @param ctx : Context for make request for character cards
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheet
     */
    @NonNull
    public static DnDSheet getRandomCaracterSheet(@NonNull Context ctx,
                                                  @NonNull TenorArgs tenorArgs) {
        return getRandomCaracterSheet(ctx, DnDSystem.getRandomSystem(), tenorArgs);
    }

    /**
     * Method for get a future character cards.
     * @param ctx : Context for make request for character cards
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return future
     */
    @NonNull
    public static Future<DnDSheet> getRandomFutureCaracterSheet(@NonNull Context ctx,
                                                                @Nullable DnDClass caracterClass,
                                                                @NonNull TenorArgs tenorArgs) {
        SimpleFuture<DnDSheet> future = new SimpleFuture<>();
        getRandomFutureCaracterSheet(ctx, DnDSystem.getRandomSystem(), future, caracterClass,
            tenorArgs);
        return future;
    }

    /**
     * Method for get a future character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param caracterClass : DnDClass represent with which class will be drawn the card
     * @return future
     */
    @NonNull
    public static Future<DnDSheet> getRandomFutureCaracterSheet(@NonNull Context ctx,
                                                                @NonNull DnDSystem system,
                                                                @Nullable DnDClass caracterClass) {
        SimpleFuture<DnDSheet> future = new SimpleFuture<>();
        getRandomFutureCaracterSheet(ctx, system, future, caracterClass);
        return future;
    }

    /**
     * Method for get a future character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return future
     */
    @NonNull
    public static Future<DnDSheet> getRandomFutureCaracterSheet(@NonNull Context ctx,
                                                                @NonNull DnDSystem system,
                                                                @NonNull TenorArgs tenorArgs) {
        SimpleFuture<DnDSheet> future = new SimpleFuture<>();
        getRandomFutureCaracterSheet(ctx, system, future, tenorArgs);
        return future;
    }

    /**
     * Method for get a future character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param caracterClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return future
     */
    @NonNull
    public static Future<DnDSheet> getRandomFutureCaracterSheet(@NonNull Context ctx,
                                                                @NonNull DnDSystem system,
                                                                @Nullable DnDClass caracterClass,
                                                                @NonNull TenorArgs tenorArgs) {
        SimpleFuture<DnDSheet> future = new SimpleFuture<>();
        getRandomFutureCaracterSheet(ctx, system, future, caracterClass, tenorArgs);
        return future;
    }

    /**
     * Method for get a character cards.
     * @param ctx : Context for make request for character cardsd
     * @param caracterClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheet
     */
    @NonNull
    public static DnDSheet getRandomCaracterSheet(@NonNull Context ctx,
                                                  @Nullable DnDClass caracterClass,
                                                  @NonNull TenorArgs tenorArgs) {
        try {
            return getRandomFutureCaracterSheet(ctx, caracterClass, tenorArgs).get();
        } catch (ExecutionException | InterruptedException e) {
            return getRandomCaracterSheet(ctx, caracterClass, tenorArgs);
        }
    }

    /**
     * Method for get a character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param caracterClass : DnDClass represent with which class will be drawn the card
     * @return sheet
     */
    @NonNull
    public static DnDSheet getRandomCaracterSheet(@NonNull Context ctx, @NonNull DnDSystem system,
                                                  @Nullable DnDClass caracterClass) {
        try {
            return getRandomFutureCaracterSheet(ctx, system, caracterClass).get();
        } catch (ExecutionException | InterruptedException e) {
            return getRandomCaracterSheet(ctx, system, caracterClass);
        }
    }

    /**
     * Method for get a character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheet
     */
    @NonNull
    public static DnDSheet getRandomCaracterSheet(@NonNull Context ctx, @NonNull DnDSystem system,
                                                  @NonNull TenorArgs tenorArgs) {
        try {
            return getRandomFutureCaracterSheet(ctx, system, tenorArgs).get();
        } catch (ExecutionException | InterruptedException e) {
            return getRandomCaracterSheet(ctx, system, tenorArgs);
        }
    }

    /**
     * Method for get a character cards.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param caracterClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     * @return sheet
     */
    @NonNull
    public static DnDSheet getRandomCaracterSheet(@NonNull Context ctx, @NonNull DnDSystem system,
                                                  @Nullable DnDClass caracterClass,
                                                  @NonNull TenorArgs tenorArgs) {
        try {
            return getRandomFutureCaracterSheet(ctx, system, caracterClass, tenorArgs).get();
        } catch (ExecutionException | InterruptedException e) {
            return getRandomCaracterSheet(ctx, system, caracterClass, tenorArgs);
        }
    }

    /**
     * Method performing the api query for the given system and will place the result in the future
     * past as argument.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param future : SimpleFuture represents the object in which the result will be stored
     */
    protected static void getRandomFutureCaracterSheet(@NonNull Context ctx,
                                                     @NonNull DnDSystem system,
                                                     @NonNull SimpleFuture<DnDSheet> future) {
        DnDSheet.getRandomFutureCaracterSheet(ctx, system, future, (DnDClass) null);
    }

    /**
     * Method performing the api query for the given system and will place the result in the future
     * past as argument.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param future : SimpleFuture represents the object in which the result will be stored
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     */
    protected static void getRandomFutureCaracterSheet(@NonNull Context ctx,
                                                     @NonNull DnDSystem system,
                                                     @NonNull SimpleFuture<DnDSheet> future,
                                                     @Nullable DnDClass dnDClass) {
        DnDSheet.getRandomFutureCaracterSheet(ctx, system, future, dnDClass, TenorArgs.DEFAULT);
    }

    /**
     * Method performing the api query for the given system and will place the result in the future
     * past as argument.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param future : SimpleFuture represents the object in which the result will be stored
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     */
    protected static void getRandomFutureCaracterSheet(@NonNull Context ctx,
                                                     @NonNull DnDSystem system,
                                                     @NonNull SimpleFuture<DnDSheet> future,
                                                     @NonNull TenorArgs tenorArgs) {
        DnDSheet.getRandomFutureCaracterSheet(ctx, system, future, null, tenorArgs);
    }

    /**
     * Method performing the api query for the given system and will place the result in the future
     * past as argument.
     * @param ctx : Context for make request for character cards
     * @param system : DnDSystem represent from which system will be drawn the card
     * @param future : SimpleFuture represents the object in which the result will be stored
     * @param dnDClass : DnDClass represent with which class will be drawn the card
     * @param tenorArgs : TenorArgs represent with which arguments, the media related to card is get
     */
    protected static void getRandomFutureCaracterSheet(@NonNull Context ctx,
                                                     @NonNull DnDSystem system,
                                                     @NonNull SimpleFuture<DnDSheet> future,
                                                     @Nullable DnDClass dnDClass,
                                                     @NonNull TenorArgs tenorArgs) {
        DnDSystem finalSystem = system == DnDSystem.anyone ? DnDSystem.getRandomSystem() : system;
        boolean dnDClassProvided = (dnDClass != null && dnDClass.queryArg != null &&
            dnDClass != DnDClass.ANYONE );
        String target = String.format("%s%s/json/%s", apiProvider, finalSystem.name,
            dnDClassProvided ? String.format("?class='%s'",dnDClass.queryArg) : "" );
        Ion.with(ctx).load(target).noCache()
        .setLogging(String.format("%s-Ion", TAG), Log.DEBUG)
        .asJsonObject().withResponse().setCallback((e, response) -> {
            Log.d(TAG,
                String.format("Request submit and response receveid !%nProcessing...")
            );
            if (response != null && response.getHeaders().code() == 200) {
                JsonNull jsonNull = JsonNull.INSTANCE;
                JsonObject json = response.getResult().getAsJsonObject();
                List<DnDAttr> attr = new ArrayList<>();
                JsonArray attrsData = json.getAsJsonArray("attributes");
                JsonObject attrBeautiful = json.getAsJsonObject("attr");
                for (int i = 0; i < attrsData.size(); i++) {
                    JsonArray attrData = attrsData.get(i).getAsJsonArray();
                    String nameAttr = attrData.get(0).getAsString();
                    String beautifulAttr = attrBeautiful.get(nameAttr) != jsonNull ?
                        attrBeautiful.get(nameAttr).getAsString() : null;
                    if (beautifulAttr != null)
                        attr.add(new DnDAttr(nameAttr, attrData.get(1).getAsInt(), beautifulAttr));
                    else
                        attr.add(new DnDAttr(nameAttr, attrData.get(1).getAsInt()));
                }
                List<String> languages = new ArrayList<>();
                if (json.get("languages") != jsonNull)
                    for (JsonElement lang : json.get("languages").getAsJsonArray())
                        languages.add(lang.getAsString());
                List<String> equipment = new ArrayList<>();
                if (json.get("equipment") != jsonNull)
                    for (JsonElement equip : json.get("equipment").getAsJsonArray())
                        equipment.add(equip.getAsString());
                List<String> notes = new ArrayList<>();
                if (json.get("notes") != jsonNull)
                    for (JsonElement note : json.get("notes").getAsJsonArray())
                        notes.add(note.getAsString());
                List<String> spells = new ArrayList<>();
                if (json.get("spell") != jsonNull)
                    for (JsonElement spell : json.get("spell").getAsJsonArray())
                        spells.add(spell.getAsString());
                List<DnDSkill> skills = new ArrayList<>();
                if (json.get("skills") != jsonNull)
                    for (JsonElement skillData : json.get("skills").getAsJsonArray()) {
                        JsonArray skillDataRefined = skillData.getAsJsonArray();
                        skills.add(new DnDSkill(
                            skillDataRefined.get(0).getAsString(),
                            skillDataRefined.get(1).getAsInt()
                        ));
                    }
                JsonObject jsonSaves = json.get("saves") != jsonNull ? json.get("saves")
                    .getAsJsonObject() : null;
                future.setComplete(new DnDSheet(
                    json.get("hp") != jsonNull ? json.get("hp").getAsInt() : null,
                    json.get("thac9") != jsonNull ? json.get("thac9").getAsInt() : null,
                    json.get("ac") != jsonNull ? json.get("ac").getAsInt() : null,
                    json.get("appearance") != jsonNull ? json.get("appearance")
                        .getAsString() : null,
                    dnDClassProvided ? dnDClass : new DnDClass(json.get("class").getAsString()),
                    json.get("personality") != jsonNull ? json.get("personality")
                        .getAsString() : null,
                    finalSystem, languages, equipment, notes, spells, jsonSaves != null ? new
                        DnDSaves(
                            jsonSaves.get("wands") != jsonNull ? jsonSaves.get("wands")
                                .getAsInt() : null,
                            jsonSaves.get("breath") != jsonNull ? jsonSaves.get("breath")
                                .getAsInt() : null,
                            jsonSaves.get("stone") != jsonNull ? jsonSaves.get("stone")
                                .getAsInt() : null,
                            jsonSaves.get("magic") != jsonNull ? jsonSaves.get("magic")
                                .getAsInt() : null,
                            jsonSaves.get("poison") != jsonNull ? jsonSaves.get("poison")
                                .getAsInt() : null
                    ) : null, new DnDAttrs(attr), new DnDSkills(skills), ctx, tenorArgs)
                );
                Log.d(TAG, "Result set in OwnFuture");
            } else {
                Log.wtf(TAG, response != null ?
                    String.format("Error code %d for %s!, retrying...",
                        response.getHeaders().code(), target):
                    String.format("Null response provided for %s", target)
                );
                getRandomFutureCaracterSheet(ctx, finalSystem, future, dnDClass, tenorArgs);
            }
        });
    }
}
