package fr.iutnantes.miar.dndgenerator.api.tenor;

import androidx.annotation.NonNull;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Class representing the dimensions of a media provided by Tenor
 */
public class Dimension implements Serializable {

    public final Integer width;
    public final Integer height;

    /**
     * Basic Constructor for create Dimension from List.
     * @param rawData : List containing raw media dimensions
     */
    public Dimension(@NonNull List<Integer> rawData) {
        if (rawData.size() >= 2 && rawData.get(0) != null && rawData.get(1) != null) {
            this.width = rawData.get(0);
            this.height = rawData.get(1);
        } else
            throw new IllegalArgumentException();
    }

    /**
     * Basic Constructor for create attribute if all arguments are provided.
     * @param width : Integer represent width of media
     * @param height : Integer represent height of media
     */
    public Dimension(@NonNull Integer width, @NonNull Integer height) {
        this.width = width;
        this.height = height;
    }

    /**
     * For write dimensions.
     * @return toString
     */
    @NonNull
    @Override
    public String toString() {
        return String.format("W:%d;H:%d", height, width);
    }

    /**
     * For compare Any Object with this dimensions.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }

    /**
     * For identify dimensions.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 19)
            .append(width).append(height).toHashCode();
    }
}
