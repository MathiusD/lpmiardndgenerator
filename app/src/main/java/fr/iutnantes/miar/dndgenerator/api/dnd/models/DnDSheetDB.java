package fr.iutnantes.miar.dndgenerator.api.dnd.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import fr.iutnantes.miar.dndgenerator.api.BaseDB;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDAttr;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDAttrs;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDClass;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSheet;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSkill;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSkills;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSystem;
import fr.iutnantes.miar.dndgenerator.api.tenor.GIFObject;
import fr.iutnantes.miar.dndgenerator.api.tenor.models.GIFArtifactDB;

/**
 * Class representing the storage of favorites
 */
public class DnDSheetDB extends BaseDB {
    /**
     * Method for create table in database
     * @param db : sql connector linked to database
     * @param dbVersion : Version of database
     */
    @Override
    public void onCreate(@NonNull SQLiteDatabase db, @NonNull Integer dbVersion) {
        onCreate(db, allStringAttrs, intAttrs, DATABASE_NAME, dbVersion, PRIMARY_KEY);
    }
    /**
     * Constructor for build database in specific version
     * @param context : Context for access database
     * @param dbVersion : Version of database
     */
    public DnDSheetDB(@NonNull Context context, int dbVersion) {
        super(context, DATABASE_NAME, dbVersion);
    }
    /**
     * Constructor for build default database
     * @param context : Context for access database
     */
    public DnDSheetDB(@NonNull Context context) {
        this(context, DATABASE_VERSION);
    }
    public final static String PRIMARY_KEY;
    public final static String TABLE_NAME;
    public final static int DATABASE_VERSION;
    public final static String DATABASE_NAME;
    public final static List<String> intAttrs;
    public final static List<String> stringAttrs;
    public final static List<String> allStringAttrs;
    public final static List<String> listStringAttrs;

    /**
     * Class represent fields in current model
     */
    protected static class Vars implements BaseColumns {
        public static final String TABLE_NAME = "DnDSheet";
        public static final String HP = "hp";
        public static final String THAC9 = "thac9";
        public static final String AC = "ac";
        public static final String APPEARANCE = "appearance";
        public static final String PERSONALITY = "personality";
        public static final String LANGUAGES = "languages";
        public static final String EQUIPMENT = "equipment";
        public static final String NOTES = "notes";
        public static final String SPELLS = "spells";
        public static final String GIF_ID = "gifId";
        public static final String GIF_ARTEFACT_ID = "gifArtefactId";
        public static final String SKILLS = "skills";
        public static final String SYSTEM_ID = "system_id";
        public static final String CLASS = "class";
        public static final String SAVES_ID = "saves_id";
        public static final String ATTRS = "attrs";
        public static final String NAME = "name";
    }
    static {
        PRIMARY_KEY = Vars._ID;
        TABLE_NAME = Vars.TABLE_NAME;
        DATABASE_VERSION = 7;
        DATABASE_NAME = TABLE_NAME;
        List<String> tempInt = new ArrayList<>();
        tempInt.add(Vars.HP);
        tempInt.add(Vars.THAC9);
        tempInt.add(Vars.AC);
        tempInt.add(Vars.GIF_ARTEFACT_ID);
        tempInt.add(Vars.SAVES_ID);
        tempInt.add(Vars.SYSTEM_ID);
        intAttrs = tempInt;
        List<String> tempListString = new ArrayList<>();
        tempListString.add(Vars.LANGUAGES);
        tempListString.add(Vars.EQUIPMENT);
        tempListString.add(Vars.NOTES);
        tempListString.add(Vars.SPELLS);
        tempListString.add(Vars.SKILLS);
        tempListString.add(Vars.ATTRS);
        listStringAttrs = tempListString;
        List<String> tempString = new ArrayList<>();
        tempString.add(Vars.APPEARANCE);
        tempString.add(Vars.PERSONALITY);
        tempString.add(Vars.GIF_ID);
        tempString.add(Vars.CLASS);
        tempString.add(Vars.NAME);
        stringAttrs = tempString;
        List<String> tempAllString = new ArrayList<>();
        tempAllString.addAll(stringAttrs);
        tempAllString.addAll(listStringAttrs);
        allStringAttrs = tempAllString;
    }

    /**
     * Method for remove a sheet
     * @param sheet : Sheet to remove
     * @return isRemoved
     */
    public boolean removeDnDSheet(DnDSheet sheet) {
        if (getDnDSheet(sheet.hashCode()) == null)
            return false;
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(tableName(), String.format("%s = ?", Vars._ID), new String[]{String
            .valueOf(sheet.hashCode())});
        return true;
    }

    /**
     * Method for update GIF of sheet
     * @param sheet : Sheet to update
     * @return isUpdated
     */
    public boolean updateGIF(DnDSheet sheet) {
        if (getDnDSheet(sheet.hashCode()) == null)
            return false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Vars.GIF_ID, sheet.getGIF().id);
        values.put(Vars.GIF_ARTEFACT_ID, sheet.getGIF().artifact.hashCode());
        new GIFArtifactDB(ctx).insertGIFArtifact(sheet.getGIF().artifact);
        db.update(tableName(), values, String.format("%s = ?", Vars._ID), new String[]{
                String.valueOf(sheet.hashCode())});
        return true;
    }

    /**
     * Method for update Name of sheet
     * @param sheet : Sheet to update
     * @return isUpdated
     */
    public boolean updateName(DnDSheet sheet) {
        if (getDnDSheet(sheet.hashCode()) == null)
            return false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Vars.NAME, sheet.getName());
        db.update(tableName(), values, String.format("%s = ?", Vars._ID), new String[]{
                String.valueOf(sheet.hashCode())});
        return true;
    }

    /**
     * Method for get all sheets save in database
     * @return sheets
     */
    public List<DnDSheet> getAllDnDSheet() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(tableName(), null, null, null,
            null, null, null);
        List<DnDSheet> sheets = new ArrayList<>();
        while(cursor.moveToNext()) {
            sheets.add(transformRecordToDnDSheet(cursor));
        }
        return sheets;
    }

    /**
     * Method for transform record in database to DnDSheet
     * @param cursor : Cursor element point a record
     * @return sheet
     */
    @NonNull
    protected DnDSheet transformRecordToDnDSheet(Cursor cursor) {
        GIFObject gif = GIFObject.getGIFObjectById(ctx, cursor.getString(
                cursor.getColumnIndex(Vars.GIF_ID)), new GIFArtifactDB(ctx, currentVersion)
                .getGIFArtifact(cursor.getInt(cursor.getColumnIndex(Vars.GIF_ARTEFACT_ID))));
        List<DnDAttr> attrs = new ArrayList<>();
        for(String rawAttr : cursor.getString(cursor.getColumnIndex(Vars.ATTRS))
                .split(",")) {
            String[] splited = rawAttr.split("~");
            if (splited.length > 1)
                attrs.add(new DnDAttr(splited[0], Integer.valueOf(splited[2]), splited[1]));
        }
        List<DnDSkill> skills = new ArrayList<>();
        for(String rawSkill : cursor.getString(cursor.getColumnIndex(Vars.SKILLS))
                .split(",")) {
            String[] splited = rawSkill.split("~");
            if (splited.length > 1)
                skills.add(new DnDSkill(splited[0], Integer.valueOf(splited[1])));
        }
        List<String> langs = new ArrayList<>();
        for (String lang : cursor.getString(cursor.getColumnIndex(Vars.LANGUAGES))
                .split(","))
            if (!lang.equals(""))
                langs.add(lang);
        List<String> equips = new ArrayList<>();
        for (String equip : cursor.getString(cursor.getColumnIndex(Vars.EQUIPMENT))
                .split(","))
            if (!equip.equals(""))
                equips.add(equip);
        List<String> notes = new ArrayList<>();
        for (String note : cursor.getString(cursor.getColumnIndex(Vars.NOTES))
                .split(","))
            if (!note.equals(""))
                notes.add(note);
        List<String> spells = new ArrayList<>();
        for (String spell : cursor.getString(cursor.getColumnIndex(Vars.SPELLS))
                .split(","))
            if (!spell.equals(""))
                spells.add(spell);
        return new DnDSheet(
                cursor.getInt(cursor.getColumnIndex(Vars.HP)),
                cursor.getInt(cursor.getColumnIndex(Vars.THAC9)),
                cursor.getInt(cursor.getColumnIndex(Vars.AC)),
                cursor.getString(cursor.getColumnIndex(Vars.APPEARANCE)),
                new DnDClass(cursor.getString(cursor.getColumnIndex(Vars.CLASS))),
                cursor.getString(cursor.getColumnIndex(Vars.PERSONALITY)),
                DnDSystem.systems.get(cursor.getInt(cursor.getColumnIndex(Vars.SYSTEM_ID))),
                langs, equips, notes, spells,
                new DnDSavesDB(ctx, currentVersion).getDnDSaves(cursor.getInt(cursor.getColumnIndex(
                        Vars.SAVES_ID))),
                new DnDAttrs(attrs), new DnDSkills(skills), gif,
                cursor.getString(cursor.getColumnIndex(Vars.NAME))
        );
    }

    /**
     * Method for get sheet
     * @param id : HashCode of sheet
     * @return sheet
     */
    @Nullable
    public DnDSheet getDnDSheet(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(tableName(), null, String.format("%s = ?", Vars._ID),
            new String[]{String.valueOf(id)}, null, null, null);
        if (cursor.moveToFirst()) {
            return transformRecordToDnDSheet(cursor);
        }
        return null;
    }

    /**
     * Method for insert Sheet if not already in database
     * @param sheet : Sheet to save
     * @return isInserted
     */
    public boolean insertDnDSheet(DnDSheet sheet) {
        if (getDnDSheet(sheet.hashCode()) != null)
            return false;
        ContentValues values = new ContentValues();
        values.put(Vars.HP, sheet.hp != null ? sheet.hp : -1);
        values.put(Vars.THAC9, sheet.thac9 != null ? sheet.thac9 : -1);
        values.put(Vars.AC, sheet.ac != null ? sheet.ac : -1);
        values.put(Vars.SYSTEM_ID, DnDSystem.systems.indexOf(sheet.system));
        String languages = "";
        for (String lang: sheet.languages)
            languages = String.format("%s%s,", languages, lang);
        values.put(Vars.LANGUAGES, languages.length() > 0 ? languages.substring(0,
            languages.length() - 1) : "");
        String equipements = "";
        for (String equipement: sheet.equipment)
            equipements = String.format("%s%s,", equipements, equipement);
        values.put(Vars.EQUIPMENT, equipements.length() > 0 ? equipements.substring(0,
            equipements.length() - 1) : "");
        String notes = "";
        for (String note: sheet.notes)
            notes = String.format("%s%s,", notes, note);
        values.put(Vars.NOTES, notes.length() > 0 ? notes.substring(0, notes.length() - 1) : "");
        String spells = "";
        for (String spell: sheet.spells)
            spells = String.format("%s%s,", spells, spell);
        values.put(Vars.SPELLS, spells.length() > 0 ? spells.substring(0, spells.length() - 1) :
            "");
        String attrs = "";
        for (DnDAttr attr: sheet.attr.attrs)
            attrs = String.format("%s%s~%s~%d,", attrs, attr.nameAttr, attr.valueUserReadable,
                attr.value);
        values.put(Vars.ATTRS, attrs.length() > 0 ? attrs.substring(0, attrs.length() - 1) : "");
        String skills = "";
        for (DnDSkill skill: sheet.skills.skills)
            skills = String.format("%s%s~%d,", skills, skill.skillName, skill.value);
        values.put(Vars.SKILLS, skills.length() > 0 ? skills.substring(0, skills.length() - 1)
            : "");
        values.put(Vars.APPEARANCE, sheet.appearance);
        values.put(Vars.PERSONALITY, sheet.personality);
        values.put(Vars.CLASS, sheet.caracterClass.name);
        values.put(Vars.GIF_ID, sheet.getGIF().id);
        values.put(Vars.GIF_ARTEFACT_ID, sheet.getGIF().artifact.hashCode());
        new GIFArtifactDB(ctx, currentVersion).insertGIFArtifact(sheet.getGIF().artifact);
        values.put(Vars.SAVES_ID, sheet.saves.hashCode());
        new DnDSavesDB(ctx, currentVersion).insertDnDSaves(sheet.saves);
        values.put(Vars.NAME, sheet.getName());
        values.put(Vars._ID, sheet.hashCode());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(tableName(), null, values);
        return true;
    }
}
