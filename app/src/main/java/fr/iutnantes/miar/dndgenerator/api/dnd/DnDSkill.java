package fr.iutnantes.miar.dndgenerator.api.dnd;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * DnDSkill is a class representing a character's skills, it is currently only used by the following
 * system: "Lamentations Of The Flame Princess".
 */
public class DnDSkill implements Serializable {

    public final String skillName;
    public final Integer value;

    /**
     * Basic Constructor for create skill if all arguments are provided.
     * @param skillName : String to represent name of this skill
     * @param value : Integer to represent value of this skill
     */
    public DnDSkill(@NonNull String skillName, @NonNull Integer value) {
        this.skillName = skillName;
        this.value = value;
    }

    /**
     * For write skill.
     * @return toString
     */
    @NonNull
    @Override
    public String toString() {
        return String.format("%s : %s", skillName, value);
    }

    /**
     * For identify skill.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(13, 15)
            .append(skillName.hashCode()).append(value.hashCode()).toHashCode();
    }

    /**
     * For compare Any Object with this skill.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }
}
