package fr.iutnantes.miar.dndgenerator.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Objects;

import fr.iutnantes.miar.dndgenerator.DisplayResultActivity;
import fr.iutnantes.miar.dndgenerator.R;
import fr.iutnantes.miar.dndgenerator.api.tenor.GIFFormat;
import fr.iutnantes.miar.dndgenerator.api.tenor.GIFObject;

/**
 * Adapter for list of GIF in DisplayGif.
 */
public class ListGifAdapter extends RecyclerView.Adapter<ListGifAdapter.ViewHolder> {
    public static final String GIF_OBJECT = "gifObject";
    private static String TAG = "ListGifAdapter ::";
    private List<GIFObject> m_data;
    private Activity m_activity;

    /**
     * Class to set the view of one result
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView m_gif1;

        /**
         * Constructor
         *
         * @param p_view : view that contain data
         */
        public ViewHolder(View p_view) {
            super(p_view);

            m_gif1 = p_view.findViewById(R.id.img_patternGif);
        }

        /**
         * Getter image 1
         * @return : ImageView of image 1
         */
        public ImageView getM_gif1() {
            return m_gif1;
        }
    }

    /**
     * Constructor
     *
     * @param p_data    : List of DnDSheets
     * @param p_activity : Activity
     */
    public ListGifAdapter(List<GIFObject> p_data, Activity p_activity) {
        m_data = p_data;
        m_activity = p_activity;
    }

    /**
     * Inflae the View
     *
     * @param parent   : parent view
     * @param viewType : Type of view
     * @return : View holder that'll contains the data
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v_view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_pattern_gif, parent, false);
        return new ViewHolder(v_view);
    }

    /**
     * Link data and view
     *
     * @param holder   : the view that'll contain data
     * @param position : position in the list
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String v_url1 = Objects.requireNonNull(Objects.requireNonNull(m_data.get(position).media.get(GIFFormat.GIF)).url);

        Glide.with(holder.itemView)
                .load(v_url1)
                .into(holder.getM_gif1());

        //TODO : Set onClickListener avec retour de résultat
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent v_intent = new Intent();
                Bundle v_bundle = new Bundle();
                v_bundle.putSerializable(GIF_OBJECT, m_data.get(position));
                v_intent.putExtras(v_bundle);
                m_activity.setResult(DisplayResultActivity.GIF_RESULT_CODE, v_intent);
                m_activity.finish();
            }
        });
    }

    /**
     * Get the number of elements
     *
     * @return : Size of the list of elements
     */
    @Override
    public int getItemCount() {
        if(m_data != null) {
            return m_data.size();
        }
        return 0;
    }
}
