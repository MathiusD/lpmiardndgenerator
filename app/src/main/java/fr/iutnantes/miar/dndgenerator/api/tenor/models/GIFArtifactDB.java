package fr.iutnantes.miar.dndgenerator.api.tenor.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import fr.iutnantes.miar.dndgenerator.api.BaseDB;
import fr.iutnantes.miar.dndgenerator.api.dnd.models.DnDSheetDB;
import fr.iutnantes.miar.dndgenerator.api.tenor.FilterContent;
import fr.iutnantes.miar.dndgenerator.api.tenor.GIFArtifact;
import fr.iutnantes.miar.dndgenerator.api.tenor.MediaFilter;

/**
 * Class representing GIFArtifact storage for bookmarks.
 */
public class GIFArtifactDB extends BaseDB {

    /**
     * Method for create table in database
     * @param db : sql connector linked to database
     * @param dbVersion : Version of database
     */
    @Override
    public void onCreate(@NonNull SQLiteDatabase db, @NonNull Integer dbVersion) {
        onCreate(db, allStringAttrs, intAttrs, DATABASE_NAME, dbVersion, PRIMARY_KEY);
    }

    /**
     * Constructor for build database in specific version
     * @param context : Context for access database
     * @param dbVersion : Version of database
     */
    public GIFArtifactDB(@NonNull Context context, int dbVersion) {
        super(context, DATABASE_NAME, dbVersion);
    }

    /**
     * Constructor for build default database
     * @param context : Context for access database
     */
    public GIFArtifactDB(@NonNull Context context) {
        this(context, DATABASE_VERSION);
    }
    public final static String PRIMARY_KEY;
    public final static String TABLE_NAME;
    public final static int DATABASE_VERSION;
    public final static String DATABASE_NAME;
    public final static List<String> intAttrs;
    public final static List<String> stringAttrs;
    public final static List<String> allStringAttrs;
    public final static List<String> listStringAttrs = new ArrayList<>();

    /**
     * Class represent fields in current model
     */
    protected static class Vars implements BaseColumns {
        public static final String TABLE_NAME = "GIFArtifact";
        public static final String QUERY = "query";
        public static final String MEDIA_FILTER_ID = "mediaFilterId";
        public static final String CONTENT_FILTER_ID = "contentFilterId";
    }
    static {
        PRIMARY_KEY = Vars._ID;
        TABLE_NAME = Vars.TABLE_NAME;
        DATABASE_VERSION = DnDSheetDB.DATABASE_VERSION;
        DATABASE_NAME = TABLE_NAME;
        List<String> tempInt = new ArrayList<>();
        tempInt.add(Vars.MEDIA_FILTER_ID);
        tempInt.add(Vars.CONTENT_FILTER_ID);
        intAttrs = tempInt;
        List<String> tempString = new ArrayList<>();
        tempString.add(Vars.QUERY);
        stringAttrs = tempString;
        List<String> tempAllString = new ArrayList<>();
        tempAllString.addAll(stringAttrs);
        tempAllString.addAll(listStringAttrs);
        allStringAttrs = tempAllString;
    }

    /**
     * Method for get artifact
     * @param id : HashCode of artifact
     * @return artifact
     */
    @Nullable
    public GIFArtifact getGIFArtifact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(tableName(), null, String.format("%s = ?", Vars._ID),
                new String[]{String.valueOf(id)}, null, null, null);
        if (cursor.moveToFirst()) {
            return new GIFArtifact(
                cursor.getString(cursor.getColumnIndex(Vars.QUERY)),
                MediaFilter.filters.get(cursor.getInt(
                    cursor.getColumnIndex(Vars.MEDIA_FILTER_ID))),
                FilterContent.filters.get(cursor.getInt(
                    cursor.getColumnIndex(Vars.CONTENT_FILTER_ID)
                ))
            );
        }
        return null;
    }

    /**
     * Method for insert artifact if not already in database
     * @param artifact : Artifact to save
     * @return isInserted
     */
    public boolean insertGIFArtifact(GIFArtifact artifact) {
        if (getGIFArtifact(artifact.hashCode()) != null)
            return false;
        ContentValues values = new ContentValues();
        values.put(Vars.CONTENT_FILTER_ID, FilterContent.filters.indexOf(artifact.filterContent));
        values.put(Vars.MEDIA_FILTER_ID, MediaFilter.filters.indexOf(artifact.mediaFilter));
        values.put(Vars.QUERY, artifact.query);
        values.put(Vars._ID, artifact.hashCode());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(tableName(), null, values);
        return true;
    }
}
