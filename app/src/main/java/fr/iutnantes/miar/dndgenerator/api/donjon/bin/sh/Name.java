                                                                                                                package fr.iutnantes.miar.dndgenerator.api.donjon.bin.sh;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.koushikdutta.async.future.SimpleFuture;
import com.koushikdutta.ion.Ion;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static fr.iutnantes.miar.dndgenerator.api.utils.ListUtils.joinList;

/**
 * Name is a class represent Name Generator from donjon.bin.sh
 */
public class Name implements Serializable {

    public static final String apiProvider = "https://donjon.bin.sh/";
    public static final String nameDelimiter = "\n";
    public static final String regexUsed = "[,;| ]";
    protected static final String TAG = "donjon.bin.sh.Name";

    public final String specie;
    public final String type;

    /**
     * Basic Constructor for name
     * @param specie Specie related to this name generator
     * @param type Type related to this name generator
     */
    protected Name(@NonNull String specie, @Nullable String type) {
        this.specie = specie;
        this.type = type;
    }

    /**
     * For write name.
     * @return toString
     */
    @Override
    @NonNull
    public String toString() {
        return String.format(
            "%s of %s with %s", TAG, specie, type
        );
    }

    /**
     * For identify name.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(65, 11)
            .append(specie).append(type != null ? type.hashCode() : 1).toHashCode();
    }

    /**
     * For compare Any Object with this name.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }

    /**
     * For check if this Name Generator match with keywords given
     * @param keywords : String represent keyword used
     * @return matched
     */
    public boolean matchWith(String... keywords) {
        return matchWith(Arrays.asList(keywords));
    }

    /**
     * For check if this Name Generator match with list of keywords given
     * @param keywords : List of String represent keyword used
     * @return matched
     */
    public boolean matchWith(List<String> keywords) {
        return matchWithSpecie(keywords) && matchWithType(keywords);
    }

    /**
     * For check if this Name Generator match almost with keywords given
     * @param keywords : String represent keyword used
     * @return matched
     */
    public boolean matchAlmostWith(String... keywords) {
        return matchAlmostWith(Arrays.asList(keywords));
    }

    /**
     * For check if this Name Generator match almost with list of keywords given
     * @param keywords : List of String represent keyword used
     * @return matched
     */
    public boolean matchAlmostWith(List<String> keywords) {
        return matchWithSpecie(keywords) || type != null && matchWithType(keywords);
    }

    /**
     * For check if this Name Generator specie match with list of keywords given
     * @param keywords : List of String represent keyword used
     * @return matched
     */
    public boolean matchWithSpecie(List<String> keywords) {
        boolean matched = false;
        for (String keyword: keywords) {
            String word = keyword.trim();
            if (!matched && word.split(regexUsed).length > 1)
                matched = matchWithSpecie(Arrays.asList(word.split(regexUsed)));
            if (!matched && specie.equalsIgnoreCase(word))
                matched = true;
        }
        return matched;
    }

    /**
     * For check if this Name Generator type match with list of keywords given
     * @param keywords : List of String represent keyword used
     * @return matched
     */
    public boolean matchWithType(List<String> keywords) {
        boolean matched = type == null;
        if (type != null)
            for (String keyword: keywords) {
                String word = keyword.trim();
                if (!matched && word.split(regexUsed).length > 1)
                    matched = matchWithType(Arrays.asList(word.split(regexUsed)));
                if (!matched && type.equalsIgnoreCase(word))
                    matched = true;
            }
        return matched;
    }

    /**
     * Method for obtaining a random name from this name generator
     * @param ctx : Context for make request for name
     * @return name
     */
    @NonNull
    public String getRandomName(@NonNull Context ctx) {
        return getRandomNames(ctx, 1).get(0);
    }

    /**
     * Method for obtaining a list of random name from this name generator
     * @param ctx : Context for make request for name
     * @param nb : Integer represent number of name you want
     * @return names
     */
    @NonNull
    public List<String> getRandomNames(@NonNull Context ctx, @NonNull Integer nb) {
        try {
            return getRandomFutureNames(ctx, nb).get();
        } catch (ExecutionException | InterruptedException e) {
            return getRandomNames(ctx, nb);
        }
    }

    /**
     * Method for obtaining a list of future random name from this name generator
     * @param ctx : Context for make request for name
     * @param nb : Integer represent number of name you want
     * @return futures
     */
    @NonNull
    public Future<List<String>> getRandomFutureNames(@NonNull Context ctx, @NonNull Integer nb) {
        SimpleFuture<List<String>> future = new SimpleFuture<>();
        getRandomFutureNames(ctx, nb, future);
        return future;
    }

    /**
     * Method performing the api query for the given system and will place the result in the future
     * past as argument.
     * @param ctx : Context for make request for name
     * @param nb : Integer represent number of name you want
     * @param future : SimpleFuture represents the object in which the result will be stored
    **/
    public void getRandomFutureNames(@NonNull Context ctx, @NonNull Integer nb,
                                    @NonNull SimpleFuture<List<String>> future) {
        String target = String.format("%sname/rpc-name.fcgi?type=%s%s&n=%d",
            apiProvider, specie, type != null ? String.format("+%s", type) : "", nb);
        Ion.with(ctx).load(target).noCache()
            .setLogging(String.format("%s-Ion", TAG), Log.DEBUG)
            .asString().withResponse().setCallback((e, response) -> {
                Log.d(TAG, String.format("Request submit and response receveid !%nProcessing..."));
                if (response != null && response.getHeaders().code() == 200) {
                    List<String> names = new ArrayList<>();
                    for (String name: response.getResult().split(nameDelimiter))
                        names.add(name);
                    int diff = nb - names.size();
                    if (diff > 0)
                        names.addAll(getRandomNames(ctx, nb));
                    Log.d(TAG, "Result set in OwnFuture");
                    future.setComplete(names);
                } else {
                    Log.wtf(TAG, response != null ?
                        String.format("Error code %d for %s!, retrying...",
                            response.getHeaders().code(), target) :
                        String.format("Null response provided for %s", target)
                    );
                    getRandomFutureNames(ctx, nb, future);
                }
            }
        );
    }

    /**
     * Method for create all combination related to species given in argument
     * @param species Species used for creates names
     * @return names
     */
    public static List<Name> createAllCombination(String... species) {
        return createAllCombination(Arrays.asList(species));
    }

    /**
     * Method for create all combination related to species given in argument
     * @param species List of species used for creates names
     * @return names
     */
    public static List<Name> createAllCombination(List<String> species) {
        List<Name> out = new ArrayList<>();
        for (String specie : species)
            out.add(new Name(specie, null));
        return out;
    }

    /**
     * Method for create all combination related to species and types given in argument
     * @param species List of species used for creates names
     * @param types Types used for creates names
     * @return names
     */
    public static List<Name> createAllCombination(List<String> species, String... types) {
        return createAllCombination(species, Arrays.asList(types));
    }

    /**
     * Method for create all combination related to species and types given in argument
     * @param specie specie used for creates names
     * @param types Types used for creates names
     * @return names
     */
    public static List<Name> createAllCombination(String specie, List<String> types) {
        return createAllCombination(Collections.singletonList(specie), types);
    }

    /**
     * Method for create all combination related to species given in argument
     * @param species List of Species used for creates names
     * @param types List of Types used for creates names
     * @return names
     */
    public static List<Name> createAllCombination(List<String> species, List<String> types) {
        List<Name> out = new ArrayList<>();
        for (String specie : species)
            for (String type : types)
                out.add(new Name(specie, type));
        return out;
    }

    // Constant Definition

    // Gender Definition
    public static final String male = "Male";
    public static final String female = "Female";

    // Location Definition
    public static final String location = "Location";
    public static final String town = "Town";
    public static final String castle = "Castle";
    public static final String kingdom = "Kingdom";
    public static final String dungeon = "Dungeon";
    public static final String geography = "Geography";
    public static final String planar = "Planar";
    public static final String ward = "Ward";
    public static final String street = "Street";
    public static final String inn = "Inn";

    // Species Definition
    public static final String human = "Human";
    public static final String dwarvish = "Dwarvish";
    public static final String elvish = "Elvish";
    public static final String halfling = "Halfling";
    public static final String draconic = "Draconic";
    public static final String drow = "Drow";
    public static final String orcish = "Orcish";
    public static final String celestial = "Celestial";
    public static final String fiendish = "Fiendish";
    public static final String modron = "Modron";
    public static final String elemental = "Elemental";
    public static final String eldritch = "Eldritch";
    public static final String terran = "Terran";
    public static final String alien = "Alien";

    // Element Definition
    public static final String air = "Air";
    public static final String earth = "Earth";
    public static final String fire = "Fire";
    public static final String water = "Water";

    // Event Definition
    public static final String party = "Party";
    public static final String ship = "Ship";
    public static final String deity = "Deity";
    public static final String festival = "Festival";
    public static final String blasphemy = "Blasphemy";
    public static final String war = "War";

    // Item Definition
    public static final String tome = "Tome";
    public static final String weapon = "Weapon";

    // Transport Definition
    public static final String spaceship = "Spaceship";

    // Miscellaneous Definition
    public static final String mythos = "Mythos";
    public static final String modern = "Modern";
    public static final String zaibatsu = "Zaibatsu";
    public static final String cyberpunk = "Cyberpunk";
    public static final String space = "Space";
    public static final String world = "World";
    public static final String star = "Star";

    // Country Definition
    public static final String babylonian = "Babylonian";
    public static final String celtic = "Celtic";
    public static final String egyptian = "Egyptian";
    public static final String greek = "Greek";
    public static final String roman = "Roman";
    public static final String sumerian = "Sumerian";
    public static final String english = "English";
    public static final String french = "French";
    public static final String german = "German";
    public static final String italian = "Italian";
    public static final String norse = "Norse";
    public static final String saxon = "Saxon";
    public static final String slavic = "Slavic";
    public static final String spanish = "Spanish";
    public static final String arabic = "Arabic";
    public static final String chinese = "Chinese";
    public static final String hebrew = "Hebrew";
    public static final String hindu = "Hindu";
    public static final String mongolian = "Mongolian";
    public static final String persian = "Persian";
    public static final String japanese = "Japanese";
    public static final String congolese = "Congolese";
    public static final String ethiopian = "Ethiopian";
    public static final String malian = "Malian";
    public static final String algonquin = "Algonquin";
    public static final String aztec = "Aztec";
    public static final String inkan = "Inkan";
    public static final String inuit = "Inuit";
    public static final String navajo = "Navajo";
    public static final String sioux = "Sioux";
    public static final String russian = "Russian";

    // Class Definition
    public static final String investigator = "Investigator";
    public static final String netrunner = "Netrunner";

    // Group of Constants
    public static final List<String> genders =  Arrays.asList(male, female);
    public static final List<String> townAndCastle = Arrays.asList(town, castle);

    // Group for each type of endpoint
    public static final List<String> commonFantasySpecies = Arrays.asList(
        human, dwarvish, elvish, halfling);
    public static final List<String> monstrousFantasySpecies = Arrays.asList(
        draconic, drow, orcish);
    public static final List<String> outsiderFantasySpecies = Arrays.asList(
        celestial, fiendish, modron);
    public static final List<String> elementalFantasySpecies = Arrays.asList(
        air, earth, fire, water);
    public static final List<String> fantasySettingsEntity = Arrays.asList(
        party, ship, deity);
    public static final List<String> fantasySettingsEvent = Arrays.asList(
        festival, blasphemy, war);
    public static final List<String> fantasySettingsItem = Arrays.asList(
        tome, weapon);
    public static final List<String> fantasySettingsBaseLocation = Arrays.asList(
        kingdom, castle, dungeon, geography);
    public static final List<String> fantasySettingsSpecificLocation = Collections.singletonList(
        planar);
    public static final List<String> fantasySettingsTown = Arrays.asList(
        town, ward, street, inn);
    public static final List<String> ancientWorldPeople = Arrays.asList(
        babylonian, celtic, egyptian, greek, roman, sumerian);
    public static final List<String> medievalPeople = Arrays.asList(
        english, french, german, italian, norse, saxon, slavic, spanish);
    public static final List<String> baseAsiaticPeople = Arrays.asList(
        arabic, chinese, hebrew, hindu, mongolian, persian);
    public static final List<String> specificAsiaticPeople = Collections.singletonList(
        japanese);
    public static final List<String> africaPeople = Arrays.asList(
        congolese, egyptian, ethiopian, malian);
    public static final List<String> newWorldPeople = Arrays.asList(
        algonquin, aztec, inkan, inuit, navajo, sioux);
    public static final List<String> cthulhuPeople = Arrays.asList(
        arabic, aztec, chinese, egyptian, sumerian);
    public static final List<String> cyberpunkBasePeople = Arrays.asList(
        modern, russian);
    public static final List<String> cyberpunkExtendedPeople = Arrays.asList(
        chinese, japanese);
    public static final List<String> baseSpacePeople = Collections.singletonList(
        terran);
    public static final List<String> specificSpacePeople = Collections.singletonList(
        alien);

    // Definition of NameGenerator associated
    public static final List<Name> commonFantasyNamesOnly = createAllCombination(
        commonFantasySpecies, genders);
    public static final List<Name> commonFantasyNames = joinList(Arrays.asList(
        commonFantasyNamesOnly, createAllCombination(commonFantasySpecies, town)));
    public static final List<Name> monstrousFantasyNamesOnly = createAllCombination(
        monstrousFantasySpecies, genders);
    public static final List<Name> monstrousFantasyNames = joinList(Arrays.asList(
        monstrousFantasyNamesOnly, createAllCombination(monstrousFantasySpecies, town)));
    public static final List<Name> outsiderFantasyNames = createAllCombination(
        outsiderFantasySpecies);
    public static final List<Name> elementalFantasyNames = createAllCombination(
        elementalFantasySpecies, elemental);
    public static final List<Name> entityFantasyNames = createAllCombination(fantasySettingsEntity);
    public static final List<Name> eventFantasyNames = createAllCombination(fantasySettingsEvent);
    public static final List<Name> itemFantasyNames = createAllCombination(fantasySettingsItem);
    public static final List<Name> locationFantasyNames = joinList(Arrays.asList(
        createAllCombination(fantasySettingsBaseLocation),
        createAllCombination(fantasySettingsSpecificLocation, location)));
    public static final List<Name> townFantasyNames = createAllCombination(fantasySettingsTown);
    public static final List<Name> allPeopleFantasyNames = joinList(Arrays.asList(
        commonFantasyNamesOnly, monstrousFantasyNamesOnly, outsiderFantasyNames,
        elementalFantasyNames));
    public static final List<Name> allFantasyNames = joinList(Arrays.asList(
        commonFantasyNames, monstrousFantasyNames, outsiderFantasyNames, elementalFantasyNames,
        entityFantasyNames, eventFantasyNames, itemFantasyNames, locationFantasyNames,
        townFantasyNames));
    public static final List<Name> ancientWorldPeopleNamesOnly = createAllCombination(
        ancientWorldPeople, genders);
    public static final List<Name> ancientWorldPeopleNames = joinList(Arrays.asList(
        ancientWorldPeopleNamesOnly, createAllCombination(ancientWorldPeople, town)));
    public static final List<Name> medievalPeopleNamesOnly = createAllCombination(
        medievalPeople, genders);
    public static final List<Name> medievalPeopleNames = joinList(Arrays.asList(
        medievalPeopleNamesOnly, createAllCombination(medievalPeople, town)));
    public static final List<Name> asiaticPeopleNamesOnly = createAllCombination(
        joinList(Arrays.asList(baseAsiaticPeople, specificAsiaticPeople)), genders);
    public static final List<Name> asiaticPeopleNames = joinList(Arrays.asList(
        asiaticPeopleNamesOnly, createAllCombination(baseAsiaticPeople, town),
        createAllCombination(specificAsiaticPeople, townAndCastle)));
    public static final List<Name> africaPeopleNamesOnly = createAllCombination(
        africaPeople, genders);
    public static final List<Name> africaPeopleNames = joinList(Arrays.asList(
        africaPeopleNamesOnly, createAllCombination(africaPeople, town)));
    public static final List<Name> newWorldPeopleNamesOnly = createAllCombination(
        newWorldPeople, genders);
    public static final List<Name> newWorldPeopleNames = joinList(Arrays.asList(
        newWorldPeopleNamesOnly, createAllCombination(newWorldPeople, town)));
    public static final List<Name> allQuasiHistoricalPeopleNames = joinList(Arrays.asList(
        ancientWorldPeopleNamesOnly, medievalPeopleNamesOnly, asiaticPeopleNamesOnly,
        newWorldPeopleNamesOnly));
    public static final List<Name> allQuasiHistoricalNames = joinList(Arrays.asList(
        ancientWorldPeopleNames, medievalPeopleNames, asiaticPeopleNames, africaPeopleNames,
        newWorldPeopleNames));
    public static final List<Name> cthulhuCaracterNames = createAllCombination(
        investigator, genders);
    public static final List<Name> cthulhuEldritchNames = createAllCombination(
        eldritch);
    public static final List<Name> cthulhuTomesNames = createAllCombination(
        mythos, Collections.singletonList(tome));
    public static final List<Name> cthulhuBasePeopleNames = joinList(Arrays.asList(
        cthulhuCaracterNames, cthulhuEldritchNames));
    public static final List<Name> cthulhuExtendedPeopleNames = createAllCombination(cthulhuPeople,
        genders);
    public static final List<Name> cthulhuPeopleNames = joinList(Arrays.asList(
        cthulhuBasePeopleNames, cthulhuExtendedPeopleNames));
    public static final List<Name> cthulhuNames = joinList(Arrays.asList(
        cthulhuBasePeopleNames, cthulhuTomesNames));
    public static final List<Name> cthulhuExtendedNames = joinList(Arrays.asList(cthulhuNames,
        createAllCombination(cthulhuPeople, genders)));
    public static final List<Name> cyberpunkBasePeopleNames = joinList(Arrays.asList(
        createAllCombination(cyberpunkBasePeople, genders), createAllCombination(netrunner)));
    public static final List<Name> cyberpunkExtendedPeopleNames = createAllCombination(
        cyberpunkExtendedPeople, genders);
    public static final List<Name> cyberpunkPeopleNames = joinList(Arrays.asList(
        cyberpunkBasePeopleNames, cyberpunkExtendedPeopleNames));
    public static final List<Name> cyberpunkCorporation = createAllCombination(zaibatsu);
    public static final List<Name> cyberpunkLocation = createAllCombination(cyberpunk,
        Collections.singletonList(location));
    public static final List<Name> cyberpunkNames = joinList(Arrays.asList(
        cyberpunkBasePeopleNames, cyberpunkCorporation, cyberpunkLocation));
    public static final List<Name> cyberpunkExtendedNames = joinList(Arrays.asList(
        cyberpunkNames, cyberpunkExtendedPeopleNames));
    public static final List<Name> spacePeopleNames = joinList(Arrays.asList(
        createAllCombination(baseSpacePeople, genders), createAllCombination(specificSpacePeople)));
    public static final List<Name> spaceshipNames = createAllCombination(spaceship);
    public static final List<Name> spaceLocationNames = createAllCombination(space,
        Collections.singletonList(location));
    public static final List<Name> spaceWorldNames = createAllCombination(space,
        Collections.singletonList(world));
    public static final List<Name> starNames = createAllCombination(star);
    public static final List<Name> spaceNames = joinList(Arrays.asList(spacePeopleNames,
        spaceshipNames, spaceLocationNames, spaceWorldNames));
    public static final List<Name> sciFiNames = joinList(Arrays.asList(
        cyberpunkNames, spaceNames, starNames));
    public static final List<Name> sciFiExtendedNames = joinList(Arrays.asList(
        sciFiNames, cyberpunkExtendedPeopleNames));
    public static final List<Name> allPeopleNames = joinList(Arrays.asList(allPeopleFantasyNames,
        allQuasiHistoricalPeopleNames, cthulhuPeopleNames, cyberpunkBasePeopleNames,
        spacePeopleNames));
    public static final List<Name> allNames = joinList(Arrays.asList(allFantasyNames,
        allQuasiHistoricalNames, cthulhuNames, sciFiNames));
    public static final Name defaultName = commonFantasyNames.get(0);

    /**
     * For get random name from the API.
     * @return name
     */
    @NonNull
    public static Name getRandomName() {
        return getRandomName(allNames);
    }

    /**
     * For get random name from list provided.
     * @param names : List where we’re going to shoot randomly
     * @return name
     */
    @NonNull
    public static Name getRandomName(List<Name> names) {
        return names.get(new Random().nextInt(names.size()));
    }

    /**
     * For get random name from keywords given (If keywords match with no generator default is used)
     * @param ctx : Context for make request for name
     * @param keywords : List of String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static String getRandomName(Context ctx, String... keywords) {
        return getRandomName(ctx, Arrays.asList(keywords));
    }

    /**
     * For get random name from keywords given (If keywords match with no generator default is used)
     * @param ctx : Context for make request for name
     * @param keywords : String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static String getRandomName(Context ctx, List<String> keywords) {
        return getRandomName(ctx, allNames, keywords);
    }

    /**
     * For get random name from keywords given (If keywords match with no generator default is used)
     * @param ctx : Context for make request for name
     * @param names : List of Names used for generation if match
     * @param keywords : String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static String getRandomName(Context ctx, List<Name> names, String... keywords) {
        return getRandomName(ctx, names, Arrays.asList(keywords));
    }

    /**
     * For get random name from keywords given (If keywords match with no generator default is used)
     * @param ctx : Context for make request for name
     * @param names : List of Names used for generation if match
     * @param keywords : List of String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static String getRandomName(Context ctx, List<Name> names, List<String> keywords) {
        List<Name> baseNames = getNamesMatches(names, keywords);
        Name nameUsed;
        if (baseNames.isEmpty())
            baseNames = getNamesAlmostMatches(names, keywords);
        if (!baseNames.isEmpty())
            nameUsed = getRandomName(baseNames);
        else
            nameUsed = defaultName;
        return nameUsed.getRandomName(ctx);
    }

    /**
     * For get name generator from keywords given
     * @param keywords : String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static List<Name> getNamesMatches(String... keywords) {
        return getNamesMatches(Arrays.asList(keywords));
    }

    /**
     * For get name generator from keywords given
     * @param keywords : List of String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static List<Name> getNamesMatches(List<String> keywords) {
        return getNamesMatches(allNames, keywords);
    }

    /**
     * For get name generator from keywords given
     * @param names : List of Names used
     * @param keywords : String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static List<Name> getNamesMatches(List<Name> names, String... keywords) {
        return getNamesMatches(names, Arrays.asList(keywords));
    }

    /**
     * For get name generator from keywords given
     * @param names : List of Names used
     * @param keywords : String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static List<Name> getNamesMatches(List<Name> names, List<String> keywords) {
        List<Name> out = new ArrayList<>();
        for (Name name: names)
            if (name.matchWith(keywords))
                out.add(name);
        return out;
    }

    /**
     * For get name generator from keywords given
     * @param keywords : String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static List<Name> getNamesAlmostMatches(String... keywords) {
        return getNamesAlmostMatches(Arrays.asList(keywords));
    }

    /**
     * For get name generator from keywords given
     * @param keywords : List of String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static List<Name> getNamesAlmostMatches(List<String> keywords) {
        return getNamesAlmostMatches(allNames, keywords);
    }

    /**
     * For get name generator from keywords given
     * @param names : List of Names used
     * @param keywords : String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static List<Name> getNamesAlmostMatches(List<Name> names, String... keywords) {
        return getNamesAlmostMatches(names, Arrays.asList(keywords));
    }

    /**
     * For get name generator from keywords given
     * @param names : List of Names used
     * @param keywords : String represent keyword used
     * @return nameGenerated
     */
    @NonNull
    public static List<Name> getNamesAlmostMatches(List<Name> names, List<String> keywords) {
        List<Name> out = new ArrayList<>();
        for (Name name: names)
            if (name.matchAlmostWith(keywords))
                out.add(name);
        return out;
    }
}
