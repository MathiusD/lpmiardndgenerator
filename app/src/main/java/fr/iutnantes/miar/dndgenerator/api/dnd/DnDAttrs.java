package fr.iutnantes.miar.dndgenerator.api.dnd;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;
import java.util.List;

/**
 * DnDAttrs is a class that represents all the attributes of a character record.
 */
public class DnDAttrs implements Serializable {

    public final List<DnDAttr> attrs;

    /**
     * Basic Constructor for create attributes if all arguments are provided.
     * @param attrs : List of DnDAttr represents all attributes of DnDSheet
     */
    public DnDAttrs(@NonNull List<DnDAttr> attrs) {
        this.attrs = attrs;
    }

    /**
     * For write attributes.
     * @return toString
     */
    @Override
    @NonNull
    public String toString() {
        String out = "";
        for (DnDAttr attr : attrs)
            out = String.format("%s%n%s", out, attr);
        return out;
    }

    /**
     * For get specific attribute if is present.
     * @param nameAttr : String to represent Attribute to search
     * @return attr
     */
    @Nullable
    public DnDAttr get(@NonNull String nameAttr) {
        for (DnDAttr attr : attrs)
            if (attr.nameAttr.toLowerCase().equals(nameAttr.toLowerCase()))
                return attr;
        return null;
    }

    /**
     * For identify attributes.
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return attrs.hashCode();
    }

    /**
     * For compare Any Object with this attributes.
     * @param obj : Object for comparison
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj != null && obj.getClass() == this.getClass())
            return this.hashCode() == obj.hashCode();
        else
            return false;
    }
}
