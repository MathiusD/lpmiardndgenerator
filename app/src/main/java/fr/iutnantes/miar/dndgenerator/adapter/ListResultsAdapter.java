package fr.iutnantes.miar.dndgenerator.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Objects;

import fr.iutnantes.miar.dndgenerator.DisplayResultActivity;
import fr.iutnantes.miar.dndgenerator.R;
import fr.iutnantes.miar.dndgenerator.ResearchResultsActivity;
import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSheet;
import fr.iutnantes.miar.dndgenerator.api.tenor.GIFFormat;

/**
 * Adapter that extend RecyclerViewAdapter to provide data of list of DnDSheet in a RecycleView
 */
public class ListResultsAdapter extends RecyclerView.Adapter<ListResultsAdapter.ViewHolder> {
    private static String TAG = "ListResultsAdapter ::";
    public static final int RESULT_DISPLAYRESULT = 33;
    public static final String INDEX = "index";
    private List<DnDSheet> m_data;
    private Activity m_activity;

    /**
     * Class to set the view of one result
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView m_lbl_name, m_lbl_scpecie, m_lbl_description, m_lbl_system;
        private final ImageView m_imgGif;

        /**
         * Constructor
         * @param p_view : view that contain data
         */
        public ViewHolder(View p_view) {
            super(p_view);

            m_lbl_name = p_view.findViewById(R.id.lbl_rclr_name);
            m_lbl_scpecie = p_view.findViewById(R.id.lbl_rclr_specie);
            m_lbl_description = p_view.findViewById(R.id.lbl_rclr_decription);
            m_lbl_system = p_view.findViewById(R.id.lbl_rclr_system);
            m_imgGif = p_view.findViewById(R.id.img_rclr);
        }

        /**
         * Getter
         * @return : Specie
         */
        public TextView getM_lbl_scpecie() {
            return m_lbl_scpecie;
        }

        /**
         * Getter
         * @return : Description
         */
        public TextView getM_lbl_description() {
            return m_lbl_description;
        }

        /**
         * Getter
         * @return : System
         */
        public TextView getM_lbl_system() {
            return m_lbl_system;
        }

        /**
         * Getter
         * @return : Gif's link
         */
        public ImageView getM_imgGif() {
            return m_imgGif;
        }

        /**
         * Getter
         * @return : Name
         */
        public TextView getM_lbl_name() {
            return m_lbl_name;
        }
    }

    /**
     * Constructor
     * @param p_data : List of DnDSheets
     * @param p_activity : The activity
     */
    public ListResultsAdapter(List<DnDSheet> p_data, Activity p_activity) {
        m_data = p_data;
        m_activity = p_activity;
    }

    /**
     * Inflae the View
     * @param parent : parent view
     * @param viewType : Type of view
     * @return : View holder that'll contains the data
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v_view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_pattern_results, parent, false);
        return new ViewHolder(v_view);
    }

    /**
     * Link data and view
     * @param holder : the view that'll contain data
     * @param position : position in the list
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getM_lbl_name().setText(m_data.get(position).getName());
        holder.getM_lbl_scpecie().setText(m_data.get(position).caracterClass.toString());
        holder.getM_lbl_description().setText(m_data.get(position).appearance);
        holder.getM_lbl_system().setText(m_data.get(position).system.longName);

        String tmp = Objects.requireNonNull(Objects.requireNonNull(m_data.get(position).getGIF()
                .media.get(GIFFormat.GIF)).preview);
        Glide.with(holder.itemView)
                .load(tmp)
                .into(holder.getM_imgGif());

        holder.itemView.setOnClickListener(view -> {
            DnDSheet v_clickedSheet = m_data.get(position);
            Intent v_intent = new Intent(m_activity, DisplayResultActivity.class);
            v_intent.putExtra(ResearchResultsActivity.SHEET, v_clickedSheet);
            v_intent.putExtra(INDEX, position);
            m_activity.startActivityForResult(v_intent, RESULT_DISPLAYRESULT);
        });
    }

    /**
     * Get the number of elements
     * @return : Size of the list of elements
     */
    @Override
    public int getItemCount() {
        return m_data.size();
    }
}
