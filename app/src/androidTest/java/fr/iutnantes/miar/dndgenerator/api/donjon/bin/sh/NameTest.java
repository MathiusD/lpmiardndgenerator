package fr.iutnantes.miar.dndgenerator.api.donjon.bin.sh;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class NameTest {
    public Context getMockContext() {
        return InstrumentationRegistry.getInstrumentation().getTargetContext()
                .getApplicationContext();
    }
    //Mock
    public Context ctx = getMockContext();
    public static int n = 25;


    public void testNameGenerated(List<String> namesGenerated, int nb) {
        assertEquals(nb, namesGenerated.size());
        for(String nameGenerated: namesGenerated)
            testNameGenerated(nameGenerated);
    }

    public void testNameGenerated(String nameGenerated) {
        assertNotNull(nameGenerated);
    }

    @Test
    public void getName() {
        testNameGenerated(Name.getRandomName().getRandomName(ctx));
    }

    @Test
    public void getNames() {
        testNameGenerated(Name.getRandomName().getRandomNames(ctx, n), n);
    }

    @Test
    public void getAllNames() {
        for (Name name: Name.allNames)
            testNameGenerated(name.getRandomNames(ctx, n), n);
    }
}
