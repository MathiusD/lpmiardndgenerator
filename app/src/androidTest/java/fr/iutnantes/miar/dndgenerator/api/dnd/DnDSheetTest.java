package fr.iutnantes.miar.dndgenerator.api.dnd;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.content.Context;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.iutnantes.miar.dndgenerator.api.tenor.GIFObjectTest;
import fr.iutnantes.miar.dndgenerator.api.tenor.TenorArgs;

@RunWith(AndroidJUnit4.class)
public class DnDSheetTest {
    public static int n = 15;

    public Context getMockContext() {
        return InstrumentationRegistry.getInstrumentation().getTargetContext()
                .getApplicationContext();
    }

    public static void testSheet(DnDSheet sheet) {
        testSheet(sheet, (DnDSystem) null);
    }
    public static void testSheet(DnDSheet sheet, DnDClass dnDClass) {
        testSheet(sheet, null, dnDClass);
    }
    public static void testSheet(DnDSheet sheet, DnDSystem system) {
        testSheet(sheet, system, null);
    }
    public static void testSheet(DnDSheet sheet, DnDSystem system, DnDClass dnDClass) {
        assertNotNull(sheet);
        GIFObjectTest.testGIF(sheet.getGIF());
        if (system != null)
            assertEquals(sheet.system, system);
        if (dnDClass != null)
            assertEquals(sheet.caracterClass, dnDClass);
    }

    //Mock
    public Context ctx = getMockContext();
    @Test
    public void getRandomSheet() {
        //Get random Sheet
        testSheet(DnDSheet.getRandomCaracterSheet(ctx));
    }
    @Test
    public void getRandomSheetFromRandomSystem() {
        //Get random Sheet from specific system
        DnDSystem system = DnDSystem.getRandomSystem();
        testSheet(DnDSheet.getRandomCaracterSheet(ctx, system), system);
        //Get random Sheet from specific system with Const
        testSheet(DnDSheet.getRandomCaracterSheet(ctx, DnDSystem.anyone));
    }
    @Test
    public void getRandomSheetForEachSystem() {
        DnDSheet sheet;
        //Get random Sheet from each system
        for (DnDSystem sys : DnDSystem.systems) {
            testSheet(DnDSheet.getRandomCaracterSheet(ctx, sys), sys);
        }
    }
    @Test
    public void getRandomSheetFromRandomClass() {
        //Get random Sheet from specific class
        DnDClass dnDClass = DnDClass.getRandomClass();
        testSheet(DnDSheet.getRandomCaracterSheet(ctx, dnDClass), dnDClass);
        //Get random Sheet from specific class with Const
        testSheet(DnDSheet.getRandomCaracterSheet(ctx, DnDClass.ANYONE));
    }
    @Test
    public void getRandomSheetForEachClass() {
        DnDSheet sheet;
        //Get random Sheet from each class
        for (DnDClass cls : DnDClass.classes) {
            testSheet(DnDSheet.getRandomCaracterSheet(ctx, cls), cls);
        }
    }
    @Test
    public void getRandomSheetWithTenorArgs() {
        //Get random Sheet from specific tenor args
        testSheet(DnDSheet.getRandomCaracterSheet(ctx, TenorArgs.GIF));
    }
    @Test
    public void getRandomSheetFromRandomClassWithTenorArgs() {
        //Get random Sheet from  specific class with tenor args
        DnDClass cls = DnDClass.getRandomClass();
        testSheet(DnDSheet.getRandomCaracterSheet(ctx, cls, TenorArgs.GIF), cls);
    }
    @Test
    public void getRandomSheetFromdRandomSystemWithTenorArgs() {
        //Get random Sheet from specific system with tenor args
        DnDSystem system = DnDSystem.getRandomSystem();
        testSheet(DnDSheet.getRandomCaracterSheet(ctx, system, TenorArgs.GIF), system);
    }
    @Test
    public void getRandomSheetFromRandomClassAndRandomSystem() {
        //Get random Sheet from specific system and specific class
        DnDSystem system = DnDSystem.getRandomSystem();
        DnDClass cls = DnDClass.getRandomClass();
        testSheet(DnDSheet.getRandomCaracterSheet(ctx, system, cls), system, cls);
    }
    @Test
    public void getRandomSheetForEachClassAndEachSystem() {
        DnDSheet sheet;
        //Get random Sheet from each system
        for (DnDSystem sys : DnDSystem.systems) {
            //Get random Sheet from each class
            for (DnDClass cls : DnDClass.classes) {
                testSheet(DnDSheet.getRandomCaracterSheet(ctx, sys, cls), sys, cls);
            }
        }
    }
    @Test
    public void getRandomSheetFromRandomClassAndRandomSystemWithTenorArgs() {
        //Get random Sheet from specific system and specific class with tenor args
        DnDSystem system = DnDSystem.getRandomSystem();
        DnDClass cls = DnDClass.getRandomClass();
        testSheet(DnDSheet.getRandomCaracterSheet(ctx, system, cls, TenorArgs.GIF), system, cls);
    }
    public void checkManySheetsFromSameSystem(List<DnDSheet> sheets) {
        checkManySheetsFromSameSystem(sheets, DnDSheet.DEFAULT_SIZE_LIST_SHEET);
    }
    public void checkManySheetsFromSameSystem(List<DnDSheet> sheets, int size) {
        checkManySheetsFromSameSystem(sheets, size, (DnDClass) null);
    }
    public void checkManySheetsFromSameSystem(List<DnDSheet> sheets, DnDSystem system) {
        checkManySheetsFromSameSystem(sheets, DnDSheet.DEFAULT_SIZE_LIST_SHEET, system);
    }
    public void checkManySheetsFromSameSystem(List<DnDSheet> sheets, DnDClass dnDClass) {
        checkManySheetsFromSameSystem(sheets, DnDSheet.DEFAULT_SIZE_LIST_SHEET, dnDClass);
    }
    public void checkManySheetsFromSameSystem(List<DnDSheet> sheets, int size, DnDSystem system) {
        checkManySheetsFromSameSystem(sheets, size, system, null);
    }
    public void checkManySheetsFromSameSystem(List<DnDSheet> sheets, DnDSystem system,
                                              DnDClass dnDClass) {
        checkManySheetsFromSameSystem(sheets, DnDSheet.DEFAULT_SIZE_LIST_SHEET, system, dnDClass);
    }
    public void checkManySheetsFromSameSystem(List<DnDSheet> sheets, int size, DnDClass dnDClass) {
        checkManySheetsFromSameSystem(sheets, size, null, dnDClass);
    }
    public void checkManySheetsFromSameSystem(List<DnDSheet> sheets, int size, DnDSystem system,
                                              DnDClass dnDClass) {
        checkManySheetsFromRandomSystem(sheets, size, dnDClass);
        for (DnDSheet sheet : sheets)
            if (system == null)
                system = sheet.system;
            else
                testSheet(sheet, system);
    }
    public void checkManySheetsFromRandomSystem(List<DnDSheet> sheets) {
        checkManySheetsFromRandomSystem(sheets, DnDSheet.DEFAULT_SIZE_LIST_SHEET);
    }
    public void checkManySheetsFromRandomSystem(List<DnDSheet> sheets, DnDClass dnDClass) {
        checkManySheetsFromRandomSystem(sheets, DnDSheet.DEFAULT_SIZE_LIST_SHEET, dnDClass);
    }
    public void checkManySheetsFromRandomSystem(List<DnDSheet> sheets, int size) {
        checkManySheetsFromRandomSystem(sheets, size, null);
    }
    public void checkManySheetsFromRandomSystem(List<DnDSheet> sheets, int size,
                                                DnDClass dnDClass) {
        if (dnDClass != null)
            checkManySheetsFromSpecificClass(sheets, dnDClass, size);
        assertEquals(size, sheets.size());
        for (DnDSheet sheet : sheets) {
            testSheet(sheet);
            List<DnDSheet> tempSheets = new ArrayList<>(sheets);
            tempSheets.remove(sheet);
            assertFalse(tempSheets.contains(sheet));
        }
    }
    public void checkManySheetsFromSpecificClass(List<DnDSheet> sheets, DnDClass dnDClass) {
        checkManySheetsFromSpecificClass(sheets, dnDClass, DnDSheet.DEFAULT_SIZE_LIST_SHEET);
    }
    public void checkManySheetsFromSpecificClass(List<DnDSheet> sheets, DnDClass dnDClass,
                                                int size) {
        assertEquals(size, sheets.size());
        for (DnDSheet sheet : sheets) {
            testSheet(sheet, dnDClass);
        }
    }
    @Test
    public void getRandomSheets() {
        //Get random Sheets
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx);
        checkManySheetsFromSameSystem(sheets);
    }
    @Test
    public void getNRandomSheets() {
        //Get n random Sheets
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, n);
        checkManySheetsFromSameSystem(sheets, n);
    }
    @Test
    public void getRandomSheetsWithSpecificSystem() {
        //Get random Sheets with specific system
        DnDSystem system = DnDSystem.getRandomSystem();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, system);
        checkManySheetsFromSameSystem(sheets, system);
    }
    @Test
    public void getRandomSheetsWithSpecificClass() {
        //Get random Sheets with specific class
        DnDClass dnDClass = DnDClass.getRandomClass();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, dnDClass);
        checkManySheetsFromSameSystem(sheets, dnDClass);
    }
    @Test
    public void getRandomSheetsWithTenorArgs() {
        //Get random Sheets with tenor args
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, TenorArgs.GIF);
        checkManySheetsFromSameSystem(sheets);
    }
    @Test
    public void getNRandomSheetsWithTenorArgs() {
        //Get n random Sheets with tenor args
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, n, TenorArgs.GIF);
        checkManySheetsFromSameSystem(sheets, n);
    }
    @Test
    public void getRandomSheetsWithSpecificClassAndTenorArgs() {
        //Get random Sheets with specific class and tenor args
        DnDClass dnDClass = DnDClass.getRandomClass();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, dnDClass, TenorArgs.GIF);
        checkManySheetsFromSameSystem(sheets, dnDClass);
    }
    @Test
    public void getNRandomSheetsWithSpecificClass() {
        //Get n random Sheets with specific class
        DnDClass dnDClass = DnDClass.getRandomClass();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, n, dnDClass);
        checkManySheetsFromSameSystem(sheets, n, dnDClass);
    }
    @Test
    public void getRandomSheetsFromSpecificSystemAndTenorArgs() {
        //Get n random Sheets from specific system and tenor args
        DnDSystem system = DnDSystem.getRandomSystem();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, system, TenorArgs.GIF);
        checkManySheetsFromSameSystem(sheets, system);
    }
    @Test
    public void getNRandomSheetsFromSpecificSystem() {
        //Get n random Sheets from specific system
        DnDSystem system = DnDSystem.getRandomSystem();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, n, system);
        checkManySheetsFromSameSystem(sheets, n, system);
    }
    @Test
    public void getRandomSheetsWithSpecificClassAndSpecificSystem() {
        //Get random Sheets with specific class and specific system
        DnDClass dnDClass = DnDClass.getRandomClass();
        DnDSystem system = DnDSystem.getRandomSystem();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, system, dnDClass);
        checkManySheetsFromSameSystem(sheets, system, dnDClass);
    }
    @Test
    public void getRandomSheetsFromSpecificSystemWithSpecificClassAndTenorArgs() {
        //Get random Sheets from specific system with specific class and tenor args
        DnDClass dnDClass = DnDClass.getRandomClass();
        DnDSystem system = DnDSystem.getRandomSystem();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, system, dnDClass,
            TenorArgs.GIF);
        checkManySheetsFromSameSystem(sheets, system, dnDClass);
    }
    @Test
    public void getNRandomSheetsFromSpecificSystemAndTenorArgs() {
        //Get n random Sheets from specific system with specific class and tenor args
        DnDSystem system = DnDSystem.getRandomSystem();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, n, system,  TenorArgs.GIF);
        checkManySheetsFromSameSystem(sheets, n, system);
    }
    @Test
    public void getNRandomSheetsFromSSpecificClassAndTenorArgs() {
        //Get n random Sheets from specific system with specific class and tenor args
        DnDClass dnDClass = DnDClass.getRandomClass();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, n, dnDClass, TenorArgs.GIF);
        checkManySheetsFromSameSystem(sheets, n, dnDClass);
    }
    @Test
    public void getNRandomSheetsFromSpecificSystemWithSpecificClass() {
        //Get n random Sheets from specific system with specific class
        DnDClass dnDClass = DnDClass.getRandomClass();
        DnDSystem system = DnDSystem.getRandomSystem();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, n, system, dnDClass);
        checkManySheetsFromSameSystem(sheets, n, system, dnDClass);
    }
    @Test
    public void getNRandomSheetsFromSpecificSystemWithSpecificClassAndTenorArgs() {
        //Get n random Sheets from specific system with specific class and tenor args
        DnDClass dnDClass = DnDClass.getRandomClass();
        DnDSystem system = DnDSystem.getRandomSystem();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx, n, system, dnDClass,
            TenorArgs.GIF);
        checkManySheetsFromSameSystem(sheets, n, system, dnDClass);
    }
    @Test
    public void getRandomSheetsFromRandomSystem() {
        //Get random Sheets from Random System
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheetFromRandomSystem(ctx);
        checkManySheetsFromRandomSystem(sheets);
    }
    @Test
    public void getNRandomSheetsFromRandomSystem() {
        //Get n random Sheets from Random System
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheetFromRandomSystem(ctx, n);
        checkManySheetsFromRandomSystem(sheets, n);
    }
    @Test
    public void getRandomSheetsFromRandomSystemWithSpecificClass() {
        //Get random Sheets from Random System with specific class
        DnDClass dnDClass = DnDClass.getRandomClass();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheetFromRandomSystem(ctx, dnDClass);
        checkManySheetsFromRandomSystem(sheets, dnDClass);
    }
    @Test
    public void getRandomSheetsFromRandomSystemWithTenorArgs() {
        //Get random Sheets from Random System with tenor args
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheetFromRandomSystem(ctx,
            TenorArgs.GIF);
        checkManySheetsFromRandomSystem(sheets);
    }
    @Test
    public void getNRandomSheetsFromRandomSystemWithSpecificClass() {
        //Get n random Sheets from Random System with specific class
        DnDClass dnDClass = DnDClass.getRandomClass();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheetFromRandomSystem(ctx, n, dnDClass);
        checkManySheetsFromRandomSystem(sheets, n, dnDClass);
    }
    @Test
    public void getNRandomSheetsFromRandomSystemWithTenorArgs() {
        //Get n random Sheets from Random System with specific class and tenor args
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheetFromRandomSystem(ctx, n,
            TenorArgs.GIF);
        checkManySheetsFromRandomSystem(sheets, n);
    }
    @Test
    public void getRandomSheetsFromRandomSystemWithSpecificClassAndTenorArgs() {
        //Get random Sheets from Random System with specific class and tenor args
        DnDClass dnDClass = DnDClass.getRandomClass();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheetFromRandomSystem(ctx, dnDClass,
            TenorArgs.GIF);
        checkManySheetsFromRandomSystem(sheets, dnDClass);
    }
    @Test
    public void getNRandomSheetsFromRandomSystemWithSpecificClassAndTenorArgs() {
        //Get n random Sheets from Random System with specific class and tenor args
        DnDClass dnDClass = DnDClass.getRandomClass();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheetFromRandomSystem(ctx, n, dnDClass);
        checkManySheetsFromRandomSystem(sheets, n, dnDClass);
    }
    @Test
    public void testComparatorBySystem() {
        DnDSheet firstSheet = DnDSheet.getRandomCaracterSheet(ctx, DnDSystem.lotfp);
        DnDSheet secondSheet = DnDSheet.getRandomCaracterSheet(ctx, DnDSystem.lbb);
        List<DnDSheet> sheetsUnSorted = new ArrayList<>();
        sheetsUnSorted.add(secondSheet);
        sheetsUnSorted.add(firstSheet);
        List<DnDSheet> sheetsSorted = new ArrayList<>();
        sheetsSorted.add(firstSheet);
        sheetsSorted.add(secondSheet);
        assertNotEquals(sheetsSorted, sheetsUnSorted);
        Collections.sort(sheetsUnSorted, DnDSheet.COMPARATOR_BY_SYSTEM);
        assertEquals(sheetsUnSorted, sheetsSorted);
    }
    @Test
    public void testComparatorByClass() {
        DnDSheet firstSheet = DnDSheet.getRandomCaracterSheet(ctx, DnDClass.DWARF);
        DnDSheet secondSheet = DnDSheet.getRandomCaracterSheet(ctx, DnDClass.FIGHTER);
        List<DnDSheet> sheetsUnSorted = new ArrayList<>();
        sheetsUnSorted.add(secondSheet);
        sheetsUnSorted.add(firstSheet);
        List<DnDSheet> sheetsSorted = new ArrayList<>();
        sheetsSorted.add(firstSheet);
        sheetsSorted.add(secondSheet);
        assertNotEquals(sheetsSorted, sheetsUnSorted);
        Collections.sort(sheetsUnSorted, DnDSheet.COMPARATOR_BY_CLASS);
        assertEquals(sheetsUnSorted, sheetsSorted);
    }
}
