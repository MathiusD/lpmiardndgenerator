package fr.iutnantes.miar.dndgenerator.api.dnd.models;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.content.Context;
import java.util.List;

import fr.iutnantes.miar.dndgenerator.api.dnd.DnDSheet;
import fr.iutnantes.miar.dndgenerator.api.tenor.GIFObject;

@RunWith(AndroidJUnit4.class)
public class DnDSheetDBTest {
    public Context getMockContext() {
        return InstrumentationRegistry.getInstrumentation().getTargetContext()
                .getApplicationContext();
    }

    //Mock
    public Context ctx = getMockContext();
    public int dbVersion = DnDSheetDB.DATABASE_VERSION + 1;

    public void dropDBs() {
        DnDSheet.removeAllFavs(ctx, dbVersion);
    }

    @Test
    public void testBasicInteract() {
        dropDBs();
        DnDSheet sheet = DnDSheet.getRandomCaracterSheet(ctx);
        DnDSheetDB db = new DnDSheetDB(ctx, dbVersion);
        assertNull(db.getDnDSheet(sheet.hashCode()));
        assertFalse(db.updateGIF(sheet));
        assertTrue(db.insertDnDSheet(sheet));
        assertFalse(db.insertDnDSheet(sheet));
        assertEquals(db.getDnDSheet(sheet.hashCode()), sheet);
        assertTrue(db.removeDnDSheet(sheet));
        assertNull(db.getDnDSheet(sheet.hashCode()));
        assertFalse(db.updateGIF(sheet));
        assertFalse(db.removeDnDSheet(sheet));
        assertTrue(db.insertDnDSheet(sheet));
        assertEquals(db.getDnDSheet(sheet.hashCode()), sheet);
        sheet.changeGIF(ctx, GIFObject.getGIFObject(ctx, "Hi'"));
        assertTrue(db.updateGIF(sheet));
        assertEquals(db.getDnDSheet(sheet.hashCode()).getGIF(), sheet.getGIF());
        sheet.generateName(ctx);
        assertTrue(db.updateName(sheet));
        assertEquals(db.getDnDSheet(sheet.hashCode()).getName(), sheet.getName());
        assertTrue(db.removeDnDSheet(sheet));
        dropDBs();
    }

    @Test
    public void TestBasicInteractWithManySheet() {
        dropDBs();
        DnDSheetDB db = new DnDSheetDB(ctx, dbVersion);
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx);
        assertEquals(0, db.getAllDnDSheet().size());
        for (DnDSheet caracter : sheets)
            assertTrue(db.insertDnDSheet(caracter));
        assertEquals(sheets.size(), db.getAllDnDSheet().size());
        for (DnDSheet carac : db.getAllDnDSheet())
            assertTrue(sheets.contains(carac));
        for (DnDSheet carac : db.getAllDnDSheet())
            assertTrue(db.removeDnDSheet(carac));
        assertEquals(0, db.getAllDnDSheet().size());
        dropDBs();
    }

    @Test
    public void testBasicInteractViaDnDSheet() {
        dropDBs();
        DnDSheet sheet = DnDSheet.getRandomCaracterSheet(ctx);
        GIFObject gif = GIFObject.getGIFObject(ctx, "Hi'");
        assertTrue(sheet.addFav(ctx, dbVersion));
        assertFalse(sheet.addFav(ctx, dbVersion));
        assertTrue(sheet.removeFav(ctx, dbVersion));
        assertFalse(sheet.removeFav(ctx, dbVersion));
        assertFalse(sheet.updateFav(ctx, dbVersion));
        sheet.changeGIF(ctx, gif);
        sheet.generateName(ctx);
        assertTrue(sheet.addFav(ctx, dbVersion));
        assertTrue(sheet.updateFav(ctx, dbVersion));
        assertTrue(sheet.removeFav(ctx, dbVersion));
        dropDBs();
    }

    @Test
    public void testBasicInteractViaDnDSheetWithManySheet() {
        dropDBs();
        List<DnDSheet> sheets = DnDSheet.getRandomsCaracterSheet(ctx);
        assertEquals(0, DnDSheet.getFavs(ctx, dbVersion).size());
        for (DnDSheet caracter : sheets)
            assertTrue(caracter.addFav(ctx, dbVersion));
        assertEquals(sheets.size(), DnDSheet.getFavs(ctx, dbVersion).size());
        for (DnDSheet carac : DnDSheet.getFavs(ctx, dbVersion))
            assertTrue(sheets.contains(carac));
        for (DnDSheet carac : DnDSheet.getFavs(ctx, dbVersion))
            assertTrue(carac.removeFav(ctx, dbVersion));
        assertEquals(0, DnDSheet.getFavs(ctx, dbVersion).size());
        dropDBs();
    }
}