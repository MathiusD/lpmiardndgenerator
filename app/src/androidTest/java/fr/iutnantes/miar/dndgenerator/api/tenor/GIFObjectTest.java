package fr.iutnantes.miar.dndgenerator.api.tenor;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.content.Context;

import java.util.List;

@RunWith(AndroidJUnit4.class)
public class GIFObjectTest {
    public Context getMockContext() {
        return InstrumentationRegistry.getInstrumentation().getTargetContext()
                .getApplicationContext();
    }
    //Mock
    public Context ctx = getMockContext();
    public static int n = 15;
    public static int N = n + GIFObject.MAX_SIZE;
    public static String query = "Hi'";

    public static void testGIF(GIFObject gif) {
        testGIF(gif, null);
    }
    public static void testGIF(GIFObject gif, GIFObject excludeGIF) {
        assertNotNull(gif);
        for (GIFFormat format : GIFFormat.ALL_FORMAT) {
            if (format.isPresentInMediaFilter(gif.mediaFilter)) {
                assertNotNull(gif.media.get(format));
            }
        }
        assertNotEquals(gif, excludeGIF);
    }

    @Test
    public void getGIF() {
        //Get GIF
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query);
        testGIF(baseGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs());
        testGIF(newGIF);
        assertEquals(baseGIF, newGIF);
    }

    @Test
    public void getGIFbyID() {
        //Get GIF
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query);
        //Get Correct GIF By ID
        GIFObject newGIF = GIFObject.getGIFObjectById(ctx, baseGIF.id, baseGIF.artifact);
        testGIF(newGIF);
        assertEquals(baseGIF, newGIF);
        //Get id incorrect
        newGIF = GIFObject.getGIFObjectById(ctx, query, new GIFArtifact());
        assertNull(newGIF);
        //Get GIF doesn't exist
        newGIF = GIFObject.getGIFObjectById(ctx, "0", new GIFArtifact());
        assertNull(newGIF);
    }

    @Test
    public void getGIFWithContentFilter() {
        //Get GIF with content filter
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query, filterContent);
        testGIF(baseGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs(filterContent));
        testGIF(newGIF);
        assertEquals(baseGIF, newGIF);
    }

    @Test
    public void getGIFWithMediaFilter() {
        //Get GIF with media filter
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query, mediaFilter);
        testGIF(baseGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs(mediaFilter));
        testGIF(newGIF);
        assertEquals(baseGIF, newGIF);
    }

    @Test
    public void getGIFWithGIFFormat() {
        //Get GIF with GIF format
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query, gifFormat);
        testGIF(baseGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs(gifFormat));
        testGIF(newGIF);
        assertEquals(baseGIF, newGIF);
    }

    @Test
    public void getGIFWithGIFExclude() {
        //Get GIF different of GIF exclude
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query, excludeGIF);
        testGIF(baseGIF, excludeGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs(excludeGIF));
        testGIF(newGIF, excludeGIF);
        assertEquals(baseGIF, newGIF);
    }

    @Test
    public void getGIFWithMediaFilterAndGIFExclude() {
        //Get GIF with media filter different of GIF exclude
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query, mediaFilter, excludeGIF);
        testGIF(baseGIF, excludeGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs(mediaFilter,
            excludeGIF));
        testGIF(newGIF, excludeGIF);
        assertEquals(baseGIF, newGIF);
    }

    @Test
    public void getGIFWithMediaFilterAndContentFilter() {
        //Get GIF with media filter and content filter
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query, mediaFilter, filterContent);
        testGIF(baseGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs(mediaFilter,
            filterContent));
        testGIF(newGIF);
        assertEquals(baseGIF, newGIF);
    }

    @Test
    public void getGIFWithGIFFormatAndGIFExclude() {
        //Get GIF with GIF format different of GIF exclude
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query, gifFormat, excludeGIF);
        testGIF(baseGIF, excludeGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs(gifFormat,
            excludeGIF));
        testGIF(newGIF, excludeGIF);
        assertEquals(baseGIF, newGIF);
    }

    @Test
    public void getGIFWithContentFilterAndGIFExclude() {
        //Get GIF with content filter different of GIF exclude
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query, filterContent, excludeGIF);
        testGIF(baseGIF, excludeGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs(filterContent,
            excludeGIF));
        testGIF(newGIF, excludeGIF);
        assertEquals(baseGIF, newGIF);
    }

    @Test
    public void getGIFWithGIFFormatAndContentFilter() {
        //Get GIF with GIF format and content filter
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query, gifFormat, filterContent);
        testGIF(baseGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs(gifFormat,
            filterContent));
        testGIF(newGIF);
        assertEquals(baseGIF, newGIF);
    }

    @Test
    public void getGIFWithMediaFilterAndContentFilterANDGIFExclude() {
        //Get GIF with media filter and content filter different of GIF exclude
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query, mediaFilter,
            filterContent, excludeGIF);
        testGIF(baseGIF, excludeGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs(mediaFilter,
            filterContent, excludeGIF));
        testGIF(newGIF, excludeGIF);
        assertEquals(baseGIF, newGIF);
    }

    @Test
    public void getGIFWithGIFFormatAndContentFilterANDGIFExclude() {
        //Get GIF with GIF format and content filter different of GIF exclude
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        GIFObject baseGIF = GIFObject.getGIFObject(ctx, query, gifFormat,
                filterContent, excludeGIF);
        testGIF(baseGIF, excludeGIF);
        //Compare With Tenor Args
        GIFObject newGIF = GIFObject.getGIFObject(ctx, query, new TenorArgs(gifFormat,
                filterContent, excludeGIF));
        testGIF(newGIF, excludeGIF);
        assertEquals(baseGIF, newGIF);
    }

    public static void testSize(List<GIFObject> gifs) {
        testSize(gifs, GIFObject.DEFAULT_SIZE);
    }

    public static void testSize(List<GIFObject> gifs, int nb) {
        testSize(gifs, nb, null);
    }

    public static void testSize(List<GIFObject> gifs, GIFObject excludeGIF) {
        testSize(gifs, GIFObject.DEFAULT_SIZE, excludeGIF);
    }

    public static void testSize(List<GIFObject> gifs, int nb, GIFObject excludeGIF) {
        assertEquals(gifs.size(), nb);
        testList(gifs, excludeGIF);
    }
    public static void testList(List<GIFObject> gifs) {
        testList(gifs, null);
    }

    public static void testList(List<GIFObject> gifs, GIFObject excludeGIF) {
        for (GIFObject gif : gifs)
            testGIF(gif, excludeGIF);
    }

    @Test
    public void getGIFs() {
        //Get GIFs
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query);
        testSize(baseGIFS);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs());
        testSize(baseGIFS);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getGIFsWithContentFilter() {
        //Get GIFs with content filter
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, filterContent);
        testSize(baseGIFS);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(filterContent));
        testSize(baseGIFS);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getGIFsWithMediaFilter() {
        //Get GIFs with media filter
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, mediaFilter);
        testSize(baseGIFS);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(mediaFilter));
        testSize(baseGIFS);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getGIFsWithGIFFormat() {
        //Get GIFs with GIF Format
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, gifFormat);
        testSize(baseGIFS);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(gifFormat));
        testSize(baseGIFS);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getNGIFs() {
        //Get N GIFs
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, n);
        testSize(baseGIFS, n);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(n));
        testSize(baseGIFS, n);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
        //Get Too many GIFs
        baseGIFS = GIFObject.getGIFObjects(ctx, query, N);
        testSize(baseGIFS, N);
        //Compare With Tenor Args
        newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(N));
        testSize(baseGIFS, N);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getGIFsWithGIFExclude() {
        //Get GIFs different of GIF exclude
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, excludeGIF);
        testSize(baseGIFS, excludeGIF);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(excludeGIF));
        testSize(baseGIFS, excludeGIF);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getNGIFsWithContentFilter() {
        //Get N GIFs with content filter
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, filterContent, n);
        testSize(baseGIFS, n);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(filterContent,
            n));
        testSize(baseGIFS, n);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getNGIFsWithMediaFilter() {
        //Get N GIFs with media filter
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, mediaFilter, n);
        testSize(baseGIFS, n);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(mediaFilter,
            n));
        testSize(baseGIFS, n);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getNGIFsWithGIFFormat() {
        //Get N GIFs with GIF Format
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, gifFormat, n);
        testSize(baseGIFS, n);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(gifFormat,
            n));
        testSize(baseGIFS, n);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getNGIFsWithGIFExclude() {
        //Get N GIFs different of GIF exclude
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, n, excludeGIF);
        testSize(baseGIFS, n, excludeGIF);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(n, excludeGIF));
        testSize(baseGIFS, n, excludeGIF);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getGIFsWithGIFFormatAndGIFExclude() {
        //Get GIFs with GIF Format different of GIF exclude
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, gifFormat, excludeGIF);
        testSize(baseGIFS, excludeGIF);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(gifFormat,
            excludeGIF));
        testSize(baseGIFS, excludeGIF);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getGIFsWithGIFFormatAndContentFilter() {
        //Get GIFs with GIF Format and content filter
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, gifFormat, filterContent);
        testSize(baseGIFS);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(gifFormat,
            filterContent));
        testSize(baseGIFS);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getGIFsWithMediaFilterAndGIFExclude() {
        //Get GIFs with media filter different of GIF exclude
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, mediaFilter, excludeGIF);
        testSize(baseGIFS, excludeGIF);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(mediaFilter,
                excludeGIF));
        testSize(baseGIFS, excludeGIF);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getGIFsWithMediaFilterAndContentFilter() {
        //Get GIFs with media filter and content filter
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, mediaFilter, filterContent);
        testSize(baseGIFS);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(mediaFilter,
            filterContent));
        testSize(baseGIFS);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getGIFsWithContentFilterAndGIFExclude() {
        //Get GIFs with content filter different of GIF exclude
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, filterContent, excludeGIF);
        testSize(baseGIFS, excludeGIF);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(filterContent,
            excludeGIF));
        testSize(baseGIFS, excludeGIF);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getNGIFsWithGIFFormatAndGIFExclude() {
        //Get n GIFs with GIF Format different of GIF exclude
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, gifFormat, n, excludeGIF);
        testSize(baseGIFS, n, excludeGIF);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(gifFormat,
            n, excludeGIF));
        testSize(baseGIFS, n, excludeGIF);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getGIFsWithGIFFormatAndContentFilterAndGIFExclude() {
        //Get GIFs with GIF Format and content filter different of GIF exclude
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, gifFormat, filterContent,
            excludeGIF);
        testSize(baseGIFS, excludeGIF);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(gifFormat,
            filterContent, excludeGIF));
        testSize(baseGIFS, excludeGIF);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getNGIFsWithGIFFormatAndContentFilter() {
        //Get n GIFs with GIF Format and content filter
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, gifFormat, filterContent,
            n);
        testSize(baseGIFS, n);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(gifFormat,
            filterContent, n));
        testSize(baseGIFS, n);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getNGIFsWithMediaFilterAndGIFExclude() {
        //Get n GIFs with media filter different of GIF exclude
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, mediaFilter, n,
            excludeGIF);
        testSize(baseGIFS, n, excludeGIF);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(mediaFilter,
            n, excludeGIF));
        testSize(baseGIFS, n, excludeGIF);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getGIFsWithMediaFilterAndContentFilterAndGIFExclude() {
        //Get GIFs with media filter and content filter different of GIF exclude
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, mediaFilter, filterContent,
            excludeGIF);
        testSize(baseGIFS, excludeGIF);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(mediaFilter,
            filterContent, excludeGIF));
        testSize(baseGIFS, excludeGIF);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getNGIFsWithMediaFilterAndContentFilter() {
        //Get n GIFs with media filter and content filter
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, mediaFilter, filterContent,
            n);
        testSize(baseGIFS, n);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(mediaFilter,
            filterContent, n));
        testSize(baseGIFS, n);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getNGIFsWithGIFFormatAndContentFilterAndGIFExclude() {
        //Get n GIFs with GIF Format and content filter different of GIF exclude
        GIFFormat gifFormat = GIFFormat.getRandomGIFFormat();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, gifFormat, filterContent,
            n, excludeGIF);
        testSize(baseGIFS, n, excludeGIF);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(gifFormat,
            filterContent, n, excludeGIF));
        testSize(baseGIFS, n, excludeGIF);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
    @Test
    public void getNGIFsWithMediaFilterAndContentFilterAndGIFExclude() {
        //Get n GIFs with media filter and content filter different of GIF exclude
        MediaFilter mediaFilter = MediaFilter.getRandomMediaFilter();
        FilterContent filterContent = FilterContent.getRandomFilterContent();
        GIFObject excludeGIF = GIFObject.getGIFObject(ctx, query);
        List<GIFObject> baseGIFS = GIFObject.getGIFObjects(ctx, query, mediaFilter, filterContent,
            n, excludeGIF);
        testSize(baseGIFS, n, excludeGIF);
        //Compare With Tenor Args
        List<GIFObject> newGIFS = GIFObject.getGIFObjects(ctx, query, new TenorArgs(mediaFilter,
            filterContent, n, excludeGIF));
        testSize(baseGIFS, n, excludeGIF);
        for (GIFObject gif : baseGIFS)
            assertEquals(gif, newGIFS.get(baseGIFS.indexOf(gif)));
    }
}
