# Projet Android Cross Api

Il nous faut exploiter 2 apis simultanément, l'une qui exploite des images et l'autre récupère des fiches personnages.

## APIS

* Nous voulons utiliser [l'api de funkaoshi](https://github.com/funkaoshi/randomcharacter) pour générer des fiches D&D (Donjon et Dragon).
* Nous souhaitons également exploiter [l'api de Tenor](https://tenor.com/gifapi/documentation) afin de proposer les GIF liés à la descritpion du personnage proposée par l'api précédente.
* Et si on a du temps, pourquoi pas utiliser ce [site web](http://donjon.bin.sh) même s'il faut faire un crawler vu qu'il ne semble pas y avoir d'api.

## Description

L'app a pour but de générer une fiche D&D d'un système donné (L'api en question en supporte 9 visiblement) avec un GIF lié à la description physique de la fiche en question.
Afin de permettre à l'utilisateur d'avoir l'embaras du choix, nous comptons générer plusieurs personnages avec ses GIF liés (exemple 10 résultats par défaults).
Ainsi une fois le résultat sélectionné, l'utilisateur verra tout le détail de la fiche avec le GIF lié. Un menu permettra également de voir tout les GIF liés à la fiche en question et de le modifier s'il le désire.

### Home

La partie suppérieure de l'écran est dédiée à la sélection des critères de recherche, tel que le Système, dont doivent être issus les fiches personnages générées et enfin le nombre de résultats attendu. Par défaut, aucun système n'est sélectionné et le nombre de résultats attendu est 10.
Le click sur le bouton `Generate` mènera à l'activité `ReserchResults`.

La partie inférieure de l'écran affichera un liste de fiches personnages désignées comme favorites par l'utilisateur. Par défaut, cette liste est vide.
Le click sur un résultat mènera à l'activité `DisplayResult` remplie des informations correspondantes.

### Reserch Results

Affiche la liste des résultats correspondants à la recherche faite dans l'activité `Home`. Chaque résultat est affiché avec un GIF ainsi que la classe, sa description et le système dont il est issu.
Le click sur l'un des résultats ouvre l'activité `DisplayResult` avec les informations correspondantes.

### Display Result

Cette activité affiche le détail d'un résultat sélectionné depuis les activités `Home` ou `ReserchResults`. Cette description contient : le GIF, la classe, la description, les caractéristiques, les attributs et l'équipement du personnage.
Le click sur l'étoile ajoutera la fiche personnage aux favoris. Si l'étoile est vide, la fiche n'est pas en favori, si elle est pleine elle est en favori. La liste des favoris est affichée dans l'activité `Home`.
Le click sur le GIF ouvrira l'activité `DisplayGIF`.

### Display GIF

Cette activité affiche les détails et les GIF associés du GIF sélectionné dans l'activité `DisplayResult`. Le GIF sélectionné est en haut de la page avec son nom et les tags (mots clés de recherche) qui lui sont associé.
En dessous, une liste déroulante de GIF associé.
Le click sur l'un des GIf de la liste aura pour effet de remplacé par ce GIF, celui associé à la fiche personnage. L'activité `DisplayGIF` sera alors fermé et nous reviendrons sur l'activité `DisplayResult`.

## Pré-requis

Attention pour pouvoir exploiter pleinement notre dépôt nous vous conseillons soit d'exploiter l'un des fichiers .apk provenant des releases ou des pipelines de master.

Cependant si vous souhaiter développer ou faire avancer notre projet, il vous faudra :

* Créer un fichier nommé `apikey.properties` (Qui est ignoré par notre .gitignore donc ne le pousser pas merci par avance) à la racine de notre dépôt et y placer ceci `TENOR_KEY="Votre token tenor"`. Si vous n'avez pas de token tenor il vous suffit de vous [inscrire](https://tenor.com/developer/keyregistration) afin de l'obtenir.
* Créer un fichier `releasekey.properties` avec trois variables en son sein comme il suit :

```properties
KEY_FILE=releaseKey.jks
KEY_PASSWORD=MotDePasseALaCon
KEY_ALIAS=releaseKey
```

* Et également vous créer une clé si vous souhaitez produire une release soit :
  * Si vous êtes sous Unix/Mac : en utilisant un script dédié à cet effet comme il suit `sh ./genKey.sh releaseKey.jks releaseKey MotDePasse "Scrum Mouton" "DnDGenerator" "The RAAD Team" "Nantes" "Pays de la Loire" "FR"`.
  * Si vous êtes sur Windows nous n'avons pas travaillé sur cette plateforme et nous incitons donc à lire la [documentation](https://developer.android.com/studio/publish/app-signing#generate-key).

## tl;dr

Une app qui génère des fiches de D&D avec un GIF associé et un système de favoris.

## Authors

* Mathieu FERY
* Mathilde BALLOUHEY
